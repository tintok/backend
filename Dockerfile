FROM adoptopenjdk/openjdk11:jdk-11.0.9.1_1-debianslim-slim

ENV SPRING_PROFILES_ACTIVE prod

RUN mkdir /content

HEALTHCHECK --start-period=30s --interval=1m --timeout=10s --retries=5 \
            CMD curl --fail --silent localhost:8080/status | grep UP || exit 1

COPY ./build/libs/*.jar /usr/src/backend/app.jar
WORKDIR /usr/src/backend

RUN jar -xf app.jar

ENTRYPOINT ["java"]
CMD ["-XX:+UseContainerSupport", "-XX:MaxDirectMemorySize=10M", "-XX:MaxMetaspaceSize=183593K", \
     "-XX:ReservedCodeCacheSize=240M", "-Xss1M", "-Xmx2709206K", "-noverify", \
     "-cp", "BOOT-INF/classes:BOOT-INF/lib/*", "de.iptk.groupj.TinTokKt"]
