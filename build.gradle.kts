import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    jcenter()
    mavenCentral()
}

plugins {
    // Kotlin
    val kotlinVersion = "1.4.31"
    kotlin("jvm") version kotlinVersion
    kotlin("kapt") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
    id("org.jetbrains.dokka") version "1.4.20"

    // Spring
    id("org.springframework.boot") version "2.4.3"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"

    // Integrated Plugins
    idea
    jacoco
    application
    id("de.jansauer.printcoverage") version "2.0.0"

    // Utils
    // Integration test plugin
    id("com.coditory.integration-test") version "1.1.11"
    // Beautiful logger for tests https://github.com/radarsh/gradle-test-logger-plugin
    id("com.adarshr.test-logger") version "2.1.1"
    // Check for dependency updates via task 'dependencyUpdates --refresh-dependencies'
    id("com.github.ben-manes.versions") version "0.38.0"
}

group = "de.iptk.groupj"
version = "$version"
java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

application {
    mainClass.set("de.iptk.groupj.TinTokKt")
}

// Exclude Spring default logger from environment because of a dependency conflict
configurations.all {
    exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
}

configurations.testImplementation {
    exclude(module = "mockito-core")
    exclude(group = "org.assertj", module = "assertj-core")
    exclude(group = "rg.junit.vintage", module = "junit-vintage-engine")
}

// Exclude Spring Logger because of Dependency conflict
configurations.testRuntime {
    exclude(group = "org.apache.logging.log4j")
}

dependencyManagement {
    imports {
        mavenBom("org.junit:junit-bom:5.7.1")
    }
}

dependencies {
    if (JavaVersion.current().isJava9Compatible) {
        implementation(group = "javax.annotation", name = "javax.annotation-api", version = "1.3.2")
    }

    // Spring Boot
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-web")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-security")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-validation")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-aop")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-actuator")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-log4j2")
    kapt(group = "org.springframework.boot", name = "spring-boot-configuration-processor")

    // Other Spring related Projects
    implementation(group = "org.springframework.retry", name = "spring-retry")
    kapt(group = "org.springframework", name = "spring-context-indexer", version = "5.3.4")
    implementation(group = "com.fasterxml.jackson.module", name = "jackson-module-kotlin", version = "2.12.2")
    implementation(group = "com.github.paulcwarren", name = "spring-content-fs-boot-starter", version = "1.2.1")

    // Kotlin
    implementation(group = "org.jetbrains.kotlin", name = "kotlin-stdlib")
    implementation(group = "org.jetbrains.kotlin", name = "kotlin-reflect")
    implementation(group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-core")

    // Validate OAuth Tokens
    implementation(group = "com.github.scribejava", name = "scribejava-apis", version = "8.1.0")

    // JWT
    implementation(group = "com.auth0", name = "java-jwt", version = "3.14.0")

    // Firebase Messaging for Notifications
    implementation(group = "com.squareup.retrofit2", name = "retrofit", version = "2.9.0")
    implementation(group = "com.squareup.retrofit2", name = "converter-jackson", version = "2.9.0")

    // Enable gzip compression
    implementation(group = "com.github.ziplet", name = "ziplet", version = "2.4.1")

    // Database
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-data-neo4j")
    /* Neo4J Bug prevents loading of migrations from jar archive, this is not problem for us because we extract the
       jar file in the Docker image. */
    implementation(group = "eu.michael-simons.neo4j", name = "neo4j-migrations-spring-boot-starter", version = "0.1.2")

    // MapStruct
    kapt(group = "org.mapstruct", name = "mapstruct-processor", version = "1.4.2.Final")
    implementation(group = "org.mapstruct", name = "mapstruct", version = "1.4.2.Final")

    // Utils
    implementation(group = "org.apache.commons", name = "commons-lang3", version = "3.12.0")
    runtimeOnly(group = "org.apache.logging.log4j", name = "log4j-web", version = "2.14.1")

    // Tests
    // Embedded neo4j database
    testImplementation(group = "org.neo4j.test", name = "neo4j-harness", version = "4.2.3")
    testImplementation(
        group = "org.neo4j.driver",
        name = "neo4j-java-driver-test-harness-spring-boot-autoconfigure",
        version = "4.1.1.1"
    )

    // Neo4J plugins
    testImplementation(files("./neo4j-plugins/neo4j-tintok-functions-0.1.jar"))

    // JUnit
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter")

    // Mocking and asserting
    testImplementation(group = "io.mockk", name = "mockk", version = "1.10.6")
    testImplementation(group = "com.ninja-squad", name = "springmockk", version = "3.0.1")
    testImplementation(group = "io.kotest", name = "kotest-assertions-json-jvm", version = "4.4.3")
    testImplementation(group = "io.kotest", name = "kotest-assertions-core-jvm", version = "4.4.3")

    // OpenAPI validation
    implementation(group = "com.atlassian.oai", name = "swagger-request-validator-springmvc", version = "2.15.1")
    testImplementation(group = "com.atlassian.oai", name = "swagger-request-validator-mockmvc", version = "2.15.1")

    // Spring testing
    testImplementation(group = "org.springframework.security", name = "spring-security-test")
    testImplementation(group = "org.springframework.boot", name = "spring-boot-starter-test")

    // Coroutines testing
    testImplementation(group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-test", version = "1.4.3")
}

tasks.withType<Test> {
    useJUnitPlatform()
    systemProperty("java.util.logging.config.file", "${project.buildDir}/resources/test/logging-test.properties")
    testLogging {
        showStandardStreams = true
    }

    // define property which can skip test execution. Use './gradlew -PskipTests comeGradleTask' to skip tests.
    onlyIf {
        !project.hasProperty("skipTests")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        // Use new compiler backend
        useIR = true
        // Make Kotlin use Java 11 build target
        jvmTarget = JavaVersion.VERSION_11.toString()
        // Strict constrain validation policy
        freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
    }
}

/**
 * Find only stable dependencies.
 */
fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = stableKeyword || regex.matches(version)
    return isStable.not()
}

// Configure dependency upgrade task.
tasks.withType<DependencyUpdatesTask> {
    checkForGradleUpdate = true
    outputFormatter = "json"
    outputDir = "build/dependencyUpdates"
    reportfileName = "report"
    gradleReleaseChannel = "current"

    rejectVersionIf {
        isNonStable(candidate.version)
    }
}

tasks {
    processIntegrationResources {
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
    }

    processResources {
        filesMatching("application.yml") {
            expand(project.properties)
        }
    }
}

kapt {
    // Fail on unmapped target properties for MapStruct code generation
    arguments {
        arg("mapstruct.unmappedTargetPolicy", "ERROR")
    }
    includeCompileClasspath = false
}

jacoco {
    toolVersion = "0.8.6"
}

tasks.jacocoTestReport {
    executionData(fileTree(project.buildDir).include("jacoco/*.exec"))
    reports {
        xml.isEnabled = true
        xml.destination = file("${project.buildDir}/reports/jacoco/test/jacocoTestReport.xml")
        csv.isEnabled = false
        html.destination = file("$buildDir/jacoco-test-reports")
    }
}

idea {
    module {
        val kaptSources = listOf(
            file("${project.buildDir}/generated/source/kapt/main"),
            file("${project.buildDir}/generated/source/kapt/test"),
            file("${project.buildDir}/generated/source/kapt/integration")
        )

        kaptSources.forEach { source ->
            sourceDirs.plusAssign(source)
            generatedSourceDirs.plusAssign(source)
        }
    }
}