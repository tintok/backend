## Requirements

All development is done purely on Linux, every other OS is not guaranteed to work with the following instructions.  
You also have to have `docker` and `docker-compose` installed on your system and available in `$PATH`.

## Checkout

The master reflects the currently working branch. To check out the project clone the repository.  
After cloning, you have to execute `git submodule update --init --recursive`.

## Code Documentation

Documentation can be achieved by executing `./gradlew dokkaHtml` for HTML or `./gradlew dokkajavaDoc`
for JavaDoc.  
Also, both are hosted on GitLab Pages for the current master:

- [HTML](https://tintok.gitlab.io/backend/dokka/html/-tin-tok/index.html)
- [JavaDoc](https://tintok.gitlab.io/backend/dokka/javadoc/index.html)

## Simply running the platform

Before getting started please change the initial database password in `docker/database_credentials.env`. `NEO4J_AUTH` and
`NEO4J_PASSWORD` have to be aligned. That means when you set `DATABASE_PASSWORD` to `test123` (which you should definitely
not, chose a secure password) `NEO4J_AUTH` should have the value "some_username/`test123`".  
After configuring the database, you also have to change the JWT seed and can enable additional modules by providing the
corresponding secrets. To do that, edit `docker/secrets.env` and fill in the corresponding values. You're finding all
existing configuration keys with a short description in the table [below](#application-secrets).  
To start the platform simply execute `./start-tintok.sh` which is located in the repositories root directory.  
This will spawn a Neo4J database docker container, and the TinTok backend docker container. The backend container exposes
port 8081 to all interfaces and is reachable from any other machine in your network, the database binds only to localhost. No
further configuration is required. Docker ensures that backend and database are already connected with the corresponding
username and password. The script only needs `docker` and `docker-compose` commands available in your `PATH`.   
Without the corresponding secrets, key components like the notification management with FCM are disabled, however, the
backend still starts. You can notice this notion by the missing `FirebaseMessaging initialized`,
`Hello from FcmController`, `Hello from ChatNotificationProducer`, `Hello from GoogleOAuth2TokenService`,
`Hello from GoogleOAuth2Controller`, `Hello from MatchNotificationProducer` log lines.

## Application Secrets

The following secrets are used and configured in our application:

| secret                                 | description                                                  | required | Feature             |
| :------------------------------------: | ------------------------------------------------------------ | :------: | :-----------------: |
| JWT_SECRET                             | JWT token seed                                               | yes      | JWT tokens          |
| SECURITY_OAUTH2_PROVIDER_GOOGLE_ID     | Google OAuth 2 id for login with Google                      | no       | Google OAuth2 Login |
| SECURITY_OAUTH2_PROVIDER_GOOGLE_SECRET | Google OAuth 2 secret for login with Google                  | no       | Google OAuth2 Login |
| SECURITY_FCM_PROJECTID                 | FCM Project ID for Android notifications via Google FCM      | no       | FCM Notifications   |
| SECURITY_FCM_APIKEY                    | FCM Project API key for Android notifications via Google FCM | no       | FCM Notifications   |

If a non required secret is not present while building, the corresponding functionality is deactivated in the backend and is
not available during execution. If a required secret is missing the backend does not start.

## Local development

You only need the database for local development. Please execute `docker-compose up -d` in the repositories root directory.
The test database will download and install the correct libraries automatically. After the database is running we can start
the backend in IntelliJ or by executing `./gradlew bootRun`.  
For local development the backend will bind to all available interfaces on port `8080`.

## Building the docker image

After executing `./gradlew build` you can build the `Dockerfile` in the repositories root directory. These are the same step
as used in our CI pipeline. Just execute `docker build -t registry.gitlab.com/tintok/backend:latest .`.