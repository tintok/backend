package de.iptk.groupj

import io.kotest.matchers.collections.shouldHaveSize
import javax.validation.Validation
import javax.validation.Validator

private val validator: Validator = Validation.buildDefaultValidatorFactory().validator

infix fun <T> T.shouldHaveNumberOfConstraintValidations(size: Int): T {
    val violations = validator.validate(this)
    violations shouldHaveSize size

    return this
}

fun <T> T.shouldNotHaveConstraintValidations(): T {
    val violations = validator.validate(this)
    violations shouldHaveSize 0

    return this
}

