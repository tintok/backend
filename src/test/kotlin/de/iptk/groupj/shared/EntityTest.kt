package de.iptk.groupj.shared

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Test

internal class EntityTest {

    @Test
    internal fun `test equal with same id`() {
        val entity1 = TestEntity(1)
        val entity2 = TestEntity(1)
        entity1 shouldBe entity2
    }

    @Test
    internal fun `test equal same object`() {
        val entity = TestEntity(1)
        entity shouldBe entity
    }

    @Test
    internal fun `test not equal with different object`() {
        val entity = TestEntity(1)
        entity shouldNotBe "some string"
    }

    @Test
    internal fun `test not equal with different id`() {
        val entity1 = TestEntity(1)
        val entity2 = TestEntity(2)
        entity1 shouldNotBe entity2
    }

    @Test
    internal fun `test hashcode returns id`() {
        val entity = TestEntity(1)
        entity.hashCode() shouldBe 1
    }
}

internal class TestEntity(override val id: Long) : Entity<Long>()