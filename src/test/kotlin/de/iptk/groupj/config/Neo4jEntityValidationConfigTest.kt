package de.iptk.groupj.config

import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import org.junit.jupiter.api.Test
import javax.validation.ConstraintViolationException
import javax.validation.constraints.NotBlank

internal class Neo4jEntityValidationConfigTest {

    private val validator = Neo4jEntityValidationConfig()

    @Test
    fun `valid entity`() {
        shouldNotThrowAny { validator.validateEntity(TestEntity("valid")) }
    }

    @Test
    fun `valid entities`() {
        shouldNotThrowAny { validator.validateEntities(listOf(TestEntity("valid"))) }
    }

    @Test
    fun `invalid entity`() {
        shouldThrow<ConstraintViolationException> { validator.validateEntity(TestEntity("  ")) }
    }

    @Test
    fun `invalid entities`() {
        shouldThrow<ConstraintViolationException> { validator.validateEntities(listOf(TestEntity("  "))) }
    }

}

internal data class TestEntity(@field:NotBlank val name: String)