package de.iptk.groupj.tag.localization

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import de.iptk.groupj.getObjectMapper
import de.iptk.groupj.tag.model.TagDto
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.context.i18n.LocaleContextHolder
import java.io.StringWriter
import java.io.Writer
import java.util.*


internal class TagLocalizerTest {

    private lateinit var jsonWriter: Writer
    private lateinit var jsonGenerator: JsonGenerator
    private lateinit var serializerProvider: SerializerProvider
    private lateinit var localizer: TagLocalizer

    @BeforeEach
    fun init() {
        val mapper = getObjectMapper()

        jsonWriter = StringWriter()
        jsonGenerator = JsonFactory().createGenerator(jsonWriter)
        serializerProvider = mapper.serializerProvider
        localizer = TagLocalizer(mapper)
    }

    @Test
    internal fun `test tag conversion to german`() {
        LocaleContextHolder.setLocaleContext { Locale.GERMAN }

        localizer.serialize(TagDto("politics"), jsonGenerator, serializerProvider)
        jsonGenerator.flush()
        jsonWriter.toString() shouldBe "\"politik\""
    }

    @Test
    internal fun `test tag conversion english`() {
        LocaleContextHolder.setLocaleContext { Locale.ENGLISH }

        localizer.serialize(TagDto("politics"), jsonGenerator, serializerProvider)
        jsonGenerator.flush()
        jsonWriter.toString() shouldBe "\"politics\""
    }

    @Test
    internal fun `test tag conversion on null context`() {
        LocaleContextHolder.setLocaleContext { null }

        localizer.serialize(TagDto("politics"), jsonGenerator, serializerProvider)
        jsonGenerator.flush()
        jsonWriter.toString() shouldBe "\"politics\""
    }

}