package de.iptk.groupj.tag.localization

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import de.iptk.groupj.getObjectMapper
import de.iptk.groupj.tag.model.TagDto
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.context.i18n.LocaleContextHolder
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.*


internal class TagDelocalizerTest {

    private val mapper = getObjectMapper()

    private lateinit var delocalizer: TagDelocalizer

    @BeforeEach
    fun init() {
        delocalizer = TagDelocalizer(mapper)
    }

    @Test
    internal fun `test tag conversion to german`() {
        LocaleContextHolder.setLocaleContext { Locale.GERMAN }
        val json = "\"politik\""

        val stream: InputStream = ByteArrayInputStream(json.toByteArray(StandardCharsets.UTF_8))
        val parser: JsonParser = mapper.factory.createParser(stream)
        val ctxt: DeserializationContext = mapper.deserializationContext

        delocalizer.deserialize(parser, ctxt) shouldBe TagDto("politics")
    }

    @Test
    internal fun `test tag conversion english`() {
        LocaleContextHolder.setLocaleContext { Locale.ENGLISH }

        val json = "\"politics\""

        val stream: InputStream = ByteArrayInputStream(json.toByteArray(StandardCharsets.UTF_8))
        val parser: JsonParser = mapper.factory.createParser(stream)
        val ctxt: DeserializationContext = mapper.deserializationContext

        delocalizer.deserialize(parser, ctxt) shouldBe TagDto("politics")
    }

    @Test
    internal fun `test no tag conversion for german tag with english locale`() {
        LocaleContextHolder.setLocaleContext { Locale.ENGLISH }

        val json = "\"politik\""

        val stream: InputStream = ByteArrayInputStream(json.toByteArray(StandardCharsets.UTF_8))
        val parser: JsonParser = mapper.factory.createParser(stream)
        val ctxt: DeserializationContext = mapper.deserializationContext

        delocalizer.deserialize(parser, ctxt) shouldBe TagDto("politik")
    }

    @Test
    internal fun `test tag conversion on null context`() {
        LocaleContextHolder.setLocaleContext { null }

        val json = "\"politics\""

        val stream: InputStream = ByteArrayInputStream(json.toByteArray(StandardCharsets.UTF_8))
        val parser: JsonParser = mapper.factory.createParser(stream)
        val ctxt: DeserializationContext = mapper.deserializationContext

        delocalizer.deserialize(parser, ctxt) shouldBe TagDto("politics")
    }

    @Test
    internal fun `test german tag conversion on null context`() {
        LocaleContextHolder.setLocaleContext { null }

        val json = "\"politik\""

        val stream: InputStream = ByteArrayInputStream(json.toByteArray(StandardCharsets.UTF_8))
        val parser: JsonParser = mapper.factory.createParser(stream)
        val ctxt: DeserializationContext = mapper.deserializationContext

        delocalizer.deserialize(parser, ctxt) shouldBe TagDto("politik")
    }

}