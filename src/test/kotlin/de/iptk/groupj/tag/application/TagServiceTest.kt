package de.iptk.groupj.tag.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.tag.adapter.database.TagRepository
import de.iptk.groupj.tag.mockTag
import de.iptk.groupj.tag.model.TagDto
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactly
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class TagServiceTest(@MockK private val tagRepository: TagRepository) {

    private lateinit var service: TagService

    @BeforeEach
    internal fun init() {
        service = TagService(tagRepository)
    }

    @Test
    internal fun `find known tag entities`() {
        val tag = mockTag()
        every { tagRepository.findAllByNameIn(setOf(tag.name)) } returns setOf(tag)
        val tags = service.findAllTagsByName(setOf(TagDto(tag.name)))

        tags.shouldContainExactly(tag)
    }

    @Test
    internal fun `fail on invalid tag entities`() {
        every { tagRepository.findAllByNameIn(any()) } returns setOf()

        shouldThrow<ResourceNotFoundException> { service.findAllTagsByName(setOf(TagDto("test"))) }
    }

}
