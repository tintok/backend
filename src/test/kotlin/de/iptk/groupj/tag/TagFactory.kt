package de.iptk.groupj.tag

import de.iptk.groupj.tag.domain.TagEntity
import kotlin.random.Random

fun mockTags() = mutableSetOf(mockTag())

fun mockTag(name: String = "Test Tag") = TagEntity(Random.nextLong(), name)

fun mockTags(vararg name: String) = name.map(::mockTag)