package de.iptk.groupj.tag.domain

import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test

class TagEntityTest {

    @Test
    internal fun `valid tag`() {
        TagEntity(0, "test").shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid tag with blank name`() {
        TagEntity(0, " ") shouldHaveNumberOfConstraintValidations 1
    }

}