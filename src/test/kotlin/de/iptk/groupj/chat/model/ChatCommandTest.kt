package de.iptk.groupj.chat.model

import de.iptk.groupj.shared.CHAT_MESSAGE_MAX_SIZE
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class ChatCommandTest {

    @Test
    internal fun `valid command`() {
        ChatCommand("Hello!").shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid blank test`() {
        ChatCommand("   ") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big message size`() {
        ChatCommand(StringUtils.repeat("a", CHAT_MESSAGE_MAX_SIZE + 1)) shouldHaveNumberOfConstraintValidations 1
    }

}