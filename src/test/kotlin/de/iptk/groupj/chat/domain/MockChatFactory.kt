package de.iptk.groupj.chat.domain

import de.iptk.groupj.chat.model.ConversationOverviewEntry
import de.iptk.groupj.chat.model.ConversationPartner
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.mockUser
import java.time.ZonedDateTime
import java.util.*

fun mockConversationOverview(): List<ConversationOverviewEntry> = listOf(
    mockConversationOverviewEntry(message = "Hey, whats popin?"),
    mockConversationOverviewEntry(message = "Ready.", partner = mockConversationPartner(name = "Dark Army"))
)

fun mockConversationOverviewEntry(
    partner: ConversationPartner = mockConversationPartner(),
    last: ZonedDateTime = ZonedDateTime.now(),
    unread: Int = 3,
    message: String = "Hey!",
    send: Boolean = false
): ConversationOverviewEntry = ConversationOverviewEntry(partner, last, unread, message, send)

fun mockConversationPartner(id: UUID = UUID.randomUUID(), name: String = "Elliot Alderson"): ConversationPartner =
    ConversationPartner(id, name)

fun mockConversation(): List<ChatMessageEntity> = listOf(
    mockChatMessage(message = "Whats up?"),
    mockChatMessage(message = "Hey!")
)

fun mockChatMessage(
    id: UUID = UUID.randomUUID(),
    message: String = "Hey!",
    created: ZonedDateTime = ZonedDateTime.now(),
    subject: UserEntity = mockUser(),
    send: Boolean = true,
    read: Boolean = true,
    partner: UserEntity = mockUser()
): ChatMessageEntity = ChatMessageEntity(id, message, created, subject, send, read, partner)