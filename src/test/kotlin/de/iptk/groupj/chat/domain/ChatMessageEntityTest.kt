package de.iptk.groupj.chat.domain

import de.iptk.groupj.shared.CHAT_MESSAGE_MAX_SIZE
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class ChatMessageEntityTest {

    @Test
    internal fun `valid chat message entity`() {
        mockChatMessage().shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid chat message because of blank message`() {
        mockChatMessage(message = "   ") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid chat message because message size too large`() {
        mockChatMessage(
            message = StringUtils.repeat(
                "x",
                CHAT_MESSAGE_MAX_SIZE + 1
            )
        ) shouldHaveNumberOfConstraintValidations 1
    }

}