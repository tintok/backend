package de.iptk.groupj.chat.application

import de.iptk.groupj.chat.adapter.database.ChatRepository
import de.iptk.groupj.chat.domain.ChatMessageEntity
import de.iptk.groupj.chat.domain.mockChatMessage
import de.iptk.groupj.chat.domain.mockConversation
import de.iptk.groupj.chat.eventing.ChatMessageEvent
import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactly
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.PageImpl
import java.util.*

@ExtendWith(MockKExtension::class)
internal class ChatServiceTest(

    @MockK
    private val repository: ChatRepository,

    @MockK
    private val userService: UserService,

    @MockK
    private val publisher: ApplicationEventPublisher

) {

    private lateinit var service: ChatService

    @BeforeEach
    internal fun init() {
        service = ChatService(repository, userService, publisher)
    }

    @Test
    internal fun `retrieve conversation`() {
        val partnerId = UUID.randomUUID()
        val conversation = mockConversation()

        every { userService.existsById(partnerId) } returns true
        every { repository.findConversation(any(), eq(partnerId), any()) } returns PageImpl(conversation)

        service.findConversation(mockUser(), partnerId, mockk()).content shouldContainExactly conversation
    }

    @Test
    internal fun `non existing partner in retrieve conversation should throw`() {
        every { userService.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> { service.findConversation(mockk(), mockk(), mockk()) }
    }

    @Test
    internal fun `delete conversation`() {
        val user = mockUser()
        val partnerId = UUID.randomUUID()

        every { userService.existsById(partnerId) } returns true
        every { repository.deleteConversation(user.username, partnerId) } returns true

        shouldNotThrowAny { service.deleteConversation(user, partnerId) }
    }

    @Test
    internal fun `non existing partner in delete conversation should throw`() {
        every { userService.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> { service.deleteConversation(mockUser(), UUID.randomUUID()) }
    }

    @Test
    internal fun `non existing conversation in delete conversation should throw`() {
        val user = mockUser()
        val partnerId = UUID.randomUUID()

        every { userService.existsById(partnerId) } returns true
        every { repository.deleteConversation(user.username, partnerId) } returns false

        shouldThrow<ResourceNotFoundException> { service.deleteConversation(user, partnerId) }
    }

    @Test
    internal fun `send chat`() {
        val me = mockUser()
        val recipientId = UUID.randomUUID()
        val recipient = mockUser(id = recipientId)
        val message = mockChatMessage()
        val invertedMessage = mockChatMessage()

        every { userService.findById(recipientId) } returns recipient
        every { repository.save(match(ChatMessageEntity::send)) } returns message
        every { repository.save(match { message -> !message.send }) } returns invertedMessage

        service.sendChatMessage(me, recipientId, message.message)

        // Verify send was done twice once normal and once inverted
        verify(exactly = 1) {
            repository.save(match {
                it.message == message.message && it.subject == me && it.partner == recipient && it.read && it.send
            })
        }
        verify(exactly = 1) {
            repository.save(match {
                it.message == message.message && it.subject == recipient && it.partner == me && !it.read && !it.send
            })
        }

        // Notification was produced for the inverted message
        verify(exactly = 1) {
            publisher.publishEvent(ChatMessageEvent(me, recipient, invertedMessage.id!!, invertedMessage.message))
        }
    }

    @Test
    internal fun `mark message as read`() {
        val user = mockUser()
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every { userService.existsById(partnerId) } returns true
        every { repository.markMessageAsRead(user.username, partnerId, messageId) } returns true

        shouldNotThrowAny { service.markChatMessageAsRead(user, partnerId, messageId) }
    }

    @Test
    internal fun `non existing partner in mark message as read should throw`() {
        every { userService.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> {
            service.markChatMessageAsRead(mockUser(), UUID.randomUUID(), UUID.randomUUID())
        }
    }

    @Test
    internal fun `non existing conversation in mark message as read should throw`() {
        val user = mockUser()
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every { userService.existsById(partnerId) } returns true
        every { repository.markMessageAsRead(user.username, partnerId, messageId) } returns false

        shouldThrow<ResourceNotFoundException> { service.markChatMessageAsRead(user, partnerId, messageId) }
    }

    @Test
    internal fun `delete message`() {
        val user = mockUser()
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every { userService.existsById(partnerId) } returns true
        every { repository.deleteMessage(user.username, partnerId, messageId) } returns true

        shouldNotThrowAny { service.deleteChatMessage(user, partnerId, messageId) }
    }

    @Test
    internal fun `non existing partner in delete message should throw`() {
        every { userService.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> {
            service.deleteChatMessage(mockUser(), UUID.randomUUID(), UUID.randomUUID())
        }
    }

    @Test
    internal fun `non existing conversation in delete message should throw`() {
        val user = mockUser()
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every { userService.existsById(partnerId) } returns true
        every { repository.deleteMessage(user.username, partnerId, messageId) } returns false

        shouldThrow<ResourceNotFoundException> { service.deleteChatMessage(user, partnerId, messageId) }
    }

}