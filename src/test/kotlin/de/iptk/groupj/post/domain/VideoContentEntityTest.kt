package de.iptk.groupj.post.domain

import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test


internal class VideoContentEntityTest {

    @Test
    internal fun `valid video content`() {
        mockVideoContent().shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid video content`() {
        mockVideoContent(mimeType = "image/jpg") shouldHaveNumberOfConstraintValidations 1
    }

}