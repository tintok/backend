package de.iptk.groupj.post.domain

import de.iptk.groupj.post.mockVideoPost
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import org.junit.jupiter.api.Test
import java.util.*

internal class ContentEntityTest {

    @Test
    fun `test null id is invalid`() {
        val content = VideoContentEntity(null, 1337, "video/mp4", mockVideoPost(), 0)
        content shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    fun `test null size is invalid`() {
        val content = VideoContentEntity(UUID.randomUUID(), null, "video/mp4", mockVideoPost(), 0)
        content shouldHaveNumberOfConstraintValidations 1
    }

}