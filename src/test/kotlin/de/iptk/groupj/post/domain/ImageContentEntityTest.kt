package de.iptk.groupj.post.domain

import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test

internal class ImageContentEntityTest {

    @Test
    internal fun `valid image content`() {
        mockImageContent().shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid image content`() {
        mockImageContent(mimeType = "video/mp4") shouldHaveNumberOfConstraintValidations 1
    }

}