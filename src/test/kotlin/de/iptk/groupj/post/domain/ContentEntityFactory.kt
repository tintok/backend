package de.iptk.groupj.post.domain

import de.iptk.groupj.post.mockImagePost
import de.iptk.groupj.post.mockVideoPost
import org.springframework.http.MediaType
import java.util.*

fun mockImageContent(
    id: UUID = UUID.randomUUID(),
    size: Long = 1337,
    mimeType: String = MediaType.IMAGE_PNG_VALUE,
    post: ImagePostEntity = mockImagePost(),
    version: Long = 0
) = ImageContentEntity(id, size, mimeType, post, version)

fun mockVideoContent(
    id: UUID = UUID.randomUUID(),
    size: Long = 1337,
    mimeType: String = "video/mp4",
    post: VideoPostEntity = mockVideoPost(),
    version: Long = 0
) = VideoContentEntity(id, size, mimeType, post, version)