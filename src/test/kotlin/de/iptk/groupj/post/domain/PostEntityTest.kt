package de.iptk.groupj.post.domain

import de.iptk.groupj.post.mockImagePost
import de.iptk.groupj.post.mockTextPost
import de.iptk.groupj.post.mockVideoPost
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test

class PostEntityTest {

    // Text
    @Test
    internal fun `valid text post`() {
        mockTextPost().shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid text post with blank text`() {
        mockTextPost(text = "     ") shouldHaveNumberOfConstraintValidations 1
    }

    // Image
    @Test
    internal fun `valid image post`() {
        mockImagePost(text = null).shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `valid image post with text`() {
        mockImagePost().shouldNotHaveConstraintValidations()
    }

    // Video
    @Test
    internal fun `valid video post without text`() {
        mockVideoPost(text = null).shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `valid video post with text`() {
        mockVideoPost().shouldNotHaveConstraintValidations()
    }

    // Tags
    @Test
    internal fun `invalid post without tags`() {
        mockTextPost(tags = mutableSetOf()) shouldHaveNumberOfConstraintValidations 1
    }

}