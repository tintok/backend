package de.iptk.groupj.post.model

import de.iptk.groupj.shared.POST_TAG_MAX_SIZE
import de.iptk.groupj.shared.POST_TEXT_MAX_SIZE
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import de.iptk.groupj.tag.model.TagDto
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class UpdatePostCommandTest {

    @Test
    internal fun `valid update post command`() {
        UpdatePostCommand("test", setOf(TagDto("tag"))).shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `valid null text`() {
        UpdatePostCommand(null, setOf(TagDto("tag"))).shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid to big text`() {
        UpdatePostCommand(
            StringUtils.repeat("x", POST_TEXT_MAX_SIZE + 1),
            setOf(TagDto("tag"))
        ) shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid empty tags`() {
        UpdatePostCommand("test", setOf()) shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to many tags`() {
        val tags = IntRange(0, POST_TAG_MAX_SIZE + 1).map(Int::toString).map(::TagDto).toSet()
        val command = UpdatePostCommand("test", tags)
        command shouldHaveNumberOfConstraintValidations 1
    }

}