package de.iptk.groupj.post.model

import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.shared.POST_TAG_MAX_SIZE
import de.iptk.groupj.shared.POST_TEXT_MAX_SIZE
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import de.iptk.groupj.tag.model.TagDto
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class CreatePostCommandTest {

    @Nested
    inner class PostCommand {
        @Test
        internal fun `invalid empty tags`() {
            val command = CreateTextPostCommand(Location(0.0, 0.0), setOf(), "test")
            command shouldHaveNumberOfConstraintValidations 1
        }

        @Test
        internal fun `invalid to many tags`() {
            val tags = IntRange(0, POST_TAG_MAX_SIZE + 1).map(Int::toString).map(::TagDto).toSet()
            val command = CreateTextPostCommand(Location(0.0, 0.0), tags, "test")
            command shouldHaveNumberOfConstraintValidations 1
        }

        @Test
        internal fun `invalid to large text`() {
            val command = CreateTextPostCommand(
                Location(0.0, 0.0),
                setOf(TagDto("tag")),
                StringUtils.repeat("x", POST_TEXT_MAX_SIZE + 1)
            )
            command shouldHaveNumberOfConstraintValidations 1
        }
    }

    @Nested
    inner class TextPostCommand {
        @Test
        internal fun `valid command`() {
            val command = CreateTextPostCommand(Location(0.0, 0.0), setOf(TagDto("tag")), "test")
            command.shouldNotHaveConstraintValidations()
        }

        @Test
        internal fun `invalid blank text`() {
            val command = CreateTextPostCommand(Location(0.0, 0.0), setOf(TagDto("tag")), "   ")
            command shouldHaveNumberOfConstraintValidations 1
        }
    }

    @Nested
    inner class PostWithContentCommand {
        @Test
        internal fun `valid command`() {
            val command = CreatePostWithContentCommand(Location(0.0, 0.0), setOf(TagDto("tag")), "this is a test")
            command.shouldNotHaveConstraintValidations()
        }

        @Test
        internal fun `valid null text`() {
            val command = CreatePostWithContentCommand(Location(0.0, 0.0), setOf(TagDto("tag")), null)
            command.shouldNotHaveConstraintValidations()
        }
    }

}