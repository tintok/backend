package de.iptk.groupj.post.model

import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test

internal class LocationTest {

    @Test
    internal fun `valid location`() {
        Location(0.0, 0.0).shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid location with exceeding latitude to big`() {
        Location(91.0, 0.0) shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid location with exceeding longitude to big`() {
        Location(0.0, 181.0) shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid location with exceeding latitude to small`() {
        Location(-91.0, 0.0) shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid location with exceeding longitude to small`() {
        Location(0.0, -181.0) shouldHaveNumberOfConstraintValidations 1
    }

}