package de.iptk.groupj.post.mapper

import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.post.domain.PostType
import de.iptk.groupj.post.mockImagePost
import de.iptk.groupj.post.mockTextPost
import de.iptk.groupj.post.model.*
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.tag.mockTag
import de.iptk.groupj.tag.model.TagDto
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.neo4j.driver.Values

internal class PostMapperTest {

    private val mapper: PostMapper = PostMapper.INSTANCE

    @Test
    internal fun `command to text post`() {
        val tag = mockTag()
        val command = CreateTextPostCommand(Location(0.0, 0.0), setOf(TagDto(tag.name)), "test")
        val mapping = mapper.mapToTextPostEntity(command, setOf(tag))

        assertSoftly(mapping) {
            location shouldBe command.location.asPoint()
            text shouldBe command.text
            tags shouldHaveSize 1
            mapping.tags.first() shouldBe tag
        }
    }

    @Test
    internal fun `command to image post`() {
        val tag = mockTag()
        val command = CreatePostWithContentCommand(Location(0.0, 0.0), setOf(TagDto(tag.name)), "test")
        val mapping = mapper.mapToImagePostEntity(command, setOf(tag))

        assertSoftly(mapping) {
            location shouldBe command.location.asPoint()
            text shouldBe command.text
            tags shouldHaveSize 1
            mapping.tags.first() shouldBe tag
        }
    }

    @Test
    internal fun `command to video post`() {
        val tag = mockTag()
        val command = CreatePostWithContentCommand(Location(0.0, 0.0), setOf(TagDto(tag.name)), "test")
        val mapping = mapper.mapToVideoPostEntity(command, setOf(tag))

        assertSoftly(mapping) {
            location shouldBe command.location.asPoint()
            text shouldBe command.text
            tags shouldHaveSize 1
            mapping.tags.first() shouldBe tag
        }
    }

    @Test
    internal fun `update text post`() {
        val post = mockTextPost()
        val text = "updated"
        val newTag = "new tag"
        val command = UpdatePostCommand(text, setOf(TagDto(newTag)))
        mapper.updatePost(command, setOf(TagEntity(1, newTag)), post)

        assertSoftly(post) {
            this.text shouldBe text
            tags shouldHaveSize 1
            tags.first().name shouldBe newTag
        }
    }

    @Test
    internal fun `update post empty text`() {
        val post = mockImagePost()
        val newTag = "new tag"
        val command = UpdatePostCommand(null, setOf(TagDto(newTag)))
        mapper.updatePost(command, setOf(TagEntity(1, newTag)), post)

        assertSoftly(post) {
            text.shouldBeNull()
            tags shouldHaveSize 1
            tags.first().name shouldBe newTag
        }
    }

    @Test
    internal fun `map to post dto`() {
        val post = mockTextPost()
        val expectedMapping = PostDto(
            post.id!!,
            PostMapper.INSTANCE.mapPointToLocation(post.location),
            post.text,
            setOf(TagDto(post.tags.first().name)),
            post.created!!,
            post.updated!!,
            PostType.TEXT
        )

        mapper.mapToPostDto(post) shouldBe expectedMapping
    }

    @Test
    internal fun `map to author dto`() {
        val author = mockUser()
        val expectedMapping = AuthorDto(author.id!!, author.givenName + " " + author.familyName)

        mapper.mapToAuthorDto(author) shouldBe expectedMapping
    }

    @Test
    internal fun `map location to Point`() {
        val location = Location(69.1, 122.0)
        val expectedPoint = Values.point(4326, 122.0, 69.1).asPoint()

        PostMapper.INSTANCE.mapLocationToPoint(location) shouldBe expectedPoint
    }

    @Test
    internal fun `map Point to Location`() {
        val point = Values.point(4326, 122.0, 69.1).asPoint()
        val expectedLocation = Location(69.1, 122.0)

        PostMapper.INSTANCE.mapPointToLocation(point) shouldBe expectedLocation
    }

}