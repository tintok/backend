package de.iptk.groupj.post.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.post.adapter.database.ContentRepository
import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.post.eventing.PostInteractionEvent
import de.iptk.groupj.post.eventing.PostInteractionEventType
import de.iptk.groupj.post.exception.PostToLikeIsOwnPost
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.throwables.shouldThrow
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ApplicationEventPublisher
import java.util.*

@ExtendWith(MockKExtension::class)
class PostServiceTest(

    @MockK
    private val repository: PostRepository,

    @MockK
    private val contentRepository: ContentRepository,

    @MockK
    private val publisher: ApplicationEventPublisher

) {

    private lateinit var service: PostService

    @BeforeEach
    internal fun init() {
        service = PostService(repository, contentRepository, mockk(), publisher)
    }

    @Test
    internal fun `non existing post`() {
        val id = UUID.randomUUID()
        every { repository.findById(id) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.findPost(id) }
    }

    @Test
    internal fun `non existing image post data`() {
        val id = UUID.randomUUID()
        every { contentRepository.findImageContent(id) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.findImagePostData(id) }
    }

    @Test
    internal fun `non existing video post data`() {
        val id = UUID.randomUUID()
        every { contentRepository.findVideoContent(id) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.findVideoPostData(id) }
    }

    @Test
    internal fun `post like event is issued on like`() {
        val user = mockUser()
        val postId = UUID.randomUUID()

        every { repository.existsById(any()) } returns true
        every { repository.isPostFromUser(any(), any()) } returns false

        service.like(postId, user)
        val expectedEvent = PostInteractionEvent(user, postId, PostInteractionEventType.LIKED)
        verify(exactly = 1) { publisher.publishEvent(expectedEvent) }
    }

    @Test
    internal fun `post dislike event is issued on dislike`() {
        val user = mockUser()
        val postId = UUID.randomUUID()

        every { repository.existsById(any()) } returns true
        every { repository.isPostFromUser(any(), any()) } returns false

        service.dislike(postId, user)
        val expectedEvent = PostInteractionEvent(user, postId, PostInteractionEventType.DISLIKED)
        verify(exactly = 1) { publisher.publishEvent(expectedEvent) }
    }

    @Test
    internal fun `post clear event is issued on clear`() {
        val user = mockUser()
        val postId = UUID.randomUUID()

        every { repository.existsById(any()) } returns true
        every { repository.deleteLikeOrDislike(any(), any()) } returns true

        service.clear(postId, user)
        val expectedEvent = PostInteractionEvent(user, postId, PostInteractionEventType.CLEARED)
        verify(exactly = 1) { publisher.publishEvent(expectedEvent) }
    }

    @Test
    internal fun `test like for non existing post`() {
        every { repository.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> { service.like(UUID.randomUUID(), mockUser()) }
    }

    @Test
    internal fun `test like for your own post`() {
        every { repository.existsById(any()) } returns true
        every { repository.isPostFromUser(any(), any()) } returns true

        shouldThrow<PostToLikeIsOwnPost> { service.like(UUID.randomUUID(), mockUser()) }
    }

    @Test
    internal fun `test dislike for non existing post`() {
        every { repository.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> { service.dislike(UUID.randomUUID(), mockUser()) }
    }

    @Test
    internal fun `test dislike for your own post`() {
        every { repository.existsById(any()) } returns true
        every { repository.isPostFromUser(any(), any()) } returns true

        shouldThrow<PostToLikeIsOwnPost> { service.dislike(UUID.randomUUID(), mockUser()) }
    }

    @Test
    internal fun `test clear for non existing post`() {
        every { repository.existsById(any()) } returns false

        shouldThrow<ResourceNotFoundException> { service.clear(UUID.randomUUID(), mockUser()) }
    }
}