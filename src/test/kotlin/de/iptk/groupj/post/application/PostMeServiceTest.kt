package de.iptk.groupj.post.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.throwables.shouldThrow
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
class PostMeServiceTest(@MockK private val repository: PostRepository) {

    private lateinit var service: PostMeService

    @BeforeEach
    internal fun init() {
        service = PostMeService(repository, mockk(), mockk(), mockk())
    }

    @Test
    internal fun `update non existing post`() {
        val id = UUID.randomUUID()
        val user = mockUser()
        every { repository.findByIdAndAuthorUsername(id, user.username) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.update(id, user, mockk(), mockk()) }
    }

    @Test
    internal fun `delete non existing image post data`() {
        val id = UUID.randomUUID()
        val user = mockUser()
        every { repository.findByIdAndAuthorUsername(id, user.username) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.delete(id, user) }
    }

}