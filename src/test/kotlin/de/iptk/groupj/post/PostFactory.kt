package de.iptk.groupj.post

import de.iptk.groupj.post.domain.*
import de.iptk.groupj.post.model.AuthorDto
import de.iptk.groupj.post.model.PostFeedDto
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.tag.mockTags
import de.iptk.groupj.tag.model.TagDto
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.mockUser
import java.time.ZonedDateTime
import java.util.*

fun mockTextPost(
    id: UUID = UUID.randomUUID(),
    location: Location = Location(42.0, 69.0),
    text: String? = "text",
    tags: MutableSet<TagEntity> = mockTags(),
    created: ZonedDateTime = ZonedDateTime.now(),
    updated: ZonedDateTime = ZonedDateTime.now(),
    author: UserEntity = mockUser()
) = TextPostEntity(id, location.asPoint(), text, tags, created, updated, author)

fun mockImagePost(
    id: UUID = UUID.randomUUID(),
    location: Location = Location(42.0, 69.0),
    text: String? = "image",
    tags: MutableSet<TagEntity> = mockTags(),
    created: ZonedDateTime = ZonedDateTime.now(),
    updated: ZonedDateTime = ZonedDateTime.now(),
    author: UserEntity = mockUser()
) = ImagePostEntity(id, location.asPoint(), text, tags, created, updated, author)

fun mockVideoPost(
    id: UUID = UUID.randomUUID(),
    location: Location = Location(42.0, 69.0),
    text: String? = "image",
    tags: MutableSet<TagEntity> = mockTags(),
    created: ZonedDateTime = ZonedDateTime.now(),
    updated: ZonedDateTime = ZonedDateTime.now(),
    author: UserEntity = mockUser()
) = VideoPostEntity(id, location.asPoint(), text, tags, created, updated, author)

fun PostEntity.toFeedPostDto() = toFeedPostDto(0, 0, PostFeedStatus.NEUTRAL)

fun PostEntity.toFeedPostDto(likes: Long, dislikes: Long, status: PostFeedStatus) = PostFeedDto(
    this.id!!,
    Location(location.y(), location.x()),
    this.text,
    this.tags.map(TagEntity::name).map(::TagDto).toSet(),
    this.created!!,
    this.updated!!,
    when (this) {
        is TextPostEntity -> PostType.TEXT
        is ImagePostEntity -> PostType.IMAGE
        is VideoPostEntity -> PostType.VIDEO
    },
    AuthorDto(this.author!!.id!!, this.author!!.givenName + " " + this.author!!.familyName),
    likes,
    dislikes,
    status
)