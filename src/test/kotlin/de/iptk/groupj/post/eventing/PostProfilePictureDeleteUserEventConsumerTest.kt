package de.iptk.groupj.post.eventing

import de.iptk.groupj.post.application.PostService
import de.iptk.groupj.post.domain.mockVideoContent
import de.iptk.groupj.user.eventing.UserDeleteEvent
import de.iptk.groupj.user.eventing.UserDeletedEvent
import de.iptk.groupj.user.mockUser
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.slot
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ApplicationEventPublisher

@ExtendWith(MockKExtension::class)
internal class PostProfilePictureDeleteUserEventConsumerTest(

    @MockK
    private val publisher: ApplicationEventPublisher,

    @MockK
    private val postService: PostService

) {

    private lateinit var consumer: PostDeleteUserEventConsumer

    @BeforeEach
    internal fun init() {
        consumer = PostDeleteUserEventConsumer(publisher, postService)
    }

    @Test
    internal fun `test user delete event`() {
        val user = mockUser()
        val consumedEvent = UserDeleteEvent(user)
        val publishedEvent = slot<UserDeletedEvent>()
        val content = setOf(mockVideoContent())

        justRun { publisher.publishEvent(capture(publishedEvent)) }
        every { postService.findAllContentFromUser(user) } returns content

        consumer.onUserDeleteEvent(consumedEvent)

        publishedEvent.captured shouldBe UserDeletedEvent(content)
    }

    @Test
    internal fun `test user after delete event`() {
        val content = setOf(mockVideoContent())

        justRun { postService.deleteContent(content) }

        consumer.afterUserDeletedEvent(UserDeletedEvent(content))
    }

}