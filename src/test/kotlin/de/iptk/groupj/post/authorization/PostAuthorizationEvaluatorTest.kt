package de.iptk.groupj.post.authorization

import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.user.mockUser
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
internal class PostAuthorizationEvaluatorTest(@MockK private val repository: PostRepository) {

    private lateinit var evaluator: PostAuthorizationEvaluator

    @BeforeEach
    fun init() {
        evaluator = PostAuthorizationEvaluator(repository)
    }

    @Test
    internal fun `is own post should yield true`() {
        val postId = UUID.randomUUID()
        val user = mockUser()

        every { repository.existsById(postId) } returns true
        every { repository.isPostFromUser(postId, user.username) } returns true
        every { repository.isPostFromMatch(postId, user.username) } returns false
        every { repository.isRecommendedPost(postId, user.username) } returns false

        evaluator.isMyPostOrFromMatchOrRecommended(user, postId).shouldBeTrue()
    }

    @Test
    internal fun `is post from match should yield true`() {
        val postId = UUID.randomUUID()
        val user = mockUser()

        every { repository.existsById(postId) } returns true
        every { repository.isPostFromUser(postId, user.username) } returns false
        every { repository.isPostFromMatch(postId, user.username) } returns true
        every { repository.isRecommendedPost(postId, user.username) } returns false

        evaluator.isMyPostOrFromMatchOrRecommended(user, postId).shouldBeTrue()
    }

    @Test
    internal fun `is recommended post should yield true`() {
        val postId = UUID.randomUUID()
        val user = mockUser()

        every { repository.existsById(postId) } returns true
        every { repository.isPostFromUser(postId, user.username) } returns false
        every { repository.isPostFromMatch(postId, user.username) } returns false
        every { repository.isRecommendedPost(postId, user.username) } returns true

        evaluator.isMyPostOrFromMatchOrRecommended(user, postId).shouldBeTrue()
    }

    @Test
    internal fun `post without access should yield false`() {
        val postId = UUID.randomUUID()
        val user = mockUser()

        every { repository.existsById(postId) } returns true
        every { repository.isPostFromUser(postId, user.username) } returns false
        every { repository.isPostFromMatch(postId, user.username) } returns false
        every { repository.isRecommendedPost(postId, user.username) } returns false

        evaluator.isMyPostOrFromMatchOrRecommended(user, postId).shouldBeFalse()
    }

    @Test
    internal fun `non existing post should yield true`() {
        val postId = UUID.randomUUID()
        val user = mockUser()

        every { repository.existsById(postId) } returns false

        evaluator.isMyPostOrFromMatchOrRecommended(user, postId).shouldBeTrue()
    }

}