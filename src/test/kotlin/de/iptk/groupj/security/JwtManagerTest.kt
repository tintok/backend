package de.iptk.groupj.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import de.iptk.groupj.security.application.JwtManager
import de.iptk.groupj.security.config.JwtConfig
import de.iptk.groupj.security.exception.InvalidTokenException
import de.iptk.groupj.shared.VALIDITY_NUMBER_OF_WEEKS
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.apache.commons.lang3.time.DateUtils
import org.junit.jupiter.api.Test
import java.time.Duration
import java.util.*


internal class JwtManagerTest {

    private val jwtConfig = JwtConfig("secret", Duration.ofDays(VALIDITY_NUMBER_OF_WEEKS * 7L))
    private val jwtManager = JwtManager(jwtConfig)
    private val signAlgo: Algorithm = Algorithm.HMAC256(jwtConfig.secret)

    @Test
    internal fun `validate valid token`() {
        val token = JWT.create()
            .withIssuer("TinTok")
            .withSubject("elliotalderson@protonmail.ch")
            .withIssuedAt(Date())
            .withExpiresAt(DateUtils.addWeeks(Date(), VALIDITY_NUMBER_OF_WEEKS))
            .withClaim("id", UUID.randomUUID().toString())
            .sign(signAlgo)

        shouldNotThrowAny { jwtManager.extractUsername(token) }
    }

    @Test
    internal fun `username and id are set correct`() {
        val id = UUID.randomUUID()
        val username = "elliotalderson@protonmail.ch"
        val token = jwtManager.createToken(username, id)

        val verifier: JWTVerifier = JWT.require(signAlgo)
            .withIssuer("TinTok")
            .build()
        val jwt: DecodedJWT = verifier.verify(token)

        jwt.subject shouldBe username
        jwt.claims["id"]!!.asString() shouldBe id.toString()
    }

    @Test
    internal fun `test username from token`() {
        val token = JWT.create()
            .withIssuer("TinTok")
            .withSubject("elliotalderson@protonmail.ch")
            .withIssuedAt(Date())
            .withExpiresAt(DateUtils.addWeeks(Date(), VALIDITY_NUMBER_OF_WEEKS))
            .withClaim("id", UUID.randomUUID().toString())
            .sign(signAlgo)

        jwtManager.extractUsername(token) shouldBe "elliotalderson@protonmail.ch"
    }

    @Test
    internal fun `test invalid token throws Exception`() {
        val token = jwtManager.createToken("elliotalderson@protonmail.ch", UUID.randomUUID())

        shouldThrow<InvalidTokenException> { jwtManager.extractUsername(token.substring(1)) }
    }

    @Test
    internal fun `test refresh`() {
        val subject = "elliotalderson@protonmail.ch"
        val token = JWT.create()
            .withIssuer("TinTok")
            .withSubject(subject)
            .withIssuedAt(Date())
            .withExpiresAt(DateUtils.addWeeks(Date(), VALIDITY_NUMBER_OF_WEEKS))
            .withClaim("id", UUID.randomUUID().toString())
            .sign(signAlgo)

        val refreshedToken = jwtManager.refresh(token)
        jwtManager.extractUsername(refreshedToken) shouldBe subject
    }

    @Test
    internal fun `test expired token not older then two weeks`() {
        val token = JWT.create()
            .withIssuer("TinTok")
            .withSubject("elliotalderson@protonmail.ch")
            .withIssuedAt(DateUtils.addWeeks(Date(), -VALIDITY_NUMBER_OF_WEEKS))
            .withExpiresAt(DateUtils.addHours(Date(), -VALIDITY_NUMBER_OF_WEEKS))
            .withClaim("id", UUID.randomUUID().toString())
            .sign(signAlgo)

        shouldNotThrowAny { jwtManager.refresh(token) }
    }

    @Test
    internal fun `test expired token older then two weeks throws`() {
        val token = JWT.create()
            .withIssuer("TinTok")
            .withSubject("elliotalderson@protonmail.ch")
            .withIssuedAt(DateUtils.addWeeks(Date(), -4))
            .withExpiresAt(DateUtils.addWeeks(Date(), -3))
            .withClaim("id", UUID.randomUUID().toString())
            .sign(signAlgo)

        shouldThrow<InvalidTokenException> { jwtManager.refresh(token) }
    }
}