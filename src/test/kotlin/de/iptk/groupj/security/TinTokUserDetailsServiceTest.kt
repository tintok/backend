package de.iptk.groupj.security

import de.iptk.groupj.security.application.TinTokUserDetailsService
import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.security.core.userdetails.UsernameNotFoundException
import java.util.*

@ExtendWith(MockKExtension::class)
internal class TinTokUserDetailsServiceTest(@MockK private val repository: UserRepository) {

    private lateinit var service: TinTokUserDetailsService


    @BeforeEach
    internal fun init() {
        service = TinTokUserDetailsService(repository)
    }

    @Test
    internal fun `load existing user`() {
        every { repository.findByUsername(any()) } returns Optional.of(mockUser())

        shouldNotThrowAny { service.loadUserByUsername("Exists") }
    }

    @Test
    internal fun `load non existing user`() {
        every { repository.findByUsername(any()) } returns Optional.empty()

        shouldThrow<UsernameNotFoundException> { service.loadUserByUsername("Does not exists") }
    }

}