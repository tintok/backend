package de.iptk.groupj.security

import de.iptk.groupj.security.application.JwtAuthenticationProvider
import de.iptk.groupj.security.application.JwtManager
import de.iptk.groupj.security.model.JwtAuthenticationToken
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.security.core.userdetails.UserDetailsService


@ExtendWith(MockKExtension::class)
internal class JwtAuthenticationProviderTest(

    @MockK
    private val userDetailsService: UserDetailsService,

    @MockK
    private val jwtManager: JwtManager

) {

    private lateinit var provider: JwtAuthenticationProvider

    @BeforeEach
    internal fun init() {
        provider = JwtAuthenticationProvider(userDetailsService, jwtManager)
    }

    @Test
    internal fun `valid authentication`() {
        val token = "some-token"
        val username = "elliotalderson@protonmail.ch"
        val user = mockUser()
        every { jwtManager.extractUsername(token) } returns username
        every { userDetailsService.loadUserByUsername(username) } returns user

        val jwtToken = JwtAuthenticationToken(token, mockk())
        jwtToken.isAuthenticated.shouldBeFalse()

        val authentication = provider.authenticate(jwtToken)
        assertSoftly(authentication) {
            credentials shouldBe token
            details shouldBe jwtToken.details
            isAuthenticated.shouldBeTrue()
            principal shouldBe user
            authorities.shouldContainExactly(*user.authorities.toTypedArray())
        }
    }

}