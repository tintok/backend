package de.iptk.groupj.security

import de.iptk.groupj.security.filter.JwtAuthenticationProviderFilter
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpHeaders
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletResponse

@ExtendWith(MockKExtension::class)
internal class JwtAuthenticationProviderFilterTest(

    @MockK
    private val response: HttpServletResponse,

    @MockK(relaxed = true)
    private val chain: FilterChain

) {

    @AfterEach
    internal fun tearDown() = SecurityContextHolder.clearContext()

    @Test
    internal fun `auth is set on correct token`() {
        val request = MockHttpServletRequest("GET", "http://tintok.de").apply {
            addHeader(HttpHeaders.AUTHORIZATION, "Bearer some-token")
            servletPath = "/info"
        }
        RequestContextHolder.setRequestAttributes(ServletRequestAttributes(request))

        val jwtFilter = JwtAuthenticationProviderFilter("/foo")
        jwtFilter.doFilter(request, response, chain)
        verify(exactly = 1) { chain.doFilter(request, response) }

        val auth: Authentication = SecurityContextHolder.getContext().authentication
        auth.credentials shouldBe "some-token"
    }

    @Test
    internal fun `filter is not invoked on certain path`() {
        val request = MockHttpServletRequest("GET", "http://tintok.de").apply {
            addHeader(HttpHeaders.AUTHORIZATION, "Bearer some-token")
            servletPath = "/foo"
        }
        RequestContextHolder.setRequestAttributes(ServletRequestAttributes(request))

        val jwtFilter = JwtAuthenticationProviderFilter("/foo")
        jwtFilter.doFilter(request, response, chain)
        verify(exactly = 1) { chain.doFilter(request, response) }

        SecurityContextHolder.getContext().authentication.shouldBeNull()
    }

    @Test
    internal fun `auth is not set on invalid token`() {
        val request = MockHttpServletRequest("GET", "http://tintok.de").apply {
            addHeader(HttpHeaders.AUTHORIZATION, "some-token")
            servletPath = "/info"
        }
        RequestContextHolder.setRequestAttributes(ServletRequestAttributes(request))

        val jwtFilter = JwtAuthenticationProviderFilter("/foo")
        jwtFilter.doFilter(request, response, chain)
        verify(exactly = 1) { chain.doFilter(request, response) }

        SecurityContextHolder.getContext().authentication.shouldBeNull()
    }

    @Test
    internal fun `auth is not set on token not present`() {
        val request = MockHttpServletRequest("GET", "http://tintok.de").apply {
            servletPath = "/info"
        }
        RequestContextHolder.setRequestAttributes(ServletRequestAttributes(request))

        val jwtFilter = JwtAuthenticationProviderFilter("/foo")
        jwtFilter.doFilter(request, response, chain)
        verify(exactly = 1) { chain.doFilter(request, response) }

        SecurityContextHolder.getContext().authentication.shouldBeNull()
    }

}