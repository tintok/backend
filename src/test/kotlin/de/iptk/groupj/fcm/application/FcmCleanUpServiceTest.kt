package de.iptk.groupj.fcm.application

import de.iptk.groupj.user.adapter.database.UserRepository
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
internal class FcmCleanUpServiceTest(

    @MockK
    private val messaging: FirebaseMessagingService,

    @MockK
    private val repository: UserRepository

) {

    private val service = FcmCleanUpService(messaging, repository)

    @Test
    internal fun `unset only non existing groups`() {
        val nonExistingId = UUID.randomUUID()
        val existingId = UUID.randomUUID()

        every { messaging.doesGroupExists(nonExistingId.toString()) } returns false
        every { messaging.doesGroupExists(existingId.toString()) } returns true
        every { repository.findAllUserIdsWhereFcmTokenIsNotNull() } returns setOf(nonExistingId, existingId)

        service.unsetNonExistingFcmGroups()
        verify(exactly = 1) { repository.removeFcmTokenForUserIdIn(setOf(nonExistingId)) }
    }

    @Test
    internal fun `unset is not called for all groups exist`() {
        val existingId = UUID.randomUUID()

        every { messaging.doesGroupExists(existingId.toString()) } returns true
        every { repository.findAllUserIdsWhereFcmTokenIsNotNull() } returns setOf(existingId)

        service.unsetNonExistingFcmGroups()
        verify(exactly = 0) { repository.removeFcmTokenForUserIdIn(setOf(existingId)) }
    }

}