package de.iptk.groupj.fcm.application

import de.iptk.groupj.fcm.exception.InvalidFcmTokenException
import de.iptk.groupj.fcm.model.GroupCreatedDto
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.Response

@ExtendWith(MockKExtension::class)
internal class FirebaseMessagingServiceTest(@MockK private val api: GoogleFcmGroupApi) {

    private lateinit var messaging: FirebaseMessagingService

    @BeforeEach
    internal fun init() {
        messaging = FirebaseMessagingService(api)
    }

    @Test
    internal fun `create group`() { // a coroutine with an extra test control
        val token = "bla"
        val response: Response<GroupCreatedDto> = mockk()

        every { response.isSuccessful } returns true
        every { response.body() } returns GroupCreatedDto(token)
        coEvery { api.create(any()) } returns response

        messaging.createGroup("name", token) shouldBe token
    }

    @Test
    internal fun `create group with invalid token`() {
        val response: Response<GroupCreatedDto> = mockk(relaxed = true)

        every { response.isSuccessful } returns false
        coEvery { api.create(any()) } returns response

        shouldThrow<InvalidFcmTokenException> { messaging.createGroup("name", "invalid") }
    }

    @Test
    internal fun `add to group`() {
        val response: Response<Unit> = mockk()

        every { response.isSuccessful } returns true
        coEvery { api.addMember(any()) } returns response

        messaging.addToGroup("name", "group", "token")
    }

    @Test
    internal fun `add to group with invalid token`() {
        val response: Response<Unit> = mockk(relaxed = true)

        every { response.isSuccessful } returns false
        coEvery { api.addMember(any()) } returns response

        shouldThrow<InvalidFcmTokenException> { messaging.addToGroup("name", "roupToken", "invalid") }
    }

}