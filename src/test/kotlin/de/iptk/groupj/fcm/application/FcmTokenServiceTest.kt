package de.iptk.groupj.fcm.application

import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.mockUser
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class FcmTokenServiceTest(

    @MockK
    private val messaging: FirebaseMessagingService,

    @MockK
    private val userService: UserService

) {

    private val service = FcmTokenService(messaging, userService)

    @Test
    internal fun `create group test`() {
        val user = mockUser(fcmToken = null)
        val token = "blabla"
        val groupToken = "new-fcm-token"

        justRun { userService.update(user) }
        every { messaging.createGroup(user.id!!.toString(), token) } returns groupToken

        service.addToken(user, token)
        user.fcmToken shouldBe groupToken
        verify(exactly = 1) { userService.update(user) }
    }

    @Test
    internal fun `add to group on existing group`() {
        val groupToken = "new-fcm-token"
        val user = mockUser(fcmToken = groupToken)
        val token = "blabla"

        every { messaging.doesGroupExists(user.id!!.toString()) } returns true
        justRun { messaging.addToGroup(user.id!!.toString(), groupToken, token) }

        service.addToken(user, token)
    }

    @Test
    internal fun `add to group on non existing group`() {
        val token = "blabla"
        val groupToken = "new-fcm-token"
        val user = mockUser(fcmToken = groupToken)

        every { messaging.doesGroupExists(user.id!!.toString()) } returns false
        every { messaging.createGroup(user.id!!.toString(), token) } returns groupToken

        service.addToken(user, token)
        user.fcmToken shouldBe groupToken
        verify(exactly = 1) { userService.update(user) }
    }

}