package de.iptk.groupj.fcm.eventing

import de.iptk.groupj.chat.domain.mockChatMessage
import de.iptk.groupj.chat.eventing.ChatMessageEvent
import de.iptk.groupj.fcm.application.FirebaseMessagingService
import de.iptk.groupj.fcm.model.FcmNotificationCommand
import de.iptk.groupj.fcm.model.NotificationType
import de.iptk.groupj.user.mockUser
import io.mockk.clearMocks
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
internal class ChatFcmNotificationProducerTest(@MockK private val notifications: FirebaseMessagingService) {

    private lateinit var producer: ChatFcmNotificationProducer

    @BeforeEach
    internal fun init() {
        clearMocks(notifications)
        producer = ChatFcmNotificationProducer(notifications)
    }

    @Test
    internal fun `user without fcm token should not send`() {
        val recipient = mockUser(fcmToken = null)
        producer.afterChatMessageEvent(ChatMessageEvent(mockUser(), recipient, UUID.randomUUID(), "irrelevant"))

        verify(exactly = 0) { notifications.sendToGroup(any(), any(), any()) }
    }

    @Test
    internal fun `user with fcm token`() {
        val sender = mockUser()
        val recipient = mockUser(fcmToken = "some-token")
        val chatMessage = mockChatMessage()

        producer.afterChatMessageEvent(ChatMessageEvent(sender, recipient, chatMessage.id!!, chatMessage.message))

        val expectedMessage = FcmNotificationCommand(
            recipient.fcmToken!!, mapOf(
                "id" to sender.id,
                "name" to "${sender.givenName} ${sender.familyName}",
                "messageId" to chatMessage.id,
                "message" to chatMessage.message,
                "type" to NotificationType.CHAT
            )
        )
        verify(exactly = 1) { notifications.sendToGroup(expectedMessage, sender.id!!, recipient.id!!) }
    }

}