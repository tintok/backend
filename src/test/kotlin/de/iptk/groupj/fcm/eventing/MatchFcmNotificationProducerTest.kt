package de.iptk.groupj.fcm.eventing

import de.iptk.groupj.fcm.application.FirebaseMessagingService
import de.iptk.groupj.fcm.model.FcmNotificationCommand
import de.iptk.groupj.fcm.model.NotificationType
import de.iptk.groupj.user.eventing.MatchEvent
import de.iptk.groupj.user.mockUser
import io.mockk.clearMocks
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class MatchFcmNotificationProducerTest(@MockK private val notifications: FirebaseMessagingService) {

    private lateinit var producer: MatchFcmNotificationProducer

    @BeforeEach
    internal fun init() {
        clearMocks(notifications)
        producer = MatchFcmNotificationProducer(notifications)
    }

    @Test
    internal fun `send no match notification on both users without fcm`() {
        val user1 = mockUser(fcmToken = null)
        val user2 = mockUser(fcmToken = null)

        producer.afterMatchEvent(MatchEvent(user1, user2))

        verify(exactly = 0) { notifications.sendToGroup(any(), any(), any()) }
    }

    @Test
    internal fun `send one match notification to user1`() {
        val user1 = mockUser(fcmToken = null)
        val user2 = mockUser(fcmToken = "token")

        producer.afterMatchEvent(MatchEvent(user1, user2))

        val expectedMessage = FcmNotificationCommand(
            user2.fcmToken!!, mapOf(
                "id" to user1.id,
                "name" to "${user1.givenName} ${user1.familyName}",
                "type" to NotificationType.MATCH
            )
        )
        verify(exactly = 1) { notifications.sendToGroup(expectedMessage, user1.id!!, user2.id!!) }
    }

    @Test
    internal fun `send one match notification to user2`() {
        val user1 = mockUser(fcmToken = "token")
        val user2 = mockUser(fcmToken = null)

        producer.afterMatchEvent(MatchEvent(user1, user2))

        val expectedMessage = FcmNotificationCommand(
            user1.fcmToken!!, mapOf(
                "id" to user2.id,
                "name" to "${user2.givenName} ${user2.familyName}",
                "type" to NotificationType.MATCH
            )
        )
        verify(exactly = 1) { notifications.sendToGroup(expectedMessage, user2.id!!, user1.id!!) }
    }

    @Test
    internal fun `send match notification to both users`() {
        val user1 = mockUser(fcmToken = "token")
        val user2 = mockUser(fcmToken = "token")

        producer.afterMatchEvent(MatchEvent(user1, user2))

        val expectedMessageToUser1 = FcmNotificationCommand(
            user1.fcmToken!!, mapOf(
                "id" to user2.id,
                "name" to "${user2.givenName} ${user2.familyName}",
                "type" to NotificationType.MATCH
            )
        )
        verify(exactly = 1) { notifications.sendToGroup(expectedMessageToUser1, user2.id!!, user1.id!!) }

        val expectedMessageToUser2 = FcmNotificationCommand(
            user2.fcmToken!!, mapOf(
                "id" to user1.id,
                "name" to "${user1.givenName} ${user1.familyName}",
                "type" to NotificationType.MATCH
            )
        )
        verify(exactly = 1) { notifications.sendToGroup(expectedMessageToUser2, user1.id!!, user2.id!!) }

        verify(exactly = 2) { notifications.sendToGroup(any(), any(), any()) }
    }

}