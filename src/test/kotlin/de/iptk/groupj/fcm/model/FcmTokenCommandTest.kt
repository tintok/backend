package de.iptk.groupj.fcm.model

import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test
import java.util.*

internal class FcmTokenCommandTest {

    @Test
    internal fun `valid token`() {
        FcmTokenCommand("${UUID.randomUUID()}").shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid blank text token`() {
        FcmTokenCommand("   ") shouldHaveNumberOfConstraintValidations 1
    }

}