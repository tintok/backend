package de.iptk.groupj.user

import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.domain.AuthProvider
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.http.MediaType
import java.util.*

fun mockUser(
    id: UUID = UUID.randomUUID(),
    pictureId: UUID = UUID.randomUUID(),
    pictureLength: Long = 1337,
    pictureMimeType: String = MediaType.IMAGE_PNG_VALUE,
    username: String = "elliotalderson@protonmail.ch",
    givenName: String = "Elliot",
    familyName: String = "Alderson",
    provider: AuthProvider = AuthProvider.LOCAL,
    password: String = "secret",
    location: String? = "Darmstadt",
    biography: String? = "Hey!",
    fcmToken: String? = null
) = UserEntity(
    id,
    pictureId,
    pictureLength,
    pictureMimeType,
    username,
    givenName,
    familyName,
    provider,
    PasswordEncoder().encode(password),
    location,
    biography,
    fcmToken
)