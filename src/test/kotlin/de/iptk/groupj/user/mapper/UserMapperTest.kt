package de.iptk.groupj.user.mapper

import de.iptk.groupj.user.mockUser
import de.iptk.groupj.user.model.MatchProfileDto
import de.iptk.groupj.user.model.ProfileDto
import de.iptk.groupj.user.model.UpdateProfileCommand
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class UserMapperTest {

    private val mapper: UserMapper = UserMapper.INSTANCE

    @Test
    internal fun `user to user dto`() {
        val user = mockUser()
        val expectedUserDto = ProfileDto(
            user.id!!, user.username, user.givenName, user.familyName, user.location,
            user.biography
        )

        mapper.mapToUserMeDto(user) shouldBe expectedUserDto
    }

    @Test
    internal fun `user to match user dto`() {
        val user = mockUser()
        val expectedMatchDto =
            MatchProfileDto(user.id!!, "${user.givenName} ${user.familyName}", user.biography, user.location)

        mapper.mapToMatchProfileDto(user) shouldBe expectedMatchDto
    }

    @Test
    internal fun `update user`() {
        val command = UpdateProfileCommand("ElliotNew", "AldersonNew", "DarmstadtNew", "Hey!New")
        val user = mockUser()
        mapper.update(command, user)

        assertSoftly(user) {
            givenName shouldBe command.givenName
            familyName shouldBe command.familyName
            location shouldBe command.location
            biography shouldBe command.biography
        }
    }

}