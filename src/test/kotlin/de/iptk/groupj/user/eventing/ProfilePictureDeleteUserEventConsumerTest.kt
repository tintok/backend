package de.iptk.groupj.user.eventing

import de.iptk.groupj.user.adapter.storage.ProfilePictureStore
import de.iptk.groupj.user.mockUser
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class ProfilePictureDeleteUserEventConsumerTest(@MockK private val store: ProfilePictureStore) {

    private lateinit var eventConsumer: ProfilePictureDeleteUserEventConsumer

    @BeforeEach
    fun init() {
        eventConsumer = ProfilePictureDeleteUserEventConsumer(store)
    }

    @Test
    internal fun `test consume`() {
        val user = mockUser()

        every { store.unsetContent(user) } returns user

        eventConsumer.afterUserDeleteEvent(UserDeleteEvent(user))
    }

}