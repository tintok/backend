package de.iptk.groupj.user.model

import de.iptk.groupj.shared.USER_MAX_BIOGRAPHY_LENGTH
import de.iptk.groupj.shared.USER_MAX_FAMILY_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_GIVEN_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_LOCATION_LENGTH
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class UpdateProfileCommandTest {

    @Test
    internal fun `valid profile update`() {
        val command = UpdateProfileCommand("Ellito", "Alderson", "Darmstadt", "Hey!")
        command.shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid blank given name profile update`() {
        val command = UpdateProfileCommand("  ", "Alderson", "Darmstadt", "Hey!")
        command shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid blank family name profile update`() {
        val command = UpdateProfileCommand("Ellito", "  ", "Darmstadt", "Hey!")
        command shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big given name`() {
        val signUpRequest = UpdateProfileCommand(
            StringUtils.repeat("a", USER_MAX_GIVEN_NAME_LENGTH + 1), "Alderson", "Darmstadt", "Hey!"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big family name`() {
        val signUpRequest = UpdateProfileCommand(
            "Ellito", StringUtils.repeat("a", USER_MAX_FAMILY_NAME_LENGTH + 1), "Darmstadt", "Hey!"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big bio`() {
        val signUpRequest = UpdateProfileCommand(
            "Ellito", "Alderson", "Darmstadt", StringUtils.repeat("a", USER_MAX_BIOGRAPHY_LENGTH + 1)
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big location`() {
        val signUpRequest = UpdateProfileCommand(
            "Ellito", "Alderson", StringUtils.repeat("a", USER_MAX_LOCATION_LENGTH + 1), "Hey!"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

}