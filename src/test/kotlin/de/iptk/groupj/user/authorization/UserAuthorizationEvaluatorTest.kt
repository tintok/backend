package de.iptk.groupj.user.authorization

import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.mockUser
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
internal class UserAuthorizationEvaluatorTest(@MockK private val repository: UserRepository) {

    private lateinit var service: UserAuthorizationEvaluator

    @BeforeEach
    internal fun init() {
        service = UserAuthorizationEvaluator(repository)
    }

    @Test
    internal fun `user is self`() {
        val user = mockUser()
        service.isSelfOrIsFromMatched(user, user.id!!).shouldBeTrue()
    }

    @Test
    internal fun `user is matched`() {
        val user = mockUser()

        every { repository.existsById(any()) } returns true
        every { repository.isMatched(any<UUID>(), any()) } returns true

        service.isSelfOrIsFromMatched(user, UUID.randomUUID()).shouldBeTrue()
    }

    @Test
    internal fun `user not matched and not self`() {
        val user = mockUser()

        every { repository.existsById(any()) } returns true
        every { repository.isMatched(any<UUID>(), any()) } returns false

        service.isSelfOrIsFromMatched(user, UUID.randomUUID()).shouldBeFalse()
    }

    @Test
    internal fun `user does not exist should yield true`() {
        val user = mockUser()

        every { repository.existsById(any()) } returns false

        service.isSelfOrIsFromMatched(user, UUID.randomUUID()).shouldBeTrue()
    }

}