package de.iptk.groupj.user.domain

import de.iptk.groupj.shared.USER_MAX_BIOGRAPHY_LENGTH
import de.iptk.groupj.shared.USER_MAX_FAMILY_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_GIVEN_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_USERNAME_LENGTH
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import de.iptk.groupj.user.mockUser
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class UserEntityTest {

    @Test
    internal fun `valid user`() {
        mockUser().shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid username user`() {
        mockUser(username = "foo") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid given name`() {
        mockUser(givenName = "  ") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid family name`() {
        mockUser(familyName = "    ") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `user invalid content type`() {
        mockUser(pictureMimeType = "invalid") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big username`() {
        val mockUser = mockUser(username = StringUtils.repeat("a", USER_MAX_USERNAME_LENGTH + 1))

        mockUser shouldHaveNumberOfConstraintValidations 2
    }

    @Test
    internal fun `invalid to big given name`() {
        val mockUser = mockUser(givenName = StringUtils.repeat("a", USER_MAX_GIVEN_NAME_LENGTH + 1))

        mockUser shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big family name`() {
        val mockUser = mockUser(familyName = StringUtils.repeat("a", USER_MAX_FAMILY_NAME_LENGTH + 1))

        mockUser shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big biography`() {
        val mockUser = mockUser(biography = StringUtils.repeat("a", USER_MAX_BIOGRAPHY_LENGTH + 1))

        mockUser shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big location`() {
        val mockUser = mockUser(location = StringUtils.repeat("a", USER_MAX_FAMILY_NAME_LENGTH + 1))

        mockUser shouldHaveNumberOfConstraintValidations 1
    }

}