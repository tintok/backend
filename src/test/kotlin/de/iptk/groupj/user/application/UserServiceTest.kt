package de.iptk.groupj.user.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.domain.AuthProvider
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.eventing.UserDeleteEvent
import de.iptk.groupj.user.exception.OldPasswordIncorrectException
import de.iptk.groupj.user.exception.UsernameAlreadyInUseException
import de.iptk.groupj.user.mockUser
import de.iptk.groupj.user.model.UpdateProfileCommand
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.MediaType
import java.util.*

@ExtendWith(MockKExtension::class)
internal class UserServiceTest(

    @MockK
    private val repository: UserRepository,

    @MockK
    private val publisher: ApplicationEventPublisher

) {

    private lateinit var service: UserService


    @BeforeEach
    internal fun init() {
        service = UserService(repository, publisher, PasswordEncoder())
    }

    @Test
    internal fun `load by id should yield true`() {
        val user = mockUser()

        every { repository.findById(user.id!!) } returns Optional.of(user)

        service.findById(user.id!!) shouldBe user
    }

    @Test
    internal fun `load by id should yield false`() {
        every { repository.findById(any()) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.findById(UUID.randomUUID()) }
    }

    @Test
    internal fun `exists by id should yield true`() {
        every { repository.existsById(any()) } returns true

        service.existsById(UUID.randomUUID()).shouldBeTrue()
    }

    @Test
    internal fun `exists by id should yield false`() {
        every { repository.existsById(any()) } returns false

        service.existsById(UUID.randomUUID()).shouldBeFalse()
    }

    @Test
    internal fun `exists by username should yield true`() {
        every { repository.existsByUsername(any()) } returns true

        service.existsByUsername("elliotalderson@protonmail.ch").shouldBeTrue()
    }

    @Test
    internal fun `exists by username should yield false`() {
        every { repository.existsByUsername(any()) } returns false

        service.existsByUsername("elliotalderson@protonmail.ch").shouldBeFalse()
    }

    @Test
    internal fun `load existing user by username`() {
        val user = mockUser()

        every { repository.findByUsername(user.username) } returns Optional.of(user)

        service.findByUsername(user.username) shouldBe user
    }

    @Test
    internal fun `load non existing user by username`() {
        every { repository.findByUsername(any()) } returns Optional.empty()

        shouldThrow<ResourceNotFoundException> { service.findByUsername("Elliot") }
    }

    @Test
    internal fun `create user with already existing username`() {
        every { repository.existsByUsername(any()) } returns true

        shouldThrow<UsernameAlreadyInUseException> { service.create(mockUser()) }
    }

    @Test
    internal fun `on delete delete event should be issued`() {
        val user = mockUser()

        justRun { repository.deleteByUsername(user.username) }
        service.delete(user)

        verify(exactly = 1) { publisher.publishEvent(UserDeleteEvent(user)) }
    }

    @Test
    internal fun `update user updates correctly`() {
        val user = mockUser()
        val userUpdate = UpdateProfileCommand("name", "update", "us", "test")

        val expectedUpdatedUser = mockUser(
            givenName = userUpdate.givenName,
            familyName = userUpdate.familyName,
            location = userUpdate.location,
            biography = userUpdate.biography
        )
        justRun { repository.save(expectedUpdatedUser) }
        service.update(user, userUpdate)
    }

    @Test
    internal fun `password update`() {
        val requester = mockUser(password = "secret")

        every { repository.save(any()) } returns mockk()

        shouldNotThrowAny { service.changePassword(requester, "secret", "password") }
    }

    @Test
    internal fun `password update with non matching passwords`() {
        val requester = UserEntity(
            UUID.randomUUID(),
            UUID.randomUUID(),
            1337,
            MediaType.IMAGE_JPEG_VALUE,
            "elliotalderson@protonmail.ch",
            "Elliot",
            "Alderson",
            AuthProvider.LOCAL,
            PasswordEncoder().encode("secret")
        )

        shouldThrow<OldPasswordIncorrectException> { service.changePassword(requester, "password", "newPassword") }
    }

    @Test
    internal fun `password update with null password from oauth provider`() {
        val requester = UserEntity(
            UUID.randomUUID(),
            UUID.randomUUID(),
            1337,
            MediaType.IMAGE_JPEG_VALUE,
            "elliotalderson@protonmail.ch",
            "Elliot",
            "Alderson",
            AuthProvider.LOCAL,
            null
        )

        every { repository.save(any()) } returns mockk()

        shouldNotThrowAny { service.changePassword(requester, "ignored", "password") }
    }

}