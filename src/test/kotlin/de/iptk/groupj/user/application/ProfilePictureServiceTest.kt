package de.iptk.groupj.user.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.user.adapter.storage.ProfilePictureStore
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.MediaType.IMAGE_JPEG_VALUE
import java.io.InputStream
import java.util.*

@ExtendWith(MockKExtension::class)
internal class ProfilePictureServiceTest(

    @MockK
    private val store: ProfilePictureStore,

    @MockK
    private val userService: UserService

) {

    private lateinit var service: ProfilePictureService

    @BeforeEach
    internal fun init() {
        service = ProfilePictureService(userService, store)
    }

    @Test
    internal fun `get profile picture from non existing user`() {
        every { userService.findById(any()) } throws ResourceNotFoundException("User", UUID.randomUUID())

        shouldThrow<ResourceNotFoundException> { service.find(UUID.randomUUID()) }
    }

    @Test
    internal fun `update profile picture`() {
        val user = mockUser()
        val inputStream: InputStream = mockk()
        val mimeType = IMAGE_JPEG_VALUE

        justRun { userService.update(any()) }
        every { store.setContent(any(), eq(inputStream)) } returns user

        service.update(user, inputStream, mimeType)

        user.pictureMimeType shouldBe mimeType
    }

    @Test
    internal fun `delete picture`() {
        val user = mockUser()
        justRun { userService.update(any()) }
        every { store.unsetContent(any()) } returns mockUser()

        shouldNotThrowAny { service.delete(user) }
    }

}