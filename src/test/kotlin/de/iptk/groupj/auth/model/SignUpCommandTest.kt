package de.iptk.groupj.auth.model

import de.iptk.groupj.shared.USER_MAX_BIOGRAPHY_LENGTH
import de.iptk.groupj.shared.USER_MAX_GIVEN_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_LOCATION_LENGTH
import de.iptk.groupj.shared.USER_MAX_USERNAME_LENGTH
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class SignUpCommandTest {

    @Test
    internal fun `valid login request`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "Elliot", "Alderson",
            "Darmstadt", "bio"
        )

        signUpRequest.shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid username`() {
        val signUpRequest = SignUpCommand(
            "invalid", "secret", "Elliot",
            "Alderson"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid blank password`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "      ", "Elliot",
            "Alderson"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to short password`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", StringUtils.repeat("x", 3), "Elliot",
            "Alderson"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to long password`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", StringUtils.repeat("x", 65), "Elliot",
            "Alderson"
        )
        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid blank given name`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "   ",
            "Alderson"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid blank family name`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "Elliot",
            "  "
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big username`() {
        val signUpRequest = SignUpCommand(
            StringUtils.repeat("a", USER_MAX_USERNAME_LENGTH + 1), "secret", "Elliot",
            "Alderson"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 2
    }

    @Test
    internal fun `invalid to big given name`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret",
            StringUtils.repeat("a", USER_MAX_GIVEN_NAME_LENGTH + 1), "Alderson"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big family name`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "Elliot",
            StringUtils.repeat("a", USER_MAX_GIVEN_NAME_LENGTH + 1)
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big bio`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "Elliot", "Alderson",
            "Darmstadt", StringUtils.repeat("a", USER_MAX_BIOGRAPHY_LENGTH + 1)
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid to big location`() {
        val signUpRequest = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "Elliot", "Alderson",
            StringUtils.repeat("a", USER_MAX_LOCATION_LENGTH + 1), "bio"
        )

        signUpRequest shouldHaveNumberOfConstraintValidations 1
    }

}