package de.iptk.groupj.auth.model

import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class SignInCommandTest {

    @Test
    internal fun `test valid login request`() {
        val loginRequest = SignInCommand("test@test.de", "secret")
        loginRequest.shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `test not a valid email username`() {
        SignInCommand("foo", "secret") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `test invalid password containing only white spaces`() {
        SignInCommand("test@test.de", "      ") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `test invalid to short password`() {
        val loginRequest = SignInCommand("test@test.de", StringUtils.repeat("x", 3))
        loginRequest shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `test invalid to long password`() {
        val loginRequest = SignInCommand("test@test.de", StringUtils.repeat("x", 65))
        loginRequest shouldHaveNumberOfConstraintValidations 1
    }

}