package de.iptk.groupj.auth.model

import de.iptk.groupj.getObjectMapper
import io.kotest.assertions.throwables.shouldNotThrowAny
import org.junit.jupiter.api.Test

internal class GoogleOAuthUserInfoTest {

    private val mapper = getObjectMapper()

    @Test
    internal fun `test deserialization`() {
        val payload = mapper.writeValueAsString(
            mapOf(
                "id" to "123456789",
                "email" to "elliotalderson@protonmail.ch",
                "verified_email" to true,
                "name" to "Elliot Alderson",
                "given_name" to "Elliot",
                "family_name" to "Alderson",
                "picture" to "https://localhost:8080",
                "locale" to "de"
            )
        )

        shouldNotThrowAny { mapper.readValue(payload, GoogleOAuthUserInfo::class.java) }
    }

}