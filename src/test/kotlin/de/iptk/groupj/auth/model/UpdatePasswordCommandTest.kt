package de.iptk.groupj.auth.model

import de.iptk.groupj.shared.MAX_PASSWORD_LENGTH
import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Test

internal class UpdatePasswordCommandTest {

    @Test
    internal fun `valid password request`() {
        UpdatePasswordCommand("secret", "secretNew").shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid new password blank`() {
        UpdatePasswordCommand("secret", "      ") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid old password blank`() {
        UpdatePasswordCommand("      ", "secretNew") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid new password to small`() {
        UpdatePasswordCommand("secret", "x") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid old password to small`() {
        UpdatePasswordCommand("x", "secretNew") shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid new password to large`() {
        val passwordChange = UpdatePasswordCommand("secret", StringUtils.repeat("x", MAX_PASSWORD_LENGTH + 1))
        passwordChange shouldHaveNumberOfConstraintValidations 1
    }

    @Test
    internal fun `invalid old password to large`() {
        val passwordChange = UpdatePasswordCommand(StringUtils.repeat("x", MAX_PASSWORD_LENGTH + 1), "secretNew")
        passwordChange shouldHaveNumberOfConstraintValidations 1
    }
}