package de.iptk.groupj.auth.model

import de.iptk.groupj.shouldHaveNumberOfConstraintValidations
import de.iptk.groupj.shouldNotHaveConstraintValidations
import org.junit.jupiter.api.Test

internal class RefreshJwtTokenCommandTest {

    @Test
    internal fun `valid jwt`() {
        val tokenValue =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MTYyMzkwMjJ9.tbDepxpstvGdW8TC3G8zg4B6rUYAOvfzdceoH48wgRQ"
        RefreshJwtTokenCommand(tokenValue).shouldNotHaveConstraintValidations()
    }

    @Test
    internal fun `invalid jwt`() {
        val tokenValue = "invalid"
        RefreshJwtTokenCommand(tokenValue) shouldHaveNumberOfConstraintValidations 1
    }

}