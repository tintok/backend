package de.iptk.groupj.auth.mapper

import de.iptk.groupj.auth.model.GoogleOAuthUserInfo
import de.iptk.groupj.auth.model.SignUpCommand
import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.domain.AuthProvider
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.mapper.UserMapper
import de.iptk.groupj.user.model.ProfileDto
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.equality.shouldBeEqualToUsingFields
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.util.*

internal class AuthMapperTest {

    @Test
    internal fun `sign up request to user mapping`() {
        val signUp = SignUpCommand(
            "elliotalderson@protonmail.ch", "secret", "Elliot", "Alderson",
            "Darmstadt", "Hey!"
        )

        val mapping: UserEntity = AuthMapper.INSTANCE.mapToUserEntity(signUp)
        val expectedUser = UserEntity(
            null,
            null,
            null,
            null,
            signUp.username,
            signUp.givenName,
            signUp.familyName,
            AuthProvider.LOCAL,
            mapping.password,
            signUp.location,
            signUp.biography,
            null
        )

        assertSoftly(mapping) {
            mapping.shouldBeEqualToUsingFields(
                expectedUser, UserEntity::id, UserEntity::pictureId, UserEntity::pictureLength,
                UserEntity::pictureMimeType, UserEntity::givenName, UserEntity::familyName, UserEntity::provider,
                UserEntity::location, UserEntity::biography, UserEntity::fcmToken
            )
            username shouldBe expectedUser.username
            PasswordEncoder().matches(signUp.password, password!!).shouldBeTrue()
        }
    }

    @Test
    internal fun `user to user me dto mapping`() {
        val user = UserEntity(
            UUID.randomUUID(),
            null,
            null,
            null,
            "elliotalderson@protonmail.ch",
            "Elliot",
            "Alderson",
            AuthProvider.LOCAL,
            "secret",
            "Darmstadt",
            "Hey!"
        )

        val expectedSignUpResponse = ProfileDto(
            user.id!!, user.username, user.givenName, user.familyName, user.location, user.biography
        )

        UserMapper.INSTANCE.mapToUserMeDto(user) shouldBe expectedSignUpResponse
    }

    @Test
    internal fun `google oauth info to user mapping`() {
        val oAuthUserInfo = GoogleOAuthUserInfo(
            "${UUID.randomUUID()}", "elliotalderson@protonmail.ch", true, "Elliot Alderson",
            "Elliot", "Alderson", null, "de"
        )

        val expectedUser = UserEntity(
            null,
            null,
            null,
            null,
            "elliotalderson@protonmail.ch",
            "Elliot",
            "Alderson",
            AuthProvider.GOOGLE,
            null,
            null,
            null,
            null
        )

        val mapping: UserEntity = AuthMapper.INSTANCE.mapGoogleOAuthToUser(oAuthUserInfo)
        assertSoftly(mapping) {
            mapping.shouldBeEqualToUsingFields(
                expectedUser, UserEntity::id, UserEntity::pictureId, UserEntity::pictureLength,
                UserEntity::pictureMimeType, UserEntity::givenName, UserEntity::familyName, UserEntity::provider,
                UserEntity::location, UserEntity::biography, UserEntity::fcmToken
            )
            username shouldBe expectedUser.username
            password.shouldBeNull()
        }
    }

}