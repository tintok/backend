package de.iptk.groupj.similarity.application

import de.iptk.groupj.shared.MATCH_INTERACTION_THRESHOLD
import de.iptk.groupj.similarity.adapter.database.SimilarityRepositoryImpl
import de.iptk.groupj.similarity.mockRandomPostInteractions
import de.iptk.groupj.similarity.model.PostInteractions
import de.iptk.groupj.similarity.model.Similarity
import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.eventing.MatchEvent
import de.iptk.groupj.user.mockUser
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.slot
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ApplicationEventPublisher
import java.util.*

@ExtendWith(MockKExtension::class)
internal class UserPostSimilarityServiceTest(

    @MockK
    private val repository: SimilarityRepositoryImpl,

    @MockK
    private val userRepository: UserRepository,

    @MockK
    private val publisher: ApplicationEventPublisher

) {

    private lateinit var service: UserPostSimilarityService

    @BeforeEach
    internal fun init() {
        service = UserPostSimilarityService(repository, userRepository, publisher)
    }

    @Test
    internal fun `update similarities does not throw exceptions`() {
        every { repository.countCommonLikes(any(), any()) } returns mockRandomPostInteractions(10)
        every { repository.countCommonDislikes(any(), any()) } returns mockRandomPostInteractions(10)
        every { repository.countOpposedLikes(any(), any()) } returns mockRandomPostInteractions(10)
        every { repository.countOpposedDislikes(any(), any()) } returns mockRandomPostInteractions(10)
        every { userRepository.isMatched(any<String>(), any()) } returns true   // don't test matching here
        justRun { repository.saveUserToUserSimilarities(any()) }

        shouldNotThrowAny {
            // function just calls other functions, test no exception is thrown
            service.updateSimilarities("", UUID.randomUUID())
        }
    }

    @Test
    internal fun `update similarities generates match`() {
        val user1 = mockUser(username = "user1@test.de")
        val user2 = mockUser(username = "user2@test.de")
        val interactions = PostInteractions("user2@test.de", MATCH_INTERACTION_THRESHOLD)
        val publishedEvent = slot<MatchEvent>()

        every { repository.countCommonLikes(any(), any()) } returns listOf(interactions)
        every { repository.countCommonDislikes(any(), any()) } returns emptyList()
        every { repository.countOpposedLikes(any(), any()) } returns emptyList()
        every { repository.countOpposedDislikes(any(), any()) } returns emptyList()
        every { userRepository.findByUsername("user1@test.de") } returns Optional.of(user1)
        every { userRepository.findByUsername("user2@test.de") } returns Optional.of(user2)
        every { userRepository.isMatched(any<String>(), any()) } returns false
        justRun { userRepository.saveMatched(any(), any()) }
        justRun { publisher.publishEvent(capture(publishedEvent)) }
        justRun { repository.saveUserToUserSimilarities(any()) }

        // should generate MatchEvent
        service.updateSimilarities(user1.username, UUID.randomUUID())

        publishedEvent.captured shouldBe MatchEvent(user1, user2)
    }

    @Test
    internal fun `generate match`() {
        val user1 = mockUser(username = "user1@test.de")
        val user2 = mockUser(username = "user2@test.de")
        val publishedEvent = slot<MatchEvent>()

        every { userRepository.findByUsername("user1@test.de") } returns Optional.of(user1)
        every { userRepository.findByUsername("user2@test.de") } returns Optional.of(user2)
        justRun { userRepository.saveMatched(any(), any()) }
        justRun { publisher.publishEvent(capture(publishedEvent)) }

        service.generateMatch(Similarity(user1.username, user2.username, 0))
        publishedEvent.captured shouldBe MatchEvent(user1, user2)
    }

    @Test
    internal fun `generate invalid match`() {
        every { userRepository.findByUsername(any()) } returns Optional.empty()
        justRun { userRepository.saveMatched(any(), any()) }

        shouldThrow<NoSuchElementException> {
            service.generateMatch(Similarity("", "", 0))
        }
    }

    @Test
    internal fun `compute similarity scores`() {
        val username = "me"
        val commonLikes = listOf(
            PostInteractions("1", 1),
            PostInteractions("2", 1),
            PostInteractions("4", 4),
        )
        val commonDislikes = listOf(
            PostInteractions("1", 1),
            PostInteractions("2", 2),
            PostInteractions("3", 3),
            PostInteractions("4", 1),
        )
        val opposedLikes = listOf(
            PostInteractions("1", 1),
            PostInteractions("3", 1),
        )
        val opposedDislikes = listOf(
            PostInteractions("1", 1),
            PostInteractions("2", 2),
            PostInteractions("5", 2)
        )

        val result = service.computeSimilarityScores(
            username, commonLikes, commonDislikes, opposedLikes, opposedDislikes
        )

        result shouldContainExactlyInAnyOrder listOf(
            Similarity(username, "1", 0),
            Similarity(username, "2", 1),
            Similarity(username, "3", 2),
            Similarity(username, "4", 5),
            Similarity(username, "5", -2)
        )
    }

    @Test
    internal fun `compute similarity scores with modifier`() {
        val commonLikes = listOf(
            PostInteractions("other", 1)
        )

        val result = service.computeSimilarityScores("me", commonLikes, listOf(), listOf(), listOf()) { x ->
            (x * 2).toDouble()
        }

        result shouldContainExactly listOf(
            Similarity("me", "other", 1, 2.0)
        )
    }

}
