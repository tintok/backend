package de.iptk.groupj.similarity.application

import de.iptk.groupj.similarity.adapter.database.SimilarityRepositoryImpl
import de.iptk.groupj.similarity.mockRandomTagInteractions
import de.iptk.groupj.similarity.model.Similarity
import de.iptk.groupj.similarity.model.TagInteractions
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
internal class UserTagSimilarityServiceTest(@MockK private val repository: SimilarityRepositoryImpl) {

    private lateinit var service: UserTagSimilarityService

    @BeforeEach
    internal fun init() {
        service = UserTagSimilarityService(repository)
    }

    @Test
    internal fun `update similarities does not throw exceptions`() {
        every { repository.countLikedTags(any(), any()) } returns mockRandomTagInteractions(10)
        every { repository.countDislikedTags(any(), any()) } returns mockRandomTagInteractions(10)
        justRun { repository.saveUserToTagSimilarities(any()) }

        shouldNotThrowAny {
            // function just calls other functions, test no exception is thrown
            service.updateSimilarities("me", UUID.randomUUID())
        }
    }

    @Test
    internal fun `compute similarity scores`() {
        val username = "me"
        val likes = listOf(
            TagInteractions("tag1", 1),
            TagInteractions("tag2", 2),
            TagInteractions("tag3", 3),
        )
        val dislikes = listOf(
            TagInteractions("tag1", 2),
            TagInteractions("tag2", 1),
            TagInteractions("tag4", 2)
        )

        val result = service.computeSimilarityScores(username, likes, dislikes, Int::toDouble)

        result shouldContainExactlyInAnyOrder listOf(
            Similarity(username, "tag1", 0),     // dislikes are halved, net is zero
            Similarity(username, "tag2", 2),     // integer division means one dislike is no dislike
            Similarity(username, "tag3", 3),
            Similarity(username, "tag4", -1),
        )
    }

    @Test
    internal fun `compute similarity scores with modifier`() {
        val likes = listOf(
            TagInteractions("tag1", 1)
        )

        val result = service.computeSimilarityScores("me", likes, emptyList()) { x -> (x * 2).toDouble() }

        result shouldContainExactlyInAnyOrder listOf(
            Similarity("me", "tag1", 1, 2.0)
        )
    }

}