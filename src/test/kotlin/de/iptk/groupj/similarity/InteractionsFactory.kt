package de.iptk.groupj.similarity

import de.iptk.groupj.similarity.model.PostInteractions
import de.iptk.groupj.similarity.model.TagInteractions
import kotlin.random.Random

private val charPool: List<Char> = ('a'..'z') + ('A'..'Z')

private fun randomString() = (1..5).map { Random.nextInt(charPool.size) }.map(charPool::get).joinToString("")

fun mockRandomPostInteractions(length: Int) = (1..length).map {
    PostInteractions(randomString(), Random.nextInt(100))
}.toList()

fun mockRandomTagInteractions(length: Int) = (1..length).map {
    TagInteractions(randomString(), Random.nextInt(100))
}.toList()
