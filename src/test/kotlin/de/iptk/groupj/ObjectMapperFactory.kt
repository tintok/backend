package de.iptk.groupj

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jsonMapper
import com.fasterxml.jackson.module.kotlin.kotlinModule

fun getObjectMapper(): ObjectMapper = jsonMapper {
    addModule(kotlinModule())
    addModule(JavaTimeModule())
}