package de.iptk.groupj.tag.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.shared.TAGS
import de.iptk.groupj.tag.application.TagService
import de.iptk.groupj.tag.mockTags
import io.mockk.every
import org.hamcrest.collection.IsCollectionWithSize
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.get

@DefaultMockUser
@WebMvcTest(controllers = [TagController::class])
class TagControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var tagService: TagService

    @Test
    internal fun `retrieve tag collection`() {
        every { tagService.findAllTagsSortedBySimilarity(any()) } returns mockTags("a", "b", "c")

        mvc.get(TAGS)
            .andExpect {
                status { isOk() }
                jsonPath("$") { IsCollectionWithSize.hasSize<Array<String>>(3) }
                jsonPath("$[0]") { value("a") }
                jsonPath("$[1]") { value("b") }
                jsonPath("$[2]") { value("c") }
            }
    }

}