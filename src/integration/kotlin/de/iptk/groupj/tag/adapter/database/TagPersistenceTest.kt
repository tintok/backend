package de.iptk.groupj.tag.adapter.database

import de.iptk.groupj.Cypher
import de.iptk.groupj.PersistenceIntegrationTest
import de.iptk.groupj.tag.domain.TagEntity
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldBeSortedWith
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.optional.shouldBePresent
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import javax.validation.ConstraintViolationException

@Cypher("TagPersistenceTest.cypher")
class TagPersistenceTest @Autowired constructor(private val repository: TagRepository) : PersistenceIntegrationTest() {

    @Test
    internal fun `find by name`() {
        repository.findByName("sport").shouldBePresent()
    }

    @Test
    internal fun `find all by name in`() {
        val tags = repository.findAllByNameIn(setOf("sport", "computer"))
        tags shouldHaveSize 2
        tags.map { it.name }.shouldContainExactly("sport", "computer")
    }

    @Test
    internal fun `fail on constraint validation`() {
        val tag = TagEntity(0, "  ")
        shouldThrow<ConstraintViolationException> { repository.save(tag) }
    }

    @Test
    internal fun `find all sorted by similarity`() {
        val tags = repository.findAllOrderBySimilarityAndName("elliotalderson1@protonmail.ch")

        tags shouldHaveSize 209
        // first two tags should be sorted by similarity
        tags[0].name shouldBe "computer"
        tags[1].name shouldBe "politics"
        // remaining tags should be sorted by name
        tags.subList(2, tags.size) shouldBeSortedWith { t1, t2 -> t1.name.compareTo(t2.name) }
    }

}