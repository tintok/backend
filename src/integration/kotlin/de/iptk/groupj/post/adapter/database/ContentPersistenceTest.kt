package de.iptk.groupj.post.adapter.database

import de.iptk.groupj.Cypher
import de.iptk.groupj.PersistenceIntegrationTest
import de.iptk.groupj.post.domain.ImageContentEntity
import de.iptk.groupj.post.domain.VideoContentEntity
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.optional.shouldBeEmpty
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

@Cypher("ContentPersistenceTest.cypher")
class ContentPersistenceTest @Autowired constructor(private val repository: ContentRepository) :
    PersistenceIntegrationTest() {

    @Test
    internal fun `find image content`() {
        val content = repository.findImageContent(UUID.fromString("00000000-0000-0000-0000-000000000007")).get()

        assertSoftly(content) {
            content.shouldBeTypeOf<ImageContentEntity>()
            id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000009")
            size shouldBe 69
            type shouldBe "image/jpeg"
        }
    }

    @Test
    internal fun `find non existing image content`() {
        val content = repository.findImageContent(UUID.fromString("00000000-0000-0000-0000-000000000008"))

        content.shouldBeEmpty()
    }

    @Test
    internal fun `find video content`() {
        val content = repository.findVideoContent(UUID.fromString("00000000-0000-0000-0000-000000000008")).get()

        assertSoftly(content) {
            content.shouldBeTypeOf<VideoContentEntity>()
            id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000010")
            size shouldBe 1337
            type shouldBe "video/mp4"
        }
    }

    @Test
    internal fun `find non existing video content`() {
        val content = repository.findVideoContent(UUID.fromString("00000000-0000-0000-0000-000000000007"))

        content.shouldBeEmpty()
    }

    @Test
    internal fun `find all content from user`() {
        val test = repository.findContentFromUser("elliotalderson2@protonmail.ch")
        repository.findContentFromUser("elliotalderson2@protonmail.ch") shouldHaveSize 2
    }

}