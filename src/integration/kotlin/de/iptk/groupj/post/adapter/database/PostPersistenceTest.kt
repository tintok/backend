package de.iptk.groupj.post.adapter.database

import de.iptk.groupj.Cypher
import de.iptk.groupj.PersistenceIntegrationTest
import de.iptk.groupj.post.domain.*
import de.iptk.groupj.post.model.PostFeedDto
import de.iptk.groupj.shared.REC_RANDOM_LIKE_LIMIT
import de.iptk.groupj.shared.REC_RANDOM_POOL
import de.iptk.groupj.tag.adapter.database.TagRepository
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldBeSortedWith
import io.kotest.matchers.collections.shouldHaveSingleElement
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.optional.shouldBeEmpty
import io.kotest.matchers.optional.shouldBePresent
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.neo4j.core.Neo4jClient
import java.time.ZonedDateTime
import java.util.*

@Cypher("PostPersistenceTest.cypher")
internal class PostPersistenceTest @Autowired constructor(

    private val repository: PostRepository,

    private val client: Neo4jClient

) : PersistenceIntegrationTest() {

    @Nested
    inner class Mappings {
        @Test
        internal fun `text post`() {
            val post = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000001")).get()

            assertSoftly(post) {
                shouldBeInstanceOf<TextPostEntity>()
                id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000001")
                created shouldBe ZonedDateTime.parse("2020-12-14T17:57:00+01:00[Europe/Berlin]")
                updated shouldBe ZonedDateTime.parse("2020-12-14T17:57:00+01:00[Europe/Berlin]")
                location shouldBe Location(8.65493541, 49.87763091).asPoint()
                text shouldBe "TigerKing?"
                author.shouldNotBeNull()
                author!!.id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000069")
                tags.shouldNotBeNull()
                tags.first().name shouldBe "sport"
            }
        }

        @Test
        internal fun `image post`() {
            val post = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000002")).get()

            assertSoftly(post) {
                shouldBeInstanceOf<ImagePostEntity>()
                id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000002")
                created shouldBe ZonedDateTime.parse("2020-12-14T17:58:00+01:00[Europe/Berlin]")
                updated shouldBe ZonedDateTime.parse("2020-12-14T17:58:00+01:00[Europe/Berlin]")
                location shouldBe Location(8.68051002, 49.86112964).asPoint()
                text shouldBe "TigerKing ist einfach beste"
                author.shouldNotBeNull()
                author!!.id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000069")
                tags.shouldNotBeNull()
                tags.first().name shouldBe "sport"
            }
        }

        @Test
        internal fun `video post`() {
            val post = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000003")).get()

            assertSoftly(post) {
                shouldBeInstanceOf<VideoPostEntity>()
                id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000003")
                created shouldBe ZonedDateTime.parse("2020-12-14T17:59:00+01:00[Europe/Berlin]")
                updated shouldBe ZonedDateTime.parse("2020-12-14T17:59:00+01:00[Europe/Berlin]")
                location shouldBe Location(8.67393361, 49.87457339).asPoint()
                text shouldBe null
                author.shouldNotBeNull()
                author!!.id shouldBe UUID.fromString("00000000-0000-0000-0000-000000000069")
                tags.shouldNotBeNull()
                tags.first().name shouldBe "sport"
            }
        }
    }

    @Nested
    inner class FindOwnPosts {
        @Test
        internal fun `find all sorted correctly`() {
            val posts = repository.findAllYourPosts(
                "elliotalderson1@protonmail.ch", PageRequest.of(0, 20)
            )

            // Sub classes got resolved correctly
            posts.content.shouldHaveSingleElement { it::class == TextPostEntity::class }
            posts.content.shouldHaveSingleElement { it::class == ImagePostEntity::class }
            posts.content.shouldHaveSingleElement { it::class == VideoPostEntity::class }

            // check content is sorted correctly and tags are included
            posts.content.shouldBeSortedWith { p1, p2 -> p2.created!!.compareTo(p1.created) }
            posts.map(PostEntity::tags).forEach { tag -> tag.shouldNotBeEmpty() }
        }

        @Test
        internal fun `pageable page 0 is correct`() {
            val posts = repository.findAllYourPosts(
                "elliotalderson1@protonmail.ch", PageRequest.of(0, 2)
            )

            posts.totalElements shouldBe 3
            posts.totalPages shouldBe 2
            posts.numberOfElements shouldBe 2
            posts.isFirst.shouldBeTrue()
        }

        @Test
        internal fun `pageable page 1 is correct`() {
            val posts = repository.findAllYourPosts(
                "elliotalderson1@protonmail.ch", PageRequest.of(1, 2)
            )

            posts.totalElements shouldBe 3
            posts.totalPages shouldBe 2
            posts.numberOfElements shouldBe 1
            posts.isFirst.shouldBeFalse()
            posts.isLast.shouldBeTrue()
        }
    }

    /**
     * Only test that correct posts are returned, don't test sorting. This is done by the
     * postScore function which is tested extensively in it's own repository.
     */
    @Nested
    inner class FindRecommended {
        @Test
        internal fun `find recommended posts`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val posts = repository.findRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, emptySet(), 20
            )

            posts shouldHaveSize 3
            // Test Spring Data Inheritance derives correct Sub Class
            posts.shouldHaveSingleElement { it::class == TextPostEntity::class }
            posts.shouldHaveSingleElement { it::class == ImagePostEntity::class }
            posts.shouldHaveSingleElement { it::class == VideoPostEntity::class }
            // check tags are included
            posts.map(PostEntity::tags).forEach { it.shouldNotBeEmpty() }
        }

        @Test
        internal fun `find single recommended posts`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val posts = repository.findRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, emptySet(), 1
            )

            posts shouldHaveSize 1
        }

        @Test
        internal fun `find recommended except`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val excluded = setOf(UUID.fromString("00000000-0000-0000-0000-000000000001"))
            val posts = repository.findRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, excluded, 20
            )

            posts shouldHaveSize 2
        }

        @Test
        internal fun `recommended posts should have recommended relationship`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val username = "ellenalderson2@protonmail.ch"
            val posts = repository.findRecommendedPosts(username, location, emptySet(), 20)

            posts.map(PostEntity::id)
                .forEach { id ->
                    val statement =
                        "RETURN EXISTS((:User {username: '$username'}) <- [:RECOMMENDED] - (:Post {id: '$id'}))"
                    val isRecommended = client.query(statement)
                        .`in`(null)
                        .fetchAs(Boolean::class.java)
                        .one()
                        .get()
                    isRecommended.shouldBeTrue()
                }
        }

        @Test
        internal fun `do not find own posts`() {
            val location = Location(8.65493541, 49.87763091).asPoint()
            val posts = repository.findRecommendedPosts(
                "elliotalderson1@protonmail.ch", location, emptySet(), 20
            )

            posts shouldHaveSize 0
        }

        @Test
        internal fun `do not find liked and disliked posts`() {
            val location = Location(8.843097, 49.904935).asPoint()
            val posts = repository.findRecommendedPosts(
                "elliotalderson3@protonmail.ch", location, emptySet(), 20
            )

            posts shouldHaveSize 1
        }
    }

    @Nested
    inner class FindRandomRecommended {
        @Test
        internal fun `find random recommended post`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val posts = repository.findRandomRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, emptySet(), REC_RANDOM_POOL, REC_RANDOM_LIKE_LIMIT, 3
            )

            posts shouldHaveSize 3
            // Test Spring Data Inheritance derives correct Sub Class
            posts.shouldHaveSingleElement { it::class == TextPostEntity::class }
            posts.shouldHaveSingleElement { it::class == ImagePostEntity::class }
            posts.shouldHaveSingleElement { it::class == VideoPostEntity::class }
            // check tags are included
            posts.map(PostEntity::tags).forEach { it.shouldNotBeEmpty() }
        }

        @Test
        internal fun `find single random recommended post`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val posts = repository.findRandomRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, emptySet(), REC_RANDOM_POOL, REC_RANDOM_LIKE_LIMIT, 1
            )

            posts shouldHaveSize 1
        }

        @Test
        internal fun `find random recommended post with pool size`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val posts = repository.findRandomRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, emptySet(), 1, REC_RANDOM_LIKE_LIMIT, 3
            )

            posts shouldHaveSize 1
            // pool is restricted to one post, should always be the same (sorted by age and distance)
            posts shouldHaveSingleElement { it.id == UUID.fromString("00000000-0000-0000-0000-000000000003") }
        }

        @Test
        internal fun `find random recommended post with like limit`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val posts = repository.findRandomRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, emptySet(), REC_RANDOM_POOL, 1, 3
            )

            posts shouldHaveSize 1
            // pool is restricted to at least one like
            posts shouldHaveSingleElement { it.id == UUID.fromString("00000000-0000-0000-0000-000000000002") }
        }

        @Test
        internal fun `find random recommended except`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val excluded = setOf(UUID.fromString("00000000-0000-0000-0000-000000000001"))
            val posts = repository.findRandomRecommendedPosts(
                "ellenalderson2@protonmail.ch", location, excluded, REC_RANDOM_POOL, REC_RANDOM_LIKE_LIMIT, 3
            )

            posts shouldHaveSize 2
        }

        @Test
        internal fun `recommended posts should have recommended relationship`() {
            val location = Location(8.67393361, 49.87457339).asPoint()
            val username = "ellenalderson2@protonmail.ch"
            val posts = repository.findRandomRecommendedPosts(
                username, location, emptySet(), REC_RANDOM_POOL, REC_RANDOM_LIKE_LIMIT, 3
            )

            posts.map(PostEntity::id)
                .forEach { id ->
                    val statement =
                        "RETURN EXISTS((:User {username: '$username'}) <- [:RECOMMENDED] - (:Post {id: '$id'}))"
                    val isRecommended = client.query(statement)
                        .`in`(null)
                        .fetchAs(Boolean::class.java)
                        .one()
                        .get()
                    isRecommended.shouldBeTrue()
                }
        }

        @Test
        internal fun `do not find own posts`() {
            val location = Location(8.65493541, 49.87763091).asPoint()
            val posts = repository.findRandomRecommendedPosts(
                "elliotalderson1@protonmail.ch", location, emptySet(), REC_RANDOM_POOL, REC_RANDOM_LIKE_LIMIT, 3
            )

            posts shouldHaveSize 0
        }

        @Test
        internal fun `do not find liked and disliked posts`() {
            val location = Location(8.843097, 49.904935).asPoint()
            val posts = repository.findRandomRecommendedPosts(
                "elliotalderson3@protonmail.ch", location, emptySet(), REC_RANDOM_POOL, REC_RANDOM_LIKE_LIMIT, 3
            )

            posts shouldHaveSize 1
        }
    }

    @Nested
    inner class FindMatched {
        @Test
        internal fun `find matched posts`() {
            val posts = repository.findPostsFromMatches(
                "elliotalderson4@protonmail.ch",
                ZonedDateTime.parse("2020-12-14T17:56:59+01:00[Europe/Berlin]"),
                ZonedDateTime.parse("2020-12-14T17:59:01+01:00[Europe/Berlin]"),
                PageRequest.of(0, 3)
            )

            posts.numberOfElements shouldBe 3
            // Test type is derived correctly
            posts.shouldHaveSingleElement { it.type == PostType.TEXT }
            posts.shouldHaveSingleElement { it.type == PostType.IMAGE }
            posts.shouldHaveSingleElement { it.type == PostType.VIDEO }
            // check text is mapped correctly
            posts.filter { it.type == PostType.TEXT }.first().text shouldBe "TigerKing?"
            posts.filter { it.type == PostType.IMAGE }.first().text shouldBe "TigerKing ist einfach beste"
            posts.filter { it.type == PostType.VIDEO }.first().text.shouldBeNull()
            // check tags are included
            posts.map(PostFeedDto::tags).forEach { post -> post.shouldNotBeEmpty() }
        }

        @Test
        internal fun `find matched posts nbf`() {
            val posts = repository.findPostsFromMatches(
                "elliotalderson4@protonmail.ch",
                ZonedDateTime.parse("2020-12-14T17:57:00+01:00[Europe/Berlin]"),
                ZonedDateTime.parse("2020-12-14T17:59:01+01:00[Europe/Berlin]"),
                PageRequest.of(0, 3)
            )

            posts.numberOfElements shouldBe 2
        }

        @Test
        internal fun `find matched posts naf`() {
            val posts = repository.findPostsFromMatches(
                "elliotalderson4@protonmail.ch",
                ZonedDateTime.parse("2020-12-14T17:56:59+01:00[Europe/Berlin]"),
                ZonedDateTime.parse("2020-12-14T17:59:00+01:00[Europe/Berlin]"),
                PageRequest.of(0, 3)
            )

            posts.numberOfElements shouldBe 2
        }

        @Test
        internal fun `find no matched posts`() {
            val posts = repository.findPostsFromMatches(
                "elliotalderson1@protonmail.ch",
                ZonedDateTime.parse("2020-12-14T17:56:59+01:00[Europe/Berlin]"),
                ZonedDateTime.parse("2020-12-14T17:59:01+01:00[Europe/Berlin]"),
                PageRequest.of(0, 3)
            )

            posts.numberOfElements shouldBe 0
        }
    }

    @Nested
    inner class Delete {
        @Test
        internal fun `image post`(@Autowired contentRepository: ContentRepository) {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000002")
            val username = "elliotalderson1@protonmail.ch"

            repository.deleteOwnPost(id, username)

            // assert post was deleted
            val post = repository.findById(id)
            post.shouldBeEmpty()

            contentRepository.findImageContent(id).shouldBeEmpty()
        }

        @Test
        internal fun `video post`(@Autowired contentRepository: ContentRepository) {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000003")
            val username = "elliotalderson1@protonmail.ch"

            repository.deleteOwnPost(id, username)

            // assert post was deleted
            val post = repository.findById(id)
            post.shouldBeEmpty()
            contentRepository.findVideoContent(id).shouldBeEmpty()
        }
    }

    @Nested
    inner class Predicates {
        @Test
        internal fun `find by id and author username`() {
            val post = repository.findByIdAndAuthorUsername(
                UUID.fromString("00000000-0000-0000-0000-000000000001"),
                "elliotalderson1@protonmail.ch"
            )

            post.shouldBePresent()
        }

        @Test
        internal fun `post should be from user`() {
            repository.isPostFromUser(
                UUID.fromString("00000000-0000-0000-0000-000000000001"),
                "elliotalderson1@protonmail.ch"
            ).shouldBeTrue()
        }

        @Test
        internal fun `post should not be from user`() {
            repository.isPostFromUser(
                UUID.fromString("00000000-0000-0000-0000-000000000001"),
                "elliotalderson3@protonmail.ch"
            ).shouldBeFalse()
        }

        @Test
        internal fun `post should be from match`() {
            repository.isPostFromMatch(
                UUID.fromString("00000000-0000-0000-0000-000000000001"),
                "elliotalderson4@protonmail.ch"
            ).shouldBeTrue()
        }

        @Test
        internal fun `post should not be from match`() {
            repository.isPostFromMatch(
                UUID.fromString("00000000-0000-0000-0000-000000000001"),
                "elliotalderson1@protonmail.ch"
            ).shouldBeFalse()
        }

        @Test
        internal fun `post should be recommended`() {
            repository.isRecommendedPost(
                UUID.fromString("00000000-0000-0000-0000-000000000002"),
                "elliotalderson3@protonmail.ch"
            ).shouldBeTrue()
        }

        @Test
        internal fun `expired recommendation should not be recommended`() {
            repository.isRecommendedPost(
                UUID.fromString("00000000-0000-0000-0000-000000000003"),
                "elliotalderson3@protonmail.ch"
            ).shouldBeFalse()
        }

        @Test
        internal fun `post should not be recommended`() {
            repository.isRecommendedPost(
                UUID.fromString("00000000-0000-0000-0000-000000000002"),
                "elliotalderson2@protonmail.ch"
            ).shouldBeFalse()
        }
    }

    @Nested
    inner class Update {
        @Test
        internal fun `text of post`() {
            val post = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000003")).get()
            post.text = "changed"
            repository.save(post)

            val updatedPost = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000003")).get()
            updatedPost.text shouldBe post.text
        }

        @Test
        internal fun `tags of post`(@Autowired tagRepository: TagRepository) {
            val post = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000001")).get()

            post.tags = setOf(tagRepository.findByName("computer").get())
            repository.save(post)

            val updatedPost = repository.findById(UUID.fromString("00000000-0000-0000-0000-000000000001")).get()
            assertSoftly(updatedPost) {
                tags shouldHaveSize 1
                tags.first() shouldBe post.tags.first()
                text shouldBe post.text
            }
        }
    }

    @Nested
    inner class Like {
        @Test
        internal fun like() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000001")
            val username = "elliotalderson3@protonmail.ch"
            repository.like(id, username)

            val statement = "RETURN EXISTS( (:User {username: '$username'})-[:LIKED]->(:Post {id: '$id'}) )"
            val isRelationshipCreated = client.query(statement)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()

            isRelationshipCreated.shouldBeTrue()
        }

        @Test
        internal fun `like of already liked post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000002")
            val username = "elliotalderson3@protonmail.ch"
            repository.like(id, username)

            val statement = "RETURN COUNT( (:User {username: '$username'})-[:LIKED]->(:Post {id: '$id'}) )"
            val numberOfRelationships = client.query(statement)
                .`in`(null)
                .fetchAs(Long::class.java)
                .one()
                .get()

            numberOfRelationships shouldBe 1
        }

        @Test
        internal fun `like of already disliked post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000003")
            val username = "elliotalderson3@protonmail.ch"
            repository.like(id, username)

            // Test that post is liked
            val likeQuery = "RETURN EXISTS( (:User {username: '$username'})-[:LIKED]->(:Post {id: '$id'}) )"
            val liked = client.query(likeQuery)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()
            liked.shouldBeTrue()

            // Test that post is not disliked anymore
            val dislikeQuery = "RETURN EXISTS( (:User {username: '$username'})-[:DISLIKED]->(:Post {id: '$id'}) )"
            val disliked = client.query(dislikeQuery)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()

            disliked.shouldBeFalse()
        }

        @Test
        internal fun dislike() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000001")
            val username = "elliotalderson3@protonmail.ch"
            repository.dislike(id, username)

            val statement = "RETURN EXISTS( (:User {username: '$username'})-[:DISLIKED]->(:Post {id: '$id'}) )"
            val isRelationshipCreated = client.query(statement)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()

            isRelationshipCreated.shouldBeTrue()
        }

        @Test
        internal fun `dislike of already disliked post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000003")
            val username = "elliotalderson3@protonmail.ch"
            repository.dislike(id, username)

            val statement = "RETURN COUNT( (:User {username: '$username'})-[:LIKED]->(:Post {id: '$id'}) )"
            val numberOfRelationships = client.query(statement)
                .`in`(null)
                .fetchAs(Long::class.java)
                .one()
                .get()

            numberOfRelationships shouldBe 1
        }

        @Test
        internal fun `dislike of already liked post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000002")
            val username = "elliotalderson3@protonmail.ch"
            repository.dislike(id, username)

            // Test that post is disliked
            val dislikeQuery = "RETURN EXISTS( (:User {username: '$username'})-[:DISLIKED]->(:Post {id: '$id'}) )"
            val disliked = client.query(dislikeQuery)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()
            disliked.shouldBeTrue()

            // Test that post is not liked anymore
            val likeQuery = "RETURN EXISTS( (:User {username: '$username'})-[:LIKED]->(:Post {id: '$id'}) )"
            val liked = client.query(likeQuery)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()

            liked.shouldBeFalse()
        }

        @Test
        internal fun `clear liked post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000002")
            val username = "elliotalderson3@protonmail.ch"
            repository.deleteLikeOrDislike(id, username).shouldBeTrue()

            val statement = "RETURN EXISTS( (:User {username: '$username'})-[:LIKED]->(:Post {id: '$id'}) )"
            val isRelationshipCreated = client.query(statement)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()

            isRelationshipCreated.shouldBeFalse()
        }

        @Test
        internal fun `clear disliked post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000003")
            val username = "elliotalderson3@protonmail.ch"
            repository.deleteLikeOrDislike(id, username).shouldBeTrue()

            val statement = "RETURN EXISTS( (:User {username: '$username'})-[:DISLIKED]->(:Post {id: '$id'}) )"
            val isRelationshipCreated = client.query(statement)
                .`in`(null)
                .fetchAs(Boolean::class.java)
                .one()
                .get()

            isRelationshipCreated.shouldBeFalse()
        }

        @Test
        internal fun `clear neutral post`() {
            val id = UUID.fromString("00000000-0000-0000-0000-000000000001")
            val username = "elliotalderson3@protonmail.ch"
            repository.deleteLikeOrDislike(id, username).shouldBeFalse()
        }
    }

}