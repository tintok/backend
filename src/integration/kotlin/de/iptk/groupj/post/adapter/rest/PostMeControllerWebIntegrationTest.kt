package de.iptk.groupj.post.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.*
import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.post.application.PostMeService
import de.iptk.groupj.post.domain.PostType
import de.iptk.groupj.post.exception.TextPostBlankTextException
import de.iptk.groupj.post.mockImagePost
import de.iptk.groupj.post.mockTextPost
import de.iptk.groupj.post.mockVideoPost
import de.iptk.groupj.shared.POSTS_ME
import de.iptk.groupj.shared.POST_TAG_MAX_SIZE
import de.iptk.groupj.tag.application.TagService
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.tag.mockTag
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import org.hamcrest.collection.IsCollectionWithSize.hasSize
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.core.io.Resource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.*
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.multipart
import org.springframework.test.web.servlet.put

@WebMvcTest(controllers = [PostMeController::class])
internal class PostMeControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var postMeService: PostMeService

    @MockkBean
    private lateinit var tagService: TagService

    @Nested
    @DefaultMockUser
    inner class Create {
        @Test
        internal fun `valid text post`() {
            val mockTags = mutableSetOf(mockTag())
            val mockPost = mockTextPost(tags = mockTags)
            every { tagService.findAllTagsByName(any()) } returns mockTags
            every { postMeService.createTextPost(any()) } returns mockPost

            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to mockPost.location.x(),
                    "latitude" to mockPost.location.y()
                ),
                "tags" to mockPost.tags.map(TagEntity::name),
                "text" to "This is a text"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("text_post", payload))
            }.andExpect {
                status { isCreated() }
                content {
                    contentType("$APPLICATION_JSON_VALUE;charset=UTF-8")
                    jsonPath("$.id") { uuid(mockPost.id) }
                    jsonPath("$.text") { value(mockPost.text) }
                    jsonPath("$.tags") { value(mockPost.tags.map(TagEntity::name)) }
                    jsonPath("$.type") { enum(PostType.TEXT) }
                    jsonPath("$.created") { zonedDateTime(mockPost.created) }
                    jsonPath("$.updated") { zonedDateTime(mockPost.updated) }
                    jsonPath("$.location.longitude") { value(mockPost.location.x()) }
                    jsonPath("$.location.latitude") { value(mockPost.location.y()) }
                }
            }
        }

        @Test
        internal fun `invalid text post with blank text`() {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("sport"),
                "text" to " "
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("text_post", payload))
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }

        @Test
        internal fun `invalid post with empty tags`() {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to emptySet<String>(),
                "text" to "Test text"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("text_post", payload))
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }

        @Test
        internal fun `invalid post with too many tags`() {
            val tags = IntRange(0, POST_TAG_MAX_SIZE + 1).map(Int::toString)
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to tags,
                "text" to "Test text"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("text_post", payload))
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }

        @Test
        internal fun `valid image post`(@Value("classpath:sample_image.png") image: Resource) {
            val mockTags = mutableSetOf(mockTag())
            val mockPost = mockImagePost(tags = mockTags)
            every { tagService.findAllTagsByName(any()) } returns mockTags
            every { postMeService.createImagePost(any(), any(), any()) } returns mockPost

            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to mockPost.location.x(),
                    "latitude" to mockPost.location.y()
                ),
                "tags" to mockPost.tags.map(TagEntity::name),
                "text" to mockPost.text
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("image_post", payload))
                file(mockMultipartFile("image", "sample_image.png", IMAGE_PNG_VALUE, image))
            }.andExpect {
                status { isCreated() }
                content {
                    contentType("$APPLICATION_JSON_VALUE;charset=UTF-8")
                    jsonPath("$.id") { uuid(mockPost.id) }
                    jsonPath("$.text") { value(mockPost.text) }
                    jsonPath("$.tags") { value(mockPost.tags.map(TagEntity::name)) }
                    jsonPath("$.type") { enum(PostType.IMAGE) }
                    jsonPath("$.created") { zonedDateTime(mockPost.created) }
                    jsonPath("$.updated") { zonedDateTime(mockPost.updated) }
                    jsonPath("$.location.longitude") { value(mockPost.location.x()) }
                    jsonPath("$.location.latitude") { value(mockPost.location.y()) }
                }
            }
        }

        @Test
        internal fun `valid image post with empty text`(@Value("classpath:sample_image.png") image: Resource) {
            val mockPost = mockImagePost(text = null)
            every { tagService.findAllTagsByName(any()) } returns mockPost.tags
            every { postMeService.createImagePost(any(), any(), any()) } returns mockPost

            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to mockPost.location.x(),
                    "latitude" to mockPost.location.y()
                ),
                "tags" to mockPost.tags.map(TagEntity::name),
                "text" to mockPost.text
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("image_post", payload))
                file(mockMultipartFile("image", "sample_image.png", IMAGE_PNG_VALUE, image))
            }.andExpect {
                status { isCreated() }
            }
        }

        @Test
        internal fun `invalid image post without image`() {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("sport"),
                "text" to "This is an image"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("image_post", payload))
            }.andExpect {
                status { isBadRequest() }
            }
        }

        @Test
        internal fun `invalid image post without image attributes`(@Value("classpath:sample_image.png") image: Resource) {
            mvc.multipart(POSTS_ME) {
                file(mockMultipartFile("image", "sample_image.png", IMAGE_PNG_VALUE, image))
            }.andExpect {
                status { isBadRequest() }
            }
        }

        @Test
        internal fun `invalid image post with non image content type`(@Value("classpath:sample_video.mp4") video: Resource) {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("test"),
                "text" to "foo"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("image_post", payload))
                file(mockMultipartFile("image", "sample_image.png", "video/mp4", video))
            }.andExpect {
                status { isUnsupportedMediaType() }
            }
        }

        @Test
        internal fun `invalid image post with empty image content type`(@Value("classpath:sample_image.png") image: Resource) {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("test"),
                "text" to "foo"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("image_post", payload))
                file(mockMultipartFile("image", "sample_image.png", "", image))
            }.andExpect {
                status { isUnsupportedMediaType() }
            }
        }

        @Test
        internal fun `valid video post`(@Value("classpath:sample_video.mp4") video: Resource) {
            val mockTags = mutableSetOf(mockTag())
            val mockPost = mockVideoPost(tags = mockTags)
            every { tagService.findAllTagsByName(any()) } returns mockTags
            every { postMeService.createVideoPost(any(), any(), any()) } returns mockPost

            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to mockPost.location.x(),
                    "latitude" to mockPost.location.y()
                ),
                "tags" to mockPost.tags.map(TagEntity::name),
                "text" to mockPost.text
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("video_post", payload))
                file(mockMultipartFile("video", "sample_video.mp4", "video/mp4", video))
            }.andExpect {
                status { isCreated() }
                content {
                    contentType("$APPLICATION_JSON_VALUE;charset=UTF-8")
                    jsonPath("$.id") { uuid(mockPost.id) }
                    jsonPath("$.text") { value(mockPost.text) }
                    jsonPath("$.tags") { value(mockPost.tags.map(TagEntity::name)) }
                    jsonPath("$.type") { enum(PostType.VIDEO) }
                    jsonPath("$.created") { zonedDateTime(mockPost.created) }
                    jsonPath("$.updated") { zonedDateTime(mockPost.updated) }
                    jsonPath("$.location.longitude") { value(mockPost.location.x()) }
                    jsonPath("$.location.latitude") { value(mockPost.location.y()) }
                }
            }
        }

        @Test
        internal fun `invalid video post without video`() {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("sport"),
                "text" to "This is a video"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("video_post", payload))
            }.andExpect {
                status { isBadRequest() }
            }
        }

        @Test
        internal fun `invalid video post without video attributes`(@Value("classpath:sample_video.mp4") video: Resource) {
            mvc.multipart(POSTS_ME) {
                file(mockMultipartFile("video", "sample_video.mp4", "video/mp4", video))
            }.andExpect {
                status { isBadRequest() }
            }
        }

        @Test
        internal fun `create invalid video post with non video content type`(@Value("classpath:sample_video.mp4") video: Resource) {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("sport"),
                "text" to "This is a video"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("video_post", payload))
                file(mockMultipartFile("video", "sample_video.mp4", IMAGE_JPEG_VALUE, video))
            }.andExpect {
                status { isUnsupportedMediaType() }
            }
        }

        @Test
        internal fun `invalid create with video and image attributes`(@Value("classpath:sample_video.mp4") video: Resource) {
            val payload = mapOf(
                "location" to mapOf(
                    "longitude" to 0.0,
                    "latitude" to 0.0
                ),
                "tags" to setOf("sport"),
                "text" to "This is a video"
            )

            mvc.multipart(POSTS_ME) {
                part(mockMultipartJson("image_post", payload))
                part(mockMultipartJson("video_post", payload))
                file(mockMultipartFile("video", "sample_video.mp4", "video/mp4", video))
            }.andExpect {
                status { isBadRequest() }
            }
        }
    }

    @Nested
    @DefaultMockUser
    inner class Retrieve {
        @Test
        internal fun `empty post collection`() {
            every { postMeService.findAllMyPosts(any(), any()) } returns Page.empty()

            mvc.get(POSTS_ME)
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        internal fun `own posts`() {
            val posts = listOf(mockTextPost(), mockImagePost(), mockVideoPost())
            val request = PageRequest.of(0, posts.size)
            val page = PageImpl(posts, request, posts.size.toLong())
            every { postMeService.findAllMyPosts(any(), any()) } returns page

            mvc.get(POSTS_ME) {
                param("size", request.pageSize.toString())
                param("page", request.pageNumber.toString())
            }.andExpect {
                status { isOk() }
                jsonPath("$.content") { hasSize<Array<Any>>(3) }
                jsonPath("$.pageable") { isNotEmpty() }
            }
        }
    }

    @Nested
    @DefaultMockUser
    inner class Update {
        @Test
        internal fun `valid text post`() {
            every { tagService.findAllTagsByName(any()) } returns mockk()
            justRun { postMeService.update(any(), any(), any(), any()) }

            val payload = mapOf(
                "text" to "this is another text",
                "tags" to setOf("politics")
            )

            mvc.put("$POSTS_ME/00000000-0000-0000-0000-000000000001") {
                contentType = APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isNoContent() }
            }
        }

        @Test
        internal fun `text post with invalid blank text`() {
            every { tagService.findAllTagsByName(any()) } returns mockk()
            every { postMeService.update(any(), any(), any(), any()) } throws TextPostBlankTextException()

            val payload = mapOf(
                "text" to "  ",
                "tags" to setOf("politics")
            )

            mvc.put("$POSTS_ME/00000000-0000-0000-0000-000000000001") {
                contentType = APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }

        @Test
        internal fun `text post to empty invalid tags`() {
            val payload = mapOf(
                "text" to "test",
                "tags" to emptySet<String>()
            )

            mvc.put("$POSTS_ME/00000000-0000-0000-0000-000000000001") {
                contentType = APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }

        @Test
        internal fun `invalid post with too many tags`() {
            val tags = IntRange(0, POST_TAG_MAX_SIZE + 1).map(Int::toString)
            val payload = mapOf(
                "text" to "test",
                "tags" to tags
            )

            mvc.put("$POSTS_ME/00000000-0000-0000-0000-000000000001") {
                contentType = APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }
    }

    @Nested
    @DefaultMockUser
    inner class Delete {
        @Test
        internal fun post() {
            justRun { postMeService.delete(any(), any()) }

            mvc.delete("$POSTS_ME/00000000-0000-0000-0000-000000000001")
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        internal fun `non existing post`() {
            every { postMeService.delete(any(), any()) } throws ResourceNotFoundException("", "")

            mvc.delete("$POSTS_ME/00000000-0000-0000-0000-000000000420")
                .andExpect {
                    status { isNotFound() }
                }
        }
    }

}