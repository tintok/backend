package de.iptk.groupj.post.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.*
import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.post.application.PostService
import de.iptk.groupj.post.domain.PostType
import de.iptk.groupj.post.domain.mockImageContent
import de.iptk.groupj.post.exception.PostToLikeIsOwnPost
import de.iptk.groupj.post.mockTextPost
import de.iptk.groupj.shared.CLEAR
import de.iptk.groupj.shared.DISLIKE
import de.iptk.groupj.shared.LIKE
import de.iptk.groupj.shared.POSTS
import de.iptk.groupj.tag.domain.TagEntity
import io.mockk.every
import io.mockk.justRun
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import java.util.*

@WebMvcTest(controllers = [PostController::class])
class PostControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var service: PostService

    @Nested
    @DefaultMockUser
    inner class Retrieve {
        @Test
        internal fun `post by id`() {
            val mockPost = mockTextPost()
            every { service.findPost(any()) } returns mockPost

            mvc.get("$POSTS/${mockPost.id}")
                .andExpect {
                    status { isOk() }
                    jsonPath("$.id") { uuid(mockPost.id) }
                    jsonPath("$.text") { value(mockPost.text) }
                    jsonPath("$.tags") { value(mockPost.tags.map(TagEntity::name)) }
                    jsonPath("$.type") { enum(PostType.TEXT) }
                    jsonPath("$.created") { zonedDateTime(mockPost.created) }
                    jsonPath("$.updated") { zonedDateTime(mockPost.updated) }
                    jsonPath("$.location.longitude") { value(mockPost.location.x()) }
                    jsonPath("$.location.latitude") { value(mockPost.location.y()) }
                }
        }

        @Test
        internal fun `non existing image from text post`() {
            every { service.findImagePostData(any()) } throws ResourceNotFoundException("", "")

            mvc.get("$POSTS/00000000-0000-0000-0000-000000000001/image")
                .andExpect {
                    status { isNotFound() }
                }
        }

        @Test
        internal fun `non existing video from text post`() {
            every { service.findVideoPostData(any()) } throws ResourceNotFoundException("", "")

            mvc.get("$POSTS/00000000-0000-0000-0000-000000000002/video")
                .andExpect {
                    status { isNotFound() }
                }
        }

        @Test
        internal fun `image from image post`(@Value("classpath:sample_image.png") image: Resource) {
            val mockImage = mockImageContent()
            every { service.findImagePostData(any()) } returns Pair(MediaType.IMAGE_PNG_VALUE, image)

            mvc.get("$POSTS/${mockImage.post.id}/image")
                .andExpect {
                    status { isOk() }
                    content { contentType(mockImage.type) }
                }
        }

        @Test
        internal fun `non existing video from image post`() {
            every { service.findVideoPostData(any()) } throws ResourceNotFoundException("", "")

            mvc.get("$POSTS/00000000-0000-0000-0000-000000000002/video")
                .andExpect {
                    status { isNotFound() }
                }
        }

        @Test
        internal fun `non existing image from video post`() {
            every { service.findImagePostData(any()) } throws ResourceNotFoundException("", "")

            mvc.get("$POSTS/00000000-0000-0000-0000-000000000003/image")
                .andExpect {
                    status { isNotFound() }
                }
        }

        @Test
        internal fun `video post retrieve video`(@Value("classpath:sample_video.mp4") video: Resource) {
            val contentType = "video/mp4"
            every { service.findVideoPostData(any()) } returns Pair(contentType, video)

            mvc.get("$POSTS/${UUID.randomUUID()}/video")
                .andExpect {
                    status { isOk() }
                    content { contentType(contentType) }
                }
        }
    }

    @Nested
    @DefaultMockUser
    inner class Like {
        @Test
        internal fun `like post`() {
            justRun { service.like(any(), any()) }

            mvc.post("$POSTS/${UUID.randomUUID()}$LIKE")
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        internal fun `like post from own user`() {
            every { service.like(any(), any()) } throws PostToLikeIsOwnPost()

            mvc.post("$POSTS/${UUID.randomUUID()}$LIKE")
                .andExpect {
                    status { isUnprocessableEntity() }
                }
        }

        @Test
        internal fun `like non existing post`() {
            every { service.like(any(), any()) } throws ResourceNotFoundException("", "")

            mvc.post("$POSTS/${UUID.randomUUID()}$LIKE")
                .andExpect {
                    status { isNotFound() }
                }
        }

        @Test
        internal fun `dislike post`() {
            justRun { service.dislike(any(), any()) }

            mvc.post("$POSTS/${UUID.randomUUID()}$DISLIKE")
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        internal fun `dislike post from own user`() {
            every { service.dislike(any(), any()) } throws PostToLikeIsOwnPost()

            mvc.post("$POSTS/${UUID.randomUUID()}$DISLIKE")
                .andExpect {
                    status { isUnprocessableEntity() }
                }
        }

        @Test
        internal fun `dislike non existing post`() {
            every { service.dislike(any(), any()) } throws ResourceNotFoundException("", "")

            mvc.post("$POSTS/${UUID.randomUUID()}$DISLIKE")
                .andExpect {
                    status { isNotFound() }
                }
        }

        @Test
        internal fun `clear post`() {
            justRun { service.clear(any(), any()) }

            mvc.post("$POSTS/${UUID.randomUUID()}$CLEAR")
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        internal fun `clear non existing post`() {
            every { service.clear(any(), any()) } throws ResourceNotFoundException("", "")

            mvc.post("$POSTS/${UUID.randomUUID()}$CLEAR")
                .andExpect {
                    status { isNotFound() }
                }
        }
    }

}