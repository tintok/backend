package de.iptk.groupj.post.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.post.application.PostFeedService
import de.iptk.groupj.post.mockImagePost
import de.iptk.groupj.post.mockTextPost
import de.iptk.groupj.post.mockVideoPost
import de.iptk.groupj.post.toFeedPostDto
import de.iptk.groupj.shared.POSTS_FEED
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.slot
import org.hamcrest.collection.IsCollectionWithSize
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.test.web.servlet.get
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@WebMvcTest(controllers = [PostFeedController::class])
@DefaultMockUser
class PostFeedControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var service: PostFeedService

    @Test
    internal fun `check empty content`() {
        every { service.findPostsFromMatches(any(), any(), any(), any()) } returns Page.empty()

        mvc.get(POSTS_FEED)
            .andExpect {
                status { isNoContent() }
            }
    }

    @Test
    internal fun `correct nbf parameter if parameter not present`() {
        val nbf = slot<ZonedDateTime>()
        every { service.findPostsFromMatches(any(), capture(nbf), any(), any()) } returns Page.empty()

        mvc.get(POSTS_FEED) {
            param("page", "0")
            param("size", "20")
            param("naf", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        }.andExpect {
            status { isNoContent() }
        }

        nbf.captured shouldBe ZonedDateTime.of(1900, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault())
    }

    @Test
    internal fun `correct naf parameter if parameter not present`() {
        val naf = slot<ZonedDateTime>()
        every { service.findPostsFromMatches(any(), any(), capture(naf), any()) } returns Page.empty()

        mvc.get(POSTS_FEED) {
            param("page", "0")
            param("size", "20")
            param("nbf", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        }.andExpect {
            status { isNoContent() }
        }

        naf.captured shouldBe ZonedDateTime.of(2100, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault())
    }

    @Test
    internal fun `correct naf and nbf parameter if parameter present`() {
        val timestamp = ZonedDateTime.ofInstant(LocalDateTime.now(), ZoneOffset.of("+01:00"), ZoneOffset.of("+01:00"))
        val naf = slot<ZonedDateTime>()
        val nbf = slot<ZonedDateTime>()
        every { service.findPostsFromMatches(any(), capture(nbf), capture(naf), any()) } returns Page.empty()

        mvc.get(POSTS_FEED) {
            param("page", "0")
            param("size", "20")
            param("naf", timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
            param("nbf", timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        }.andExpect {
            status { isNoContent() }
        }

        naf.captured shouldBe timestamp
        nbf.captured shouldBe timestamp
    }

    @Test
    internal fun `check content`() {
        val posts = listOf(
            mockTextPost().toFeedPostDto(),
            mockImagePost().toFeedPostDto(),
            mockVideoPost().toFeedPostDto()
        )
        val request = PageRequest.of(0, posts.size)

        every { service.findPostsFromMatches(any(), any(), any(), any()) } returns PageImpl(
            posts,
            request,
            posts.size.toLong()
        )

        mvc.get(POSTS_FEED)
            .andExpect {
                status { isOk() }
                jsonPath("$.content") { IsCollectionWithSize.hasSize<Array<Any>>(3) }
                jsonPath("$.pageable") { isNotEmpty() }
            }
    }

}