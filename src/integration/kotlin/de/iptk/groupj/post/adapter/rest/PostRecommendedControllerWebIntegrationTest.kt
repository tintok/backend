package de.iptk.groupj.post.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.post.application.RecommendedPostsService
import de.iptk.groupj.post.mockImagePost
import de.iptk.groupj.post.mockTextPost
import de.iptk.groupj.post.mockVideoPost
import de.iptk.groupj.shared.POSTS_RECOMMENDED
import io.mockk.every
import org.hamcrest.collection.IsCollectionWithSize
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.get
import java.util.*

@WebMvcTest(controllers = [PostRecommendedController::class])
class PostRecommendedControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var recommendedPostsService: RecommendedPostsService

    @Nested
    @DefaultMockUser
    inner class Retrieve {
        @Test
        internal fun `retrieve empty post collection`() {
            every { recommendedPostsService.findRecommendedPosts(any(), any(), any(), any()) } returns emptySet()

            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "180")
                param("latitude", "-90")
            }.andExpect {
                status { isNoContent() }
            }
        }

        @Test
        internal fun `retrieve recommended posts`() {
            val exceptId = UUID.randomUUID()
            val except = setOf(exceptId.toString())
            val posts = setOf(mockTextPost(), mockImagePost(), mockVideoPost())
            every {
                recommendedPostsService.findRecommendedPosts(any(), any(), eq(setOf(exceptId)), any())
            } returns posts

            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
                param("latitude", "0")
                param("size", posts.size.toString())
                param("except", *except.toTypedArray())
            }.andExpect {
                status { isOk() }
                jsonPath("$") { IsCollectionWithSize.hasSize<Array<Any>>(3) }
            }
        }
    }

    @Nested
    @DefaultMockUser
    inner class InvalidRequests {
        @Test
        internal fun `invalid size parameter`() {
            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
                param("latitude", "0")
                param("size", "${Int.MAX_VALUE}")
            }.andExpect { status { isUnprocessableEntity() } }

            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
                param("latitude", "0")
                param("size", "0")
            }.andExpect { status { isUnprocessableEntity() } }
        }

        @Test
        internal fun `invalid except parameter`() {
            val except = IntRange(0, 101).asSequence()
                .map { UUID.randomUUID() }
                .map(UUID::toString)
                .toList()
                .toTypedArray()

            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
                param("latitude", "0")
                param("size", "${Int.MAX_VALUE}")
                param("except", *except)
            }.andExpect { status { isUnprocessableEntity() } }
        }

        @Test
        internal fun `missing parameters`() {
            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
            }.andExpect { status { isBadRequest() } }

            mvc.get(POSTS_RECOMMENDED) {
                param("latitude", "0")
            }.andExpect { status { isBadRequest() } }
        }

        @Test
        internal fun `invalid parameters`() {
            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "not a number")
                param("latitude", "0")
                param("size", "10")
            }.andExpect { status { isBadRequest() } }

            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
                param("latitude", "not a number")
                param("size", "10")
            }.andExpect { status { isBadRequest() } }

            mvc.get(POSTS_RECOMMENDED) {
                param("longitude", "0")
                param("latitude", "0")
                param("size", "not a number")
            }.andExpect { status { isBadRequest() } }
        }
    }

}