package de.iptk.groupj.user.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.shared.USER_ME
import de.iptk.groupj.shared.USER_ME_PASSWORD
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.exception.OldPasswordIncorrectException
import de.iptk.groupj.user.mockUser
import io.mockk.every
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.put

@WebMvcTest(controllers = [UserMeController::class])
internal class UserMeControllerWebIntegrationTest : WebIntegrationTest() {

    private val mockUser = mockUser()

    @MockkBean
    private lateinit var service: UserService

    @Nested
    @DefaultMockUser
    inner class User {
        @Test
        internal fun `user me details`() {
            mvc.get(USER_ME)
                .andExpect {
                    status { isOk() }
                    jsonPath("$.id") { isNotEmpty() }
                    jsonPath("$.username") { value(mockUser.username) }
                    jsonPath("$.given_name") { value(mockUser.givenName) }
                    jsonPath("$.family_name") { value(mockUser.familyName) }
                    jsonPath("$.location") { value(mockUser.location) }
                    jsonPath("$.biography") { value(mockUser.biography) }
                }
        }

        @Test
        internal fun `update user me`() {
            val payload = mapOf(
                "given_name" to "Elliot",
                "family_name" to "Alderson",
                "location" to "changed Location",
                "biography" to "Hey!"
            )

            mvc.put(USER_ME) {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isNoContent() }
            }
        }

        @Test
        internal fun `delete own user`() {
            mvc.delete(USER_ME)
                .andExpect {
                    status { isNoContent() }
                    jsonPath("$") { doesNotExist() }
                }
        }
    }

    @Nested
    @DefaultMockUser
    inner class Password {
        @Test
        internal fun `change password`() {
            val payload = mapOf(
                "old_password" to "password",
                "new_password" to "newPassword"
            )

            mvc.put("$USER_ME/$USER_ME_PASSWORD") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isNoContent() }
                jsonPath("$") { doesNotExist() }
            }
        }

        @Test
        internal fun `change password with invalid old password`() {
            every { service.changePassword(any(), any(), any()) } throws OldPasswordIncorrectException()

            val payload = mapOf(
                "old_password" to "password",
                "new_password" to "newPassword"
            )

            mvc.put("$USER_ME/$USER_ME_PASSWORD") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isForbidden() }
            }
        }
    }

}