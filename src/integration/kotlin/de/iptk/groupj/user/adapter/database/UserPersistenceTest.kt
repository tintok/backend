package de.iptk.groupj.user.adapter.database

import de.iptk.groupj.Cypher
import de.iptk.groupj.PersistenceIntegrationTest
import de.iptk.groupj.chat.adapter.database.ChatRepository
import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.tag.adapter.database.TagRepository
import de.iptk.groupj.user.domain.AuthProvider
import de.iptk.groupj.user.domain.UserEntity
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.optional.shouldBeEmpty
import io.kotest.matchers.optional.shouldBePresent
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import java.util.*


/**
 * User persistence test.
 */
@Cypher("UserPersistenceTest.cypher")
internal class UserPersistenceTest @Autowired constructor(val repository: UserRepository) :
    PersistenceIntegrationTest() {

    @Nested
    inner class Predicates {
        @Test
        internal fun `user should exist by its username`() {
            repository.existsByUsername("elliotalderson@protonmail.ch").shouldBeTrue()
        }

        @Test
        internal fun `user should exist by its username case insensitive`() {
            repository.existsByUsername("Elliotalderson@protonmail.ch").shouldBeTrue()
        }

        @Test
        internal fun `user should not exist by its username`() {
            repository.existsByUsername("zlliotalderson@protonmail.ch").shouldBeFalse()
        }

        @Test
        internal fun `user should exist by its id`() {
            repository.existsById(UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368ce")).shouldBeTrue()
        }

        @Test
        internal fun `user should not exist by its id`() {
            repository.existsById(UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368cf")).shouldBeFalse()
        }

        @Test
        internal fun `users should be matched by id`() {
            repository.isMatched(
                UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368ce"),
                UUID.fromString("00000000-0000-0000-0000-000000000069")
            ).shouldBeTrue()
        }

        @Test
        internal fun `users should not be matched by id`() {
            repository.isMatched(
                UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368ce"),
                UUID.fromString("00000000-0000-0000-0000-000000000420")
            ).shouldBeFalse()
        }

        @Test
        internal fun `users should be matched by name`() {
            repository.isMatched("elliotalderson@protonmail.ch", "foo@bar.de").shouldBeTrue()
        }

        @Test
        internal fun `users should not be matched by name`() {
            repository.isMatched("elliotalderson@protonmail.ch", "test@bar.de").shouldBeFalse()
        }
    }

    @Nested
    inner class Save {
        @Test
        internal fun `save matched`() {
            // confirm users are not matched
            repository.isMatched("foo@bar.de", "test@bar.de").shouldBeFalse()
            // save match
            repository.saveMatched("foo@bar.de", "test@bar.de")
            // check if users are matched now
            repository.isMatched("foo@bar.de", "test@bar.de").shouldBeTrue()
        }
    }

    @Nested
    inner class Mapping {
        @Test
        internal fun user() {
            val user: UserEntity = repository.findByUsername("elliotalderson@protonmail.ch").get()
            assertSoftly(user) {
                id shouldBe UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368ce")
                username shouldBe "elliotalderson@protonmail.ch"
                givenName shouldBe "Elliot"
                location shouldBe "Darmstadt"
                biography shouldBe "Hey!"
                pictureId shouldBe UUID.fromString("4d4f3e4d-a658-48d8-a34b-0438d4d47698")
                pictureLength shouldBe 1337
                pictureMimeType shouldBe "image/jpeg"
                familyName shouldBe "Alderson"
                password shouldBe "secrete"
                provider shouldBe AuthProvider.LOCAL
                fcmToken shouldBe "some-group-token"
            }
        }
    }

    @Nested
    inner class Delete {
        @Test
        internal fun `user with posts which are liked and disliked is properly deleted`(
            @Autowired postRepository: PostRepository,
            @Autowired contentRepository: PostRepository,
            @Autowired tagRepository: TagRepository
        ) {
            val username = "elliotalderson@protonmail.ch"
            repository.deleteByUsername(username)
            repository.findByUsername(username).shouldBeEmpty()
            postRepository.findByIdAndAuthorUsername(
                UUID.fromString("47877713-2105-4299-9278-134950e8494c"),
                "elliotalderson@protonmail.ch"
            ).shouldBeEmpty()
            contentRepository.findById(UUID.fromString("3a00aec0-c579-48fe-b124-2daf71817524")).shouldBeEmpty()
            tagRepository.findByName("sport").shouldBePresent()
        }

        @Test
        internal fun `user which has liked and disliked posts gets deleted properly`() {
            val username = "elliotalderson3@protonmail.ch"
            repository.deleteByUsername(username)
            repository.findByUsername(username).shouldBeEmpty()
        }

        @Test
        internal fun `user which has chats gets deleted properly`(@Autowired chatRepository: ChatRepository) {
            val username = "elliotalderson@protonmail.ch"
            repository.deleteByUsername(username)
            repository.existsByUsername(username).shouldBeFalse()
            repository.existsByUsername("foo@bar.de").shouldBeTrue()
            chatRepository.existsById(UUID.fromString("28bab65b-d8d8-4e11-9cb9-c4b61b50489a")).shouldBeFalse()
            chatRepository.existsById(UUID.fromString("8079299b-010e-4bde-b93e-c2f9cc24ca12")).shouldBeFalse()
        }
    }

    @Nested
    inner class Matches {
        @Test
        internal fun `find match`() {
            val userOne = "elliotalderson@protonmail.ch"
            val userTwo = "foo@bar.de"

            val match = repository.findAllMatches(userOne)
            match shouldHaveSize 1
            match.first().username shouldBe userTwo
        }

        @Test
        internal fun `find match inverted`() {
            val userOne = "foo@bar.de"
            val userTwo = "elliotalderson@protonmail.ch"

            val match = repository.findAllMatches(userOne)
            match shouldHaveSize 1
            match.first().username shouldBe userTwo
        }

        @Test
        internal fun `no matches`() {
            repository.findAllMatches("test@bar.de") shouldHaveSize 0
        }
    }

    @Nested
    inner class FCM {
        private val userIdWithFCMToken = UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368ce")

        @Test
        internal fun `find all user with FCM token`() {
            repository.findAllUserIdsWhereFcmTokenIsNotNull() shouldContainExactly setOf(userIdWithFCMToken)
        }

        @Test
        internal fun `remove all FCM token where user id is in`() {
            repository.removeFcmTokenForUserIdIn(setOf(userIdWithFCMToken))
            repository.findByIdOrNull(userIdWithFCMToken)!!.fcmToken.shouldBeNull()
        }
    }

}