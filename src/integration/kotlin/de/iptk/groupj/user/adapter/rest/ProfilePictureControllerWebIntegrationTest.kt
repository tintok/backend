package de.iptk.groupj.user.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.shared.PROFILE_PICTURE
import de.iptk.groupj.shared.USER
import de.iptk.groupj.shared.USER_ME_PROFILE_PICTURE
import de.iptk.groupj.user.application.ProfilePictureService
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.multipart
import java.util.*

@DefaultMockUser
@WebMvcTest(controllers = [ProfilePictureController::class])
class ProfilePictureControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var profilePictureService: ProfilePictureService

    @Test
    internal fun `retrieve profile picture`(@Value("classpath:sample_image.png") image: Resource) {
        every { profilePictureService.find(any()) } returns Pair(MediaType.IMAGE_PNG_VALUE, image)

        mvc.get("$USER/${UUID.randomUUID()}$PROFILE_PICTURE")
            .andExpect {
                status { isOk() }
                content {
                    contentType(MediaType.IMAGE_PNG_VALUE)
                }
            }
    }

    @Test
    internal fun `retrieve empty profile picture`() {
        every { profilePictureService.find(any()) } returns Pair(null, null)

        mvc.get("$USER/${UUID.randomUUID()}$PROFILE_PICTURE")
            .andExpect {
                status { isNoContent() }
            }
    }

    @Test
    internal fun `update profile picture`(@Value("classpath:sample_image.png") image: Resource) {
        val profileImage = MockMultipartFile(
            "picture", "sample_image.png", MediaType.IMAGE_PNG_VALUE, image.inputStream
        )

        mvc.multipart(USER_ME_PROFILE_PICTURE) {
            file(profileImage)
        }.andExpect {
            status { isNoContent() }
            jsonPath("$") { doesNotExist() }
        }
    }

    @Test
    internal fun `update profile picture with invalid content type`() {
        val profileImage = MockMultipartFile(
            "picture", "tiger.pdf", MediaType.APPLICATION_PDF_VALUE, "content".toByteArray()
        )

        mvc.multipart(USER_ME_PROFILE_PICTURE) {
            file(profileImage)
        }.andExpect {
            status { isUnsupportedMediaType() }
        }
    }

    @Test
    internal fun `update profile picture with null content type`() {
        val profileImage = MockMultipartFile(
            "picture", "tiger.", null, "content".toByteArray()
        )

        mvc.multipart(USER_ME_PROFILE_PICTURE) {
            file(profileImage)
        }.andExpect {
            status { isUnsupportedMediaType() }
        }
    }

    @Test
    internal fun `delete profile picture`() {
        mvc.delete(USER_ME_PROFILE_PICTURE)
            .andExpect {
                status { isNoContent() }
                jsonPath("$") { doesNotExist() }
            }
    }

}