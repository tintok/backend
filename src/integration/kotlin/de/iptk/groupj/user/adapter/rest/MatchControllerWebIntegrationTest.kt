package de.iptk.groupj.user.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.shared.MATCHES
import de.iptk.groupj.user.application.MatchesService
import de.iptk.groupj.user.mockUser
import io.mockk.every
import org.hamcrest.collection.IsCollectionWithSize
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.get

@DefaultMockUser
@WebMvcTest(controllers = [MatchController::class])
internal class MatchControllerWebIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var matchesService: MatchesService

    @Test
    internal fun `no matches`() {
        every { matchesService.findAllMatches(any()) } returns emptySet()

        mvc.get(MATCHES) {
            param("page", "0")
            param("size", "20")
        }.andExpect {
            status { isNoContent() }
        }
    }

    @Test
    internal fun `find all matches`() {
        every { matchesService.findAllMatches(any()) } returns setOf(mockUser())

        mvc.get(MATCHES)
            .andExpect {
                status { isOk() }
                jsonPath("$") { IsCollectionWithSize.hasSize<Array<Any>>(1) }
            }
    }

}