package de.iptk.groupj

import org.springframework.security.test.context.support.WithSecurityContext

/**
 * Annotation for injecting a custom MockUser in Springs security Context for tests. It injects a user with the given
 * password and username.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@WithSecurityContext(factory = SecurityMockUserFactory::class)
annotation class MockUser(val username: String, val password: String)
