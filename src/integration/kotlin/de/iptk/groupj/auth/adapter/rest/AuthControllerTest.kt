package de.iptk.groupj.auth.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import com.ninjasquad.springmockk.SpykBean
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.security.application.JwtManager
import de.iptk.groupj.security.exception.InvalidTokenException
import de.iptk.groupj.shared.AUTH
import de.iptk.groupj.shared.REFRESH
import de.iptk.groupj.shared.SIGN_IN
import de.iptk.groupj.shared.SIGN_UP
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.exception.UsernameAlreadyInUseException
import de.iptk.groupj.user.mockUser
import io.mockk.every
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.post

@WebMvcTest(controllers = [AuthController::class])
internal class AuthControllerTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var service: UserService

    @SpykBean
    private lateinit var jwtManager: JwtManager

    @Nested
    inner class Login {
        @Test
        internal fun `valid login`() {
            val username = "elliotalderson@protonmail.ch"
            val password = "secret"

            every { userDetailsService.loadUserByUsername(username) } returns mockUser(password = password)

            val payload = mapOf(
                "username" to username,
                "password" to password
            )

            mvc.post("$AUTH$SIGN_IN") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isOk() }
                jsonPath("$.token") { isNotEmpty() }
                jsonPath("$.prefix") { value("Bearer") }
            }
        }

        @Test
        internal fun `invalid credentials`() {
            every { userDetailsService.loadUserByUsername(any()) } returns mockUser()

            val payload = mapOf(
                "username" to "elliotalderson@protonmail.ch",
                "password" to "invalidPassword"
            )

            mvc.post("$AUTH$SIGN_IN") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnauthorized() }
            }
        }
    }

    @Nested
    inner class SignUp {
        @Test
        internal fun `valid sign up`() {
            val mockUser = mockUser()

            every { service.existsByUsername(any()) } returns false
            every { service.create(any()) } returns mockUser

            val payload = mapOf(
                "username" to mockUser.username,
                "password" to "secret",
                "given_name" to mockUser.givenName,
                "family_name" to mockUser.familyName,
                "location" to mockUser.location,
                "biography" to mockUser.biography
            )

            mvc.post("$AUTH$SIGN_UP") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isCreated() }
                jsonPath("$.id") { isNotEmpty() }
                jsonPath("$.username") { value(mockUser.username) }
                jsonPath("$.given_name") { value(mockUser.givenName) }
                jsonPath("$.family_name") { value(mockUser.familyName) }
                jsonPath("$.location") { value(mockUser.location) }
                jsonPath("$.biography") { value(mockUser.biography) }
            }
        }

        @Test
        internal fun `already existing username`() {
            val userName = "elliotalderson@protonmail.ch"
            every { service.create(any()) } throws UsernameAlreadyInUseException(userName)

            val payload = mapOf(
                "username" to userName,
                "password" to "secret",
                "given_name" to "Elliot",
                "family_name" to "Alderson",
                "location" to "Darmstadt",
                "biography" to "Hey!"
            )

            mvc.post("$AUTH$SIGN_UP") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isConflict() }
            }
        }

        @Test
        internal fun `invalid username`() {
            val payload = mapOf(
                "username" to "invalidMail.com",
                "password" to "secret",
                "given_name" to "Elliot",
                "family_name" to "Alderson",
                "location" to "Darmstadt",
                "biography" to "Hey!"
            )

            mvc.post("$AUTH$SIGN_UP") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnprocessableEntity() }
            }
        }
    }

    @Nested
    inner class Refresh {
        @Test
        internal fun `valid refresh`() {
            val token =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MTYyMzkwMjJ9.tbDepxpstvGdW8TC3G8zg4B6rUYAOvfzdceoH48wgRQ"
            val username = "some-username@test.de"

            every { jwtManager.refresh(token) } returns token
            every { jwtManager.extractUsername(token) } returns username
            every { service.existsByUsername(username) } returns true

            val payload = mapOf("token" to token)
            mvc.post("$AUTH$REFRESH") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isOk() }
                jsonPath("$.token") { isNotEmpty() }
                jsonPath("$.prefix") { value("Bearer") }
            }
        }

        @Test
        internal fun `invalid refresh`() {
            every { jwtManager.refresh(any()) } throws InvalidTokenException("invalid")
            val payload = mapOf("token" to "some invalid jwt")

            mvc.post("$AUTH$REFRESH") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnauthorized() }
            }
        }

        @Test
        internal fun `non existing username`() {
            val token = "token"
            val username = "nonexisting@test.de"
            every { jwtManager.refresh(any()) } returns token
            every { jwtManager.extractUsername(token) } returns username
            every { service.existsByUsername(username) } returns false
            val payload = mapOf("token" to token)

            mvc.post("$AUTH$REFRESH") {
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(payload)
            }.andExpect {
                status { isUnauthorized() }
            }
        }
    }

}