package de.iptk.groupj.similarity.adapter.database

import com.fasterxml.jackson.databind.ObjectMapper
import de.iptk.groupj.Cypher
import de.iptk.groupj.PersistenceIntegrationTest
import de.iptk.groupj.similarity.model.PostInteractions
import de.iptk.groupj.similarity.model.Similarity
import de.iptk.groupj.similarity.model.TagInteractions
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.data.neo4j.core.Neo4jTemplate
import org.springframework.data.neo4j.core.PreparedQuery
import java.util.*

@Import(SimilarityRepositoryImpl::class)
@Cypher("SimilarityRepositoryImplTest.cypher")
class SimilarityPersistenceTest @Autowired constructor(private val repository: SimilarityRepositoryImpl) :
    PersistenceIntegrationTest() {

    @Nested
    inner class SaveSimilarities(

        @Autowired private val mapper: ObjectMapper,

        @Autowired private val template: Neo4jTemplate

    ) {

        @Test
        internal fun `save user to user similarities`() {
            val similarities = listOf(
                Similarity("me@test.de", "other1@test.de", 0, 1.0),
                Similarity("me@test.de", "other2@test.de", 0, 0.0),
                Similarity("me@test.de", "other3@test.de", 0, 0.0),
            )

            repository.saveUserToUserSimilarities(similarities)

            val statement = """
                MATCH (from:User) - [s:SIMILAR] -> (to:User) 
                RETURN from.username AS from, to.username AS to, s.score AS score
                """

            val query = PreparedQuery.queryFor(Similarity::class.java)
                .withCypherQuery(statement.trimIndent())
                .withParameters(emptyMap()) // Neo4jBug, throws NullPointer when no empty Map is set
                .usingMappingFunction { _, similarity ->
                    mapper.convertValue(similarity.asMap(), Similarity::class.java)
                }
                .build()

            template.toExecutableQuery(query).results shouldContainExactlyInAnyOrder similarities
        }

        @Test
        internal fun `save user to tag similarities`() {
            val similarities = listOf(
                Similarity("me@test.de", "computer", 0, 1.0),
                Similarity("me@test.de", "series", 0, 0.0),
                Similarity("me@test.de", "politics", 0, 0.0)
            )

            repository.saveUserToTagSimilarities(similarities)

            val statement = """
                MATCH (from:User) - [s:SIMILAR] -> (to:Tag)
                RETURN from.username AS from, to.name AS to, s.score AS score
                """

            val query = PreparedQuery.queryFor(Similarity::class.java)
                .withCypherQuery(statement.trimIndent())
                .withParameters(emptyMap()) // Neo4jBug, throws NullPointer when no empty Map is set
                .usingMappingFunction { _, similarity ->
                    mapper.convertValue(similarity.asMap(), Similarity::class.java)
                }
                .build()

            template.toExecutableQuery(query).results shouldContainExactlyInAnyOrder similarities
        }
    }

    @Nested
    inner class CountUserInteractions {
        @Test
        internal fun `count common likes`() {
            val commonLikes = repository.countCommonLikes(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000001")
            )

            commonLikes shouldContainExactlyInAnyOrder listOf(
                PostInteractions("other1@test.de", 2),
                PostInteractions("other2@test.de", 1),
                PostInteractions("other3@test.de", 1)
            )
        }

        @Test
        internal fun `count common dislikes`() {
            val commonDislikes = repository.countCommonDislikes(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000004")
            )

            commonDislikes shouldContainExactlyInAnyOrder listOf(
                PostInteractions("other3@test.de", 1)
            )
        }

        @Test
        internal fun `count opposed likes`() {
            val opposedLikes = repository.countOpposedLikes(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000001")
            )

            opposedLikes shouldContainExactlyInAnyOrder listOf(
                PostInteractions("other1@test.de", 1),
                PostInteractions("other2@test.de", 1)
            )
        }

        @Test
        internal fun `count opposed dislikes`() {
            val opposedDislikes = repository.countOpposedDislikes(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000004")
            )

            opposedDislikes shouldContainExactlyInAnyOrder listOf(
                PostInteractions("other3@test.de", 2)
            )
        }
    }

    @Nested
    inner class CountTagInteractions {
        @Test
        internal fun `count liked tags from own post`() {
            val likes = repository.countLikedTags(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000001")
            )

            likes shouldContainExactlyInAnyOrder listOf(
                TagInteractions("series", 2)
            )
        }

        @Test
        internal fun `count liked tags from other post`() {
            val likes = repository.countLikedTags(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000003")
            )

            likes shouldContainExactlyInAnyOrder listOf(
                TagInteractions("series", 2),
                TagInteractions("computer", 1)
            )
        }

        @Test
        internal fun `count disliked tags from own post`() {
            val dislikes = repository.countDislikedTags(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000002")
            )

            dislikes shouldContainExactlyInAnyOrder listOf(
                TagInteractions("politics", 1)
            )
        }

        @Test
        internal fun `count disliked tags from other post`() {
            val dislikes = repository.countDislikedTags(
                "me@test.de", UUID.fromString("00000000-0000-0000-0001-000000000004")
            )

            dislikes shouldContainExactlyInAnyOrder listOf(
                TagInteractions("politics", 1)
            )
        }
    }

}
