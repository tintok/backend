package de.iptk.groupj.chat.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.chat.application.ChatService
import de.iptk.groupj.chat.domain.mockChatMessage
import de.iptk.groupj.chat.model.ChatCommand
import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.shared.CHAT_MESSAGES
import io.mockk.every
import io.mockk.justRun
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import java.util.*

@DefaultMockUser
@WebMvcTest(controllers = [ChatMessageController::class])
internal class ChatMessageControllerIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var service: ChatService

    @Test
    internal fun send() {
        val recipientId = UUID.randomUUID()
        val message = "Hello!"

        every { service.sendChatMessage(any(), eq(recipientId), message) } returns mockChatMessage()

        mvc.post(CHAT_MESSAGES, recipientId) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(ChatCommand(message))
        }.andExpect {
            status { isCreated() }
        }
    }

    @Test
    internal fun `send to non existing user`() {
        val recipientId = UUID.randomUUID()

        every { service.sendChatMessage(any(), any(), any()) } throws ResourceNotFoundException("User", recipientId)

        mvc.post(CHAT_MESSAGES, recipientId) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(ChatCommand("NotFound"))
        }.andExpect {
            status { isNotFound() }
        }
    }

    @Test
    internal fun `mark as read`() {
        val recipientId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        justRun { service.markChatMessageAsRead(any(), eq(recipientId), eq(messageId)) }

        mvc.put("$CHAT_MESSAGES/{messageId}", recipientId, messageId)
            .andExpect {
                status { isNoContent() }
            }
    }

    @Test
    internal fun `mark as read non existing chat partner`() {
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every {
            service.markChatMessageAsRead(any(), eq(partnerId), eq(messageId))
        } throws ResourceNotFoundException("Chat Partner", partnerId)

        mvc.put("$CHAT_MESSAGES/{messageId}", partnerId, messageId)
            .andExpect {
                status { isNotFound() }
            }
    }

    @Test
    internal fun `mark as read non existing message`() {
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every {
            service.markChatMessageAsRead(any(), eq(partnerId), eq(messageId))
        } throws ResourceNotFoundException("Message in Conversation with partner $partnerId", messageId)

        mvc.put("$CHAT_MESSAGES/{messageId}", partnerId, messageId)
            .andExpect {
                status { isNotFound() }
            }
    }

    @Test
    internal fun `delete message`() {
        val recipientId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        justRun { service.deleteChatMessage(any(), eq(recipientId), eq(messageId)) }

        mvc.put("$CHAT_MESSAGES/{messageId}", recipientId, messageId)
            .andExpect {
                status { isNoContent() }
            }
    }

    @Test
    internal fun `delete message non existing chat partner`() {
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every {
            service.deleteChatMessage(any(), eq(partnerId), eq(messageId))
        } throws ResourceNotFoundException("Chat Partner", partnerId)

        mvc.delete("$CHAT_MESSAGES/{messageId}", partnerId, messageId)
            .andExpect {
                status { isNotFound() }
            }
    }

    @Test
    internal fun `delete non existing message`() {
        val partnerId = UUID.randomUUID()
        val messageId = UUID.randomUUID()

        every {
            service.deleteChatMessage(any(), eq(partnerId), eq(messageId))
        } throws ResourceNotFoundException("Message in Conversation with partner $partnerId", messageId)

        mvc.delete("$CHAT_MESSAGES/{messageId}", partnerId, messageId)
            .andExpect {
                status { isNotFound() }
            }
    }

}