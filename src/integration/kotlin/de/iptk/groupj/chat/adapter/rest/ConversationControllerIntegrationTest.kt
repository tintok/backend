package de.iptk.groupj.chat.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.chat.application.ChatService
import de.iptk.groupj.chat.domain.mockConversation
import de.iptk.groupj.chat.domain.mockConversationOverview
import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.shared.CHAT_CONVERSATIONS
import io.mockk.every
import io.mockk.justRun
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import java.util.*

@DefaultMockUser
@WebMvcTest(controllers = [ConversationController::class])
internal class ConversationControllerIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var service: ChatService

    @Test
    internal fun `get conversations`() {
        every {
            service.findAllConversations(any(), any())
        } returns PageImpl(mockConversationOverview(), PageRequest.of(0, 20), 2)

        mvc.get(CHAT_CONVERSATIONS) {
            param("page", "0")
            param("size", "20")
        }.andExpect {
            status {
                isOk()
            }
        }
    }

    @Test
    internal fun `empty conversations`() {
        every { service.findAllConversations(any(), any()) } returns Page.empty()

        mvc.get(CHAT_CONVERSATIONS) {
            param("page", "0")
            param("size", "20")
        }.andExpect {
            status {
                isNoContent()
            }
        }
    }

    @Test
    internal fun `get conversation`() {
        val partnerId = UUID.randomUUID()
        every {
            service.findConversation(any(), eq(partnerId), any())
        } returns PageImpl(mockConversation(), PageRequest.of(0, 20), 2)

        mvc.get("$CHAT_CONVERSATIONS/{partnerId}", partnerId) {
            param("page", "0")
            param("size", "20")
        }.andExpect {
            status {
                isOk()
            }
        }
    }

    @Test
    internal fun `get conversation non existing partner`() {
        val partnerId = UUID.randomUUID()
        every {
            service.findConversation(any(), eq(partnerId), any())
        } throws ResourceNotFoundException("User", partnerId)

        mvc.get("$CHAT_CONVERSATIONS/{partnerId}", partnerId).andExpect {
            status { isNotFound() }
        }
    }

    @Test
    internal fun `get empty conversation`() {
        val partnerId = UUID.randomUUID()
        every { service.findConversation(any(), eq(partnerId), any()) } returns Page.empty()

        mvc.get("$CHAT_CONVERSATIONS/{partnerId}", partnerId).andExpect {
            status { isNoContent() }
        }
    }

    @Test
    internal fun `delete conversation`() {
        val partnerId = UUID.randomUUID()
        justRun { service.deleteConversation(any(), eq(partnerId)) }

        mvc.delete("$CHAT_CONVERSATIONS/{partnerId}", partnerId).andExpect {
            status {
                isNoContent()
            }
        }
    }

    @Test
    internal fun `delete conversation non existing partner`() {
        val partnerId = UUID.randomUUID()
        every {
            service.deleteConversation(any(), eq(partnerId))
        } throws ResourceNotFoundException("User", partnerId)

        mvc.delete("$CHAT_CONVERSATIONS/{partnerId}", partnerId).andExpect {
            status {
                isNotFound()
            }
        }
    }

}