package de.iptk.groupj.chat.adapter.database

import de.iptk.groupj.Cypher
import de.iptk.groupj.PersistenceIntegrationTest
import de.iptk.groupj.chat.domain.ChatMessageEntity
import de.iptk.groupj.chat.model.ConversationPartner
import de.iptk.groupj.user.adapter.database.UserRepository
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.date.shouldHaveSameInstantAs
import io.kotest.matchers.optional.shouldBeEmpty
import io.kotest.matchers.optional.shouldBePresent
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import java.time.ZonedDateTime
import java.util.*

@Cypher("ChatPersistenceTest.cypher")
internal class ChatPersistenceTest @Autowired constructor(private val repository: ChatRepository) :
    PersistenceIntegrationTest() {

    @Nested
    inner class Conversations {
        @Test
        internal fun `find all conversations`() {
            val username = "elliotalderson@protonmail.ch"
            val pageable = PageRequest.of(0, 20)
            val conversations = repository.findConversations(username, pageable)

            assertSoftly(conversations) {
                shouldHaveSize(2)
                totalElements shouldBe 2
            }

            assertSoftly(conversations.content[0]) {
                partner shouldBe ConversationPartner(UUID.fromString("00000000-0000-0000-0000-000000000070"), "Bar Foo")
                last shouldHaveSameInstantAs ZonedDateTime.parse("2020-12-15T17:59:43.843836000+01:00[Europe/Berlin]")
                unread shouldBe 1
                message shouldBe "Hello!"
                send.shouldBeFalse()
            }

            assertSoftly(conversations.content[1]) {
                partner shouldBe ConversationPartner(UUID.fromString("00000000-0000-0000-0000-000000000069"), "Foo Bar")
                last shouldHaveSameInstantAs ZonedDateTime.parse("2020-12-14T17:59:44.843836000+01:00[Europe/Berlin]")
                unread shouldBe 0
                message shouldBe "Whats popin?"
                send.shouldBeTrue()
            }
        }

        @Test
        internal fun `find all conversations page 0`() {
            val username = "elliotalderson@protonmail.ch"
            val pageable = PageRequest.of(0, 1)
            val conversations = repository.findConversations(username, pageable)

            assertSoftly(conversations) {
                shouldHaveSize(1)
                number shouldBe 0
                totalElements shouldBe 2
            }
        }

        @Test
        internal fun `find all conversations page 1`() {
            val username = "elliotalderson@protonmail.ch"
            val pageable = PageRequest.of(1, 1)
            val conversations = repository.findConversations(username, pageable)

            assertSoftly(conversations) {
                shouldHaveSize(1)
                number shouldBe 1
                totalElements shouldBe 2
            }
        }

        @Test
        internal fun `read count`() {
            // Bar should have no unread messages
            val barEntry = repository.findConversations("bar@foo.de", PageRequest.of(0, 20)).first()
            barEntry.unread shouldBe 0

            // Foo should have two unread message from Elliot
            val fooEntry = repository.findConversations("foo@bar.de", PageRequest.of(0, 20)).first()
            fooEntry.unread shouldBe 2

            // Elliot should have one unread message from Bar
            val elliotEntry = repository.findConversations("elliotalderson@protonmail.ch", PageRequest.of(0, 20)).first()
            elliotEntry.unread shouldBe 1

            // Andy should not have an overview
            repository.findConversations("andy@tintok.de", PageRequest.of(0, 20)).shouldBeEmpty()

            // Test should not have an overview
            repository.findConversations("test@bar.de", PageRequest.of(0, 20)).shouldBeEmpty()
        }

    }

    @Nested
    inner class Conversation {
        @Test
        internal fun `retrieve conversation`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.fromString("00000000-0000-0000-0000-000000000069")
            val pageable = PageRequest.of(0, 20)

            val conversation = repository.findConversation(username, matchId, pageable)

            conversation shouldHaveSize 2
        }

        @Test
        internal fun `conversation correct paging page 0`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.fromString("00000000-0000-0000-0000-000000000069")
            val pageable = PageRequest.of(0, 1)

            val conversation = repository.findConversation(username, matchId, pageable)

            conversation shouldHaveSize 1
            conversation.totalElements shouldBe 2
            // Correctly formatted
            conversation.content.first().id shouldBe UUID.fromString("28bab65b-d8d8-4e11-9cb9-c4b61b50489b")
        }

        @Test
        internal fun `conversation correct paging page 1`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.fromString("00000000-0000-0000-0000-000000000069")
            val pageable = PageRequest.of(1, 1)

            val conversation = repository.findConversation(username, matchId, pageable)

            conversation shouldHaveSize 1
            conversation.totalElements shouldBe 2
            // Correctly formatted
            conversation.content.first().id shouldBe UUID.fromString("28bab65b-d8d8-4e11-9cb9-c4b61b50489a")
        }

        @Test
        internal fun `empty conversation`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.fromString("5ac4d91a-4686-4cea-89a6-c2408b078b58")
            val pageable = PageRequest.of(0, 20)

            val conversation = repository.findConversation(username, matchId, pageable)

            conversation shouldHaveSize 0
        }

        @Test
        internal fun `delete conversation`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.fromString("00000000-0000-0000-0000-000000000070")

            repository.findConversation(username, matchId, PageRequest.of(0, 20)).shouldNotBeEmpty()
            repository.deleteConversation(username, matchId).shouldBeTrue()
            repository.findConversation(username, matchId, PageRequest.of(0, 20)).shouldBeEmpty()

            // Conversation inverted should still be present
            repository.findConversation(
                "bar@foo.de",
                UUID.fromString("8702e89a-dc90-4df1-9374-fec7830368ce"),
                PageRequest.of(0, 20)
            ).shouldNotBeEmpty()
        }

        @Test
        internal fun `delete non existing conversation`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.randomUUID()

            repository.deleteConversation(username, matchId).shouldBeFalse()
        }

        @Test
        internal fun `delete conversation of non matched user`() {
            val username = "elliotalderson@protonmail.ch"
            val matchId = UUID.fromString("d764e969-7123-432b-a35e-f45b01305a0a")

            repository.deleteConversation(username, matchId).shouldBeFalse()
        }

    }

    @Nested
    inner class Message {
        @Test
        internal fun save(@Autowired userRepository: UserRepository) {
            val andy = userRepository.findByUsername("andy@tintok.de").get()
            val elliot = userRepository.findByUsername("elliotalderson@protonmail.ch").get()
            val message = ChatMessageEntity(
                message = "Howdy Partner",
                subject = elliot,
                partner = andy,
                created = ZonedDateTime.now(),
                read = true,
                send = true
            )

            repository.save(message)
            repository.findConversation(elliot.username, andy.id!!, PageRequest.of(0, 20)) shouldHaveSize 1
        }

        @Test
        internal fun `mark as read`() {
            val messageId = UUID.fromString("29bab65b-d8d8-4e11-9cb9-c4b61b50489a")
            repository.findByIdOrNull(messageId)!!.read.shouldBeFalse()

            repository.markMessageAsRead(
                "elliotalderson@protonmail.ch",
                UUID.fromString("00000000-0000-0000-0000-000000000070"),
                messageId
            ).shouldBeTrue()

            repository.findByIdOrNull(messageId)!!.read.shouldBeTrue()
        }

        @Test
        internal fun `mark non existing as read`() {
            // Non existing User
            repository.markMessageAsRead(
                "elliotalderson@protonmail.ch",
                UUID.fromString("00000000-0000-0000-0000-000000000079"),
                UUID.fromString("29bab65b-d8d8-4e11-9cb9-c4b61b50489a")
            ).shouldBeFalse()

            // Non existing Message
            repository.markMessageAsRead(
                "elliotalderson@protonmail.ch",
                UUID.fromString("00000000-0000-0000-0000-000000000070"),
                UUID.fromString("29bab65b-d8d9-4e11-9cb9-c4b61b50489a")
            ).shouldBeFalse()
        }

        @Test
        internal fun delete() {
            val messageId = UUID.fromString("29bab65b-d8d8-4e11-9cb9-c4b61b50489a")
            repository.findById(messageId).shouldBePresent()

            repository.deleteMessage(
                "elliotalderson@protonmail.ch",
                UUID.fromString("00000000-0000-0000-0000-000000000070"),
                messageId
            ).shouldBeTrue()

            repository.findById(messageId).shouldBeEmpty()
        }

        @Test
        internal fun `non existing delete`() {
            // Non existing User
            repository.deleteMessage(
                "elliotalderson@protonmail.ch",
                UUID.fromString("00000000-0000-0000-0000-000000000079"),
                UUID.fromString("29bab65b-d8d8-4e11-9cb9-c4b61b50489a")
            ).shouldBeFalse()

            // Non existing Message
            repository.deleteMessage(
                "elliotalderson@protonmail.ch",
                UUID.fromString("00000000-0000-0000-0000-000000000070"),
                UUID.fromString("29bab65b-d8d9-4e11-9cb9-c4b61b50489a")
            ).shouldBeFalse()
        }
    }

}