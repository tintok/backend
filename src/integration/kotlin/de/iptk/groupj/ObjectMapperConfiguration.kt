package de.iptk.groupj

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean

/**
 * Object Mapper for Persistence Tests with Kotlin and java time module.
 */
@TestConfiguration(proxyBeanMethods = false)
class ObjectMapperConfiguration {

    @Bean
    fun mapper(): ObjectMapper = getObjectMapper()

}