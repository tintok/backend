package de.iptk.groupj

import org.springframework.test.web.servlet.result.JsonPathResultMatchersDsl
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * JsonPath Matcher which matches LocalDateTime objects.
 */
fun JsonPathResultMatchersDsl.zonedDateTime(value: ZonedDateTime?) =
    value(value?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME) ?: "")

/**
 * JsonPath Matcher which matches enums objects.
 */
fun <T : Enum<T>> JsonPathResultMatchersDsl.enum(enum: T) = value(enum.toString())

/**
 * JsonPath Matcher which matches uuids.
 */
fun JsonPathResultMatchersDsl.uuid(uuid: UUID?) = value(Objects.toString(uuid))
