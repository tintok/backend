package de.iptk.groupj

import ac.simons.neo4j.migrations.springframework.boot.autoconfigure.MigrationsAutoConfiguration
import de.iptk.groupj.config.Neo4jEntityValidationConfig
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode
import org.neo4j.driver.springframework.boot.test.autoconfigure.Neo4jTestHarnessAutoConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.neo4j.DataNeo4jTest
import org.springframework.context.annotation.Import
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import org.springframework.data.neo4j.core.Neo4jTemplate
import org.springframework.data.neo4j.core.PreparedQuery
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestContextAnnotationUtils
import org.springframework.test.context.util.TestContextResourceUtils
import java.io.File
import java.io.FileReader
import java.util.*
import java.util.stream.StreamSupport
import kotlin.streams.toList

/**
 * Generic Persistence test.
 */
@DataNeo4jTest
@ActiveProfiles("test")
/*
Start and configure Neo4J in-memory database, also execute migrations
the order of these imports is important, don't change them if you don't know what you're doing!
*/
@Import(
    Neo4jInMemoryDB::class,
    Neo4jTestHarnessAutoConfiguration::class,
    MigrationsAutoConfiguration::class,
    Neo4jEntityValidationConfig::class,
    ObjectMapperConfiguration::class
)
@Execution(ExecutionMode.SAME_THREAD)
abstract class PersistenceIntegrationTest {

    /**
     * Populate database with test data if a Cypher annotation is present. Each Transaction gets rolled back.
     */
    @BeforeEach
    fun populateDatabase(@Autowired resourceLoader: ResourceLoader, @Autowired template: Neo4jTemplate) {
        val cyphers = TestContextAnnotationUtils.getMergedRepeatableAnnotations(javaClass, Cypher::class.java)

        if (cyphers.size > 0) {
            cyphers.map(Cypher::path)
                .toTypedArray()
                .let { script -> TestContextResourceUtils.convertToClasspathResourcePaths(javaClass, *script) }
                .let { resources -> getStatements(resourceLoader, resources) }
                .forEach { statement ->
                    val query = PreparedQuery.queryFor(Unit::class.java)
                        .withCypherQuery(statement)
                        .withParameters(emptyMap()) // Neo4jBug, throws NullPointer when no empty Map is set
                        .build()

                    template.toExecutableQuery(query).singleResult
                }
        }
    }

    /**
     * Inputs an existing Cypher file and extracts all statements.
     */
    private fun cypherFileToStatements(file: File): List<String> {
        val scanner = Scanner(FileReader(file)).useDelimiter(";")

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(scanner, Spliterator.ORDERED), false)
            .map { statement -> statement.replace(Regex("\\s+"), " ").trim() } // remove formatting
            .filter(String::isNotBlank)
            .filter { statement -> !statement.startsWith("//") }
            .toList()
    }

    /**
     * Return all combined containing Cypher statements from a list of Cypher files.
     * Statements have to end with ';'. Comments have to start with '//' and end with ';'
     */
    private fun getStatements(resourceLoader: ResourceLoader, scripts: Array<String>): List<String> =
        TestContextResourceUtils.convertToResourceList(resourceLoader, *scripts)
            .filter(this::exists)
            .map(Resource::getFile)
            .map(this::cypherFileToStatements)
            .flatten()

    /**
     * Check whether a Resource exists.
     */
    private fun exists(file: Resource) = when {
        !file.exists() -> throw IllegalArgumentException("${file.file.absolutePath} does not exist.")
        else -> true
    }

}
