package de.iptk.groupj

import com.atlassian.oai.validator.OpenApiInteractionValidator
import com.atlassian.oai.validator.mockmvc.MockMvcRequest
import com.atlassian.oai.validator.mockmvc.MockMvcResponse
import com.atlassian.oai.validator.mockmvc.OpenApiMatchers
import com.atlassian.oai.validator.mockmvc.OpenApiMatchers.OpenApiValidationException
import com.atlassian.oai.validator.mockmvc.OpenApiValidationMatchers
import com.atlassian.oai.validator.whitelist.ValidationErrorsWhitelist
import com.atlassian.oai.validator.whitelist.rule.WhitelistRules.messageHasKey
import org.apache.commons.io.IOUtils
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.nio.charset.StandardCharsets
import javax.servlet.Filter


// Spec file URL
private const val API_BUNDLE_PATH = "openapi/api-bundle.yaml"

// Load Spec into ram to increase speed
private val OPEN_API_SPEC: String = IOUtils.toString(ClassPathResource(API_BUNDLE_PATH).inputStream, StandardCharsets.UTF_8)

// Create Matcher instance
private val openApiMatchers: OpenApiMatchers = OpenApiValidationMatchers.openApi()

// Create validator
private val validator = OpenApiInteractionValidator.createForInlineApiSpecification(OPEN_API_SPEC)
    .withWhitelist(
        ValidationErrorsWhitelist.create().withRule(
            "Ignore Authorization Header because Security is mocked in our test",
            messageHasKey("validation.request.security.missing")
        )
    )
    .build()

// Result Matcher for MockMvc
private val validationMatcher = openApiMatchers.isValid(validator)

/**
 * OpenApi validation if the return type of the http call is a 200ish. On all codes different then 200ish, no request
 * validation is performed.
 */
fun validOnOkResponse() = ResultMatcher { result: MvcResult ->
    if (HttpStatus.valueOf(result.response.status).is2xxSuccessful) {
        validationMatcher.match(result)
    } else {
        val request = MockMvcRequest.of(result.request)
        val validationReport = validator.validateResponse(request.path, request.method, MockMvcResponse.of(result.response))
        if (validationReport.hasErrors()) throw OpenApiValidationException(validationReport)
    }
}

/**
 * Set CharacterEncoding to UTF for all JSON request bodies.
 */
private fun requestSetCharterEncodingUTF8() = Filter { request, response, chain ->
    if (request.contentType != MediaType.MULTIPART_FORM_DATA_VALUE) {
        request.characterEncoding = StandardCharsets.UTF_8.name()
    }

    chain.doFilter(request, response)
}

/**
 * Create and prepare MockMvc for WebMVC tests.
 */
fun createMockMvc(context: WebApplicationContext, openApiValidator: ResultMatcher) =
    MockMvcBuilders
        .webAppContextSetup(context)
        .addFilter<DefaultMockMvcBuilder>(requestSetCharterEncodingUTF8(), "/*")
        .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
        .alwaysDo<DefaultMockMvcBuilder>(MockMvcResultHandlers.print())
        .alwaysExpect<DefaultMockMvcBuilder>(openApiValidator)
        .build()