package de.iptk.groupj

import org.neo4j.configuration.GraphDatabaseSettings
import org.neo4j.harness.Neo4j
import org.neo4j.harness.Neo4jBuilders
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.core.io.Resource

/**
 * Build in memory database with configured apoc plugin.
 */
@TestConfiguration(proxyBeanMethods = false)
class Neo4jInMemoryDB {

    @Bean
    fun neo4jBean(@Value("file:neo4j-plugins/") pluginDir: Resource): Neo4j = Neo4jBuilders.newInProcessBuilder()
        .withDisabledServer()
        .withConfig(GraphDatabaseSettings.plugin_dir, pluginDir.file.toPath())
        .withConfig(GraphDatabaseSettings.procedure_allowlist, listOf("de.iptk.groupj.*"))
        .withFunction(PostScore::class.java)
        .build()

}