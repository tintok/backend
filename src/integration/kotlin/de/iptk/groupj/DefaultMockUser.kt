package de.iptk.groupj

import org.springframework.security.test.context.support.WithSecurityContext

/**
 * Annotation for injecting a default MockUser in Springs security Context for tests. It uses the user factory for that.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@WithSecurityContext(factory = SecurityDefaultMockUserFactory::class)
annotation class DefaultMockUser