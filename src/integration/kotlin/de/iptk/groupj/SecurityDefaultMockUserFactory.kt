package de.iptk.groupj

import de.iptk.groupj.user.mockUser
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithSecurityContextFactory

/**
 * Factory for populating Springs Security Context with a UserEntity from User Factory.
 */
class SecurityDefaultMockUserFactory : WithSecurityContextFactory<DefaultMockUser> {

    override fun createSecurityContext(annotation: DefaultMockUser): SecurityContext {
        return SecurityContextHolder.createEmptyContext().apply {
            val principal = mockUser()
            authentication = UsernamePasswordAuthenticationToken(principal, null, principal.authorities)
        }
    }

}