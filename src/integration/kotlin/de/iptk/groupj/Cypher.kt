package de.iptk.groupj

import java.lang.annotation.Inherited

@Suppress("DEPRECATED_JAVA_ANNOTATION")
@java.lang.annotation.Repeatable(Cyphers::class)
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@Repeatable
@Inherited
annotation class Cypher(val path: String)


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Cyphers(vararg val value: Cypher)