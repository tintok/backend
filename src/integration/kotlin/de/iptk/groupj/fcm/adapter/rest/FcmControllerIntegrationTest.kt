package de.iptk.groupj.fcm.adapter.rest

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.fcm.application.FcmTokenService
import de.iptk.groupj.fcm.exception.InvalidFcmTokenException
import de.iptk.groupj.shared.NOTIFICATIONS_FCM
import io.mockk.every
import io.mockk.justRun
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.post

@DefaultMockUser
@WebMvcTest(controllers = [FcmController::class])
internal class FcmControllerIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var service: FcmTokenService

    @Test
    fun `add token`() {
        val token = "some-fcm-token"
        val payload = mapOf("token" to token)

        justRun { service.addToken(any(), eq(token)) }

        mvc.post(NOTIFICATIONS_FCM) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(payload)
        }.andExpect {
            status { isNoContent() }
        }
    }

    @Test
    fun `invalid token`() {
        val token = "some-invalid-fcm-token"
        val payload = mapOf("token" to token)

        every { service.addToken(any(), eq(token)) } throws InvalidFcmTokenException("Invalid Token")

        mvc.post(NOTIFICATIONS_FCM) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(payload)
        }.andExpect {
            status { isBadRequest() }
        }
    }

}