package de.iptk.groupj

import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.domain.AuthProvider
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithSecurityContextFactory
import java.util.*

/**
 * Factory for creating custom authentication in WebMvc tests.
 */
class SecurityMockUserFactory : WithSecurityContextFactory<MockUser> {

    override fun createSecurityContext(mockUser: MockUser): SecurityContext {
        return SecurityContextHolder.createEmptyContext().apply {
            val principal = UserEntity(
                id = UUID(0, 0),
                username = "elliotalderson@protonmail.ch",
                givenName = "test",
                familyName = "test",
                provider = AuthProvider.LOCAL,
                password = PasswordEncoder().encode(mockUser.password)
            )

            authentication = UsernamePasswordAuthenticationToken(principal, null, principal.authorities)
        }
    }

}