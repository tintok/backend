package de.iptk.groupj

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.config.ConfigurationProperties
import de.iptk.groupj.config.LocalizationConfig
import de.iptk.groupj.errorhandling.GlobalExceptionHandler
import de.iptk.groupj.security.application.JwtManager
import de.iptk.groupj.security.application.TinTokUserDetailsService
import org.json.JSONObject
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.web.context.WebApplicationContext


@ActiveProfiles("test")
@ExtendWith(SpringExtension::class)
@Import(
    ConfigurationProperties::class,
    GlobalExceptionHandler::class,
    JwtManager::class,
    LocalizationConfig::class
)
@Execution(ExecutionMode.SAME_THREAD) // Weired Concurrency Spring Bug
abstract class WebIntegrationTest(private val openApiMatcher: ResultMatcher = validOnOkResponse()) {

    @Autowired
    protected lateinit var mapper: ObjectMapper

    @MockkBean
    protected lateinit var userDetailsService: TinTokUserDetailsService

    protected lateinit var mvc: MockMvc

    @BeforeAll
    fun init(context: WebApplicationContext) {
        mvc = createMockMvc(context, openApiMatcher)
    }

    /**
     * Load file from Resource Folder as MockFile for Tests.
     */
    protected fun mockMultipartFile(name: String, file: String, type: String, resource: Resource) =
        MockMultipartFile(name, file, type, resource.inputStream)

    /**
     * Create Json Multipart mock form data.
     */
    protected fun mockMultipartJson(partName: String, payload: Map<String, Any?>) =
        MockPart(partName, JSONObject(payload).toString().encodeToByteArray()).apply {
            headers.contentType = MediaType.APPLICATION_JSON
        }

}