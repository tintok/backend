package de.iptk.groupj.generic

import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.shared.PROFILE_PICTURE
import de.iptk.groupj.shared.USER
import de.iptk.groupj.shared.USER_ME
import de.iptk.groupj.user.adapter.rest.ProfilePictureController
import de.iptk.groupj.user.adapter.rest.UserMeController
import de.iptk.groupj.user.application.ProfilePictureService
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.mockUser
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.http.MediaType.IMAGE_PNG_VALUE
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.patch
import org.springframework.test.web.servlet.put
import java.nio.charset.StandardCharsets
import java.util.*

/**
 * Test for checking the most common Exceptions. OpenApi validation for this test is disabled.
 */
@WebMvcTest(controllers = [UserMeController::class, ProfilePictureController::class])
internal class CommonUseCaseControllerIntegrationTest : WebIntegrationTest(ResultMatcher { }) {

    @MockkBean
    private lateinit var userService: UserService

    @MockkBean
    private lateinit var profilePictureService: ProfilePictureService

    @Test
    internal fun `test accesses denied for unauthorized user`() {
        mvc.get(USER_ME)
            .andExpect {
                status { isUnauthorized() }
            }
    }

    @Test
    @WithMockUser
    internal fun `test not found`() {
        mvc.get("/cs31")
            .andExpect {
                status { isNotFound() }
            }
    }

    @Test
    @WithMockUser
    internal fun `method not allowed`() {
        mvc.patch(USER_ME)
            .andExpect {
                status { isMethodNotAllowed() }
            }
    }

    @Test
    @WithMockUser
    internal fun `json invalid syntax`() {
        mvc.put(USER_ME) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString("{")
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    @WithMockUser
    internal fun `missing required json field`() {
        val payload = mapOf(
            "family_name" to "Alderson",
            "location" to "changed Location",
            "biography" to "Hey!"
        )

        mvc.put(USER_ME) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(payload) + ","
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    @WithMockUser
    internal fun `additional not part of the model json field`() {
        val payload = mapOf(
            "given_name" to "Elliot",
            "family_name" to "Alderson",
            "location" to "changed Location",
            "biography" to "Hey!",
            "additional" to "some value"
        )

        mvc.put(USER_ME) {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(payload)
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    @DefaultMockUser
    internal fun `char encoding should be utf8`() {
        mvc.get(USER_ME)
            .andExpect {
                content {
                    encoding(StandardCharsets.UTF_8.name())
                }
            }
    }

    @Test
    @DefaultMockUser
    internal fun `test media file not encoding utf8`(@Value("classpath:sample_image.png") image: Resource) {
        every { profilePictureService.find(any()) } returns Pair(IMAGE_PNG_VALUE, image)

        mvc.get("$USER/${UUID.randomUUID()}$PROFILE_PICTURE")
            .andExpect {
                status {
                    isOk()
                }
                content {
                    contentType(mockUser().pictureMimeType!!)
                }
            }
    }

}