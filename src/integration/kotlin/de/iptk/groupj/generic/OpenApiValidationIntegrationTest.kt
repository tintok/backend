package de.iptk.groupj.generic

import com.atlassian.oai.validator.mockmvc.OpenApiMatchers
import com.ninjasquad.springmockk.MockkBean
import de.iptk.groupj.DefaultMockUser
import de.iptk.groupj.WebIntegrationTest
import de.iptk.groupj.shared.USER_ME
import de.iptk.groupj.user.adapter.rest.ProfilePictureController
import de.iptk.groupj.user.application.ProfilePictureService
import io.kotest.assertions.throwables.shouldThrow
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.get

/**
 * Check that OpenApiValidation is enabled and works.
 */
@WebMvcTest(controllers = [ProfilePictureController::class])
class OpenApiValidationIntegrationTest : WebIntegrationTest() {

    @MockkBean
    private lateinit var profilePictureService: ProfilePictureService

    @Test
    @DefaultMockUser
    internal fun `check that open api validation works`() {
        shouldThrow<OpenApiMatchers.OpenApiValidationException> {
            // Invalid API request on purpose
            mvc.get(USER_ME) {
                content = mapper.writeValueAsString(emptySet<Any>())
            }
        }
    }

}