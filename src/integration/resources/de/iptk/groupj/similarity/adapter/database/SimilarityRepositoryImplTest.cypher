CREATE (:User {username: 'me@test.de'});

CREATE (:User {username: 'other1@test.de'});

CREATE (:User {username: 'other2@test.de'});

CREATE (:User {username: 'other3@test.de'});

MATCH (u:User {username: 'me@test.de'})
CREATE (:Post {
  id: '00000000-0000-0000-0001-000000000001', text: 'Post 1 from me'
})<-[:PUBLISHED]-(u);

MATCH (p:Post {id: '00000000-0000-0000-0001-000000000001'})
MATCH (series:Tag {name: 'series'})
CREATE (p)-[:TAGGED_WITH]->(series);

MATCH (u:User {username: 'me@test.de'})
CREATE (:Post {
  id: '00000000-0000-0000-0001-000000000002', text: 'Post 2 from me'
})<-[:PUBLISHED]-(u);

MATCH (p:Post {id: '00000000-0000-0000-0001-000000000002'})
MATCH (politics:Tag {name: 'politics'})
CREATE (p)-[:TAGGED_WITH]->(politics);

MATCH (u:User {username: 'other1@test.de'})
CREATE (:Post {
  id: '00000000-0000-0000-0001-000000000003', text: 'Post 1 from other1'
})<-[:PUBLISHED]-(u);

MATCH (p:Post {id: '00000000-0000-0000-0001-000000000003'})
MATCH (series:Tag {name: 'series'}), (computer:Tag {name: 'computer'})
CREATE (p)-[:TAGGED_WITH]->(series), (p)-[:TAGGED_WITH]->(computer);

MATCH (u:User {username: 'other1@test.de'})
CREATE (:Post {
  id: '00000000-0000-0000-0001-000000000004', text: 'Post 2 from other1'
})<-[:PUBLISHED]-(u);

MATCH (p:Post {id: '00000000-0000-0000-0001-000000000004'})
MATCH (politics:Tag {name: 'politics'})
CREATE (p)-[:TAGGED_WITH]->(politics);

MATCH (u:User {username: 'other2@test.de'})
CREATE (:Post {
  id: '00000000-0000-0000-0001-000000000005', text: 'Post 1 from other2'
})<-[:PUBLISHED]-(u);

// I like post 1 from other1;
MATCH (u:User {username: 'me@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000003'})
CREATE (u)-[:LIKED]->(p);

// I dislike post 2 from other1;
MATCH (u:User {username: 'me@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000004'})
CREATE (u)-[:DISLIKED]->(p);

// other1 likes post 1 from me;
MATCH (u:User {username: 'other1@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000001'})
CREATE (u)-[:LIKED]->(p);

// other2 likes post 1 from me;
MATCH (u:User {username: 'other2@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000001'})
CREATE (u)-[:LIKED]->(p);

// other2 likes post 2 from other1;
MATCH (u:User {username: 'other2@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000004'})
CREATE (u)-[:LIKED]->(p);

// other3 likes post 1 from me;
MATCH (u:User {username: 'other3@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000001'})
CREATE (u)-[:LIKED]->(p);

// other3 dislikes post 2 from me;
MATCH (u:User {username: 'other3@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000002'})
CREATE (u)-[:DISLIKED]->(p);

// other3 dislikes post 1 from other1;
MATCH (u:User {username: 'other3@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000003'})
CREATE (u)-[:DISLIKED]->(p);

// other3 dislikes post 2 from other1;
MATCH (u:User {username: 'other3@test.de'})
MATCH (p:Post {id: '00000000-0000-0000-0001-000000000004'})
CREATE (u)-[:DISLIKED]->(p);

// one user similarity exists;
MATCH (from:User {username: 'me@test.de'})
MATCH (to:User {username: 'other1@test.de'})
CREATE (from)-[:SIMILAR {score: 0.0}]->(to);

// one tag similarity exists;
MATCH (from:User {username: 'me@test.de'})
MATCH (to:Tag {name: 'computer'})
CREATE(from)-[:SIMILAR {score: 0.0}]->(to);
