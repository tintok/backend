CREATE  (:User {
  id:                '00000000-0000-0000-0000-000000000069', username: 'elliotalderson1@protonmail.ch',
  given_name:        'Elliot', location: 'Darmstadt', biography: 'Hey!', picture_length: 1337,
  picture_mime_type: 'image/jpeg', family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

MATCH (tag:Tag {name: 'computer'}), (u:User {username: 'elliotalderson1@protonmail.ch'})
CREATE (u) - [:SIMILAR {score: 2.0}] -> (tag);

MATCH (tag:Tag {name: 'politics'}), (u:User {username: 'elliotalderson1@protonmail.ch'})
CREATE (u) - [:SIMILAR {score: 1.0}] -> (tag);
