// Main User under test;
CREATE  (n:User {
  id:          '8702e89a-dc90-4df1-9374-fec7830368ce', username: 'elliotalderson@protonmail.ch',
  given_name:  'Elliot', location: 'Darmstadt', biography: 'Hey!',
  picture_id:  '4d4f3e4d-a658-48d8-a34b-0438d4d47698', picture_length: 1337, picture_mime_type: 'image/jpeg',
  family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', fcm_token: 'some-group-token'
});

// Matched user;
CREATE  (n:User {
  id:       '00000000-0000-0000-0000-000000000069', username: 'foo@bar.de', given_name: 'Foo', family_name: 'Bar',
  provider: 'LOCAL'
});

// Not matched user;
CREATE  (n:User {
  id:       '00000000-0000-0000-0000-000000000420', username: 'test@bar.de', given_name: 'Test', family_name: 'Bar',
  provider: 'LOCAL'
});

MATCH (first:User {username: 'elliotalderson@protonmail.ch'}),
      (second:User {username: 'foo@bar.de'})
CREATE (first)-[:MATCHED]->(second);

MATCH (first:User {username: 'elliotalderson@protonmail.ch'}),
      (second:User {username: 'foo@bar.de'})
CREATE (c1:ChatMessage {id: '28bab65b-d8d8-4e11-9cb9-c4b61b50489a'}),
       (c2:ChatMessage {id: '8079299b-010e-4bde-b93e-c2f9cc24ca12'}),
       (first)<-[:SUBJECT]-(c1)-[:PARTNER]->(second),
       (second)<-[:SUBJECT]-(c2)-[:PARTNER]->(first);

// Posts;
CREATE (:Post:ImagePost {
  id:       '47877713-2105-4299-9278-134950e8494c',
  created:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
  location: point({srid: 4326, x: 17, y: 15}),
  text:     'TigerKing ist einfach beste',
  type:     'IMAGE',
  updated:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'})
})<-[:CONTENT]-(:Content:ImageContent {
  id:   '3a00aec0-c579-48fe-b124-2daf71817524',
  size: 69,
  type: 'image/jpeg'
});

CREATE (:Post:VideoPost {
  id:       '8a2c2d8f-92a0-47af-a39c-e8c82b353b44',
  created:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
  location: point({srid: 4326, x: 49.87457339, y: 8.67393361}),
  text:     'TigerKing!',
  type:     'VIDEO',
  updated:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'})
})<-[:CONTENT]-(:Content:VideoContent {
  id:   '00000000-0000-0000-0000-000000000005',
  size: 1337,
  type: 'video/mp4'
});

// Assign Posts to User;
MATCH (p:Post), (u:User)
  WHERE u.username = 'elliotalderson@protonmail.ch'
CREATE (u)-[:PUBLISHED]->(p);

// Assign Posts Tags;
MATCH (p:Post), (t:Tag)
  WHERE t.name = 'sport'
CREATE (p)-[:TAGGED_WITH]->(t);

// Second User with likes and dislikes;
CREATE  (n:User {
  id:          'f39105eb-a43e-453f-a7de-97d5700bfa49', username: 'elliotalderson3@protonmail.ch',
  given_name:  'Elliot', location: 'Darmstadt', biography: 'Hey!',
  picture_id:  '28982f88-400b-499d-92ef-5313035846e8', picture_length: 1337, picture_mime_type: 'image/jpeg',
  family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

MATCH (p:Post {id: '47877713-2105-4299-9278-134950e8494c'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)<-[:LIKED]-(u);

MATCH (p:Post {id: '47877713-2105-4299-9278-134950e8494c'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)-[:RECOMMENDED]->(u);

MATCH (p:Post {id: '8a2c2d8f-92a0-47af-a39c-e8c82b353b44'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)<-[:DISLIKED]-(u);