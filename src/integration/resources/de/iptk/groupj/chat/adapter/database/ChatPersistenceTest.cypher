// Users;
// Elliot;
CREATE  (n:User {
  id:          '8702e89a-dc90-4df1-9374-fec7830368ce', username: 'elliotalderson@protonmail.ch',
  given_name:  'Elliot', location: 'Darmstadt', biography: 'Hey!',
  picture_id:  '4d4f3e4d-a658-48d8-a34b-0438d4d47698', picture_length: 1337, picture_mime_type: 'image/jpeg',
  family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', fcm_token: 'some-group-token'
});

// Foo;
CREATE  (n:User {
  id:       '00000000-0000-0000-0000-000000000069', username: 'foo@bar.de', given_name: 'Foo', family_name: 'Bar',
  provider: 'LOCAL'
});

// Bar;
CREATE  (n:User {
  id:       '00000000-0000-0000-0000-000000000070', username: 'bar@foo.de', given_name: 'Bar', family_name: 'Foo',
  provider: 'LOCAL'
});

// Andy;
CREATE  (n:User {
  id:          '5ac4d91a-4686-4cea-89a6-c2408b078b58', username: 'andy@tintok.de', given_name: 'Andy',
  family_name: 'Berna', provider: 'LOCAL'
});

// Not a Matched User;
CREATE  (n:User {
  id:       'd764e969-7123-432b-a35e-f45b01305a0a', username: 'test@bar.de', given_name: 'Test', family_name: 'Bar',
  provider: 'LOCAL'
});

// Match between elliot, foo, bar and andy;
MATCH (elliot:User {username: 'elliotalderson@protonmail.ch'}),
      (foo:User {username: 'foo@bar.de'}),
      (bar:User {username: 'bar@foo.de'}),
      (andy:User {username: 'andy@tintok.de'})
CREATE (elliot)-[:MATCHED]->(foo),
       (elliot)-[:MATCHED]->(bar),
       (elliot)-[:MATCHED]->(andy);

// Elliot sends two messages to foo;
MATCH (elliot:User {username: 'elliotalderson@protonmail.ch'}), (foo:User {username: 'foo@bar.de'})
CREATE
  (c1:ChatMessage {
    id:      '28bab65b-d8d8-4e11-9cb9-c4b61b50489a',
    message: 'Hello!',
    created: datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
    send:    true,
    read:    true
  }),
  (c2:ChatMessage {
    id:      '8079299b-010e-4bde-b93e-c2f9cc24ca12',
    message: 'Hello!',
    created: datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
    send:    false,
    read:    false
  }),
  (c3:ChatMessage {
    id:      '28bab65b-d8d8-4e11-9cb9-c4b61b50489b',
    message: 'Whats popin?',
    created: datetime({datetime: localdatetime('2020-12-14T17:59:44.843836000'), timezone: 'Europe/Berlin'}),
    send:    true,
    read:    true
  }),
  (c4:ChatMessage {
    id:      '8079299b-010e-4bde-b93e-c2f9cc24ca13',
    message: 'Whats popin?',
    created: datetime({datetime: localdatetime('2020-12-14T17:59:44.843836000'), timezone: 'Europe/Berlin'}),
    send:    false,
    read:    false
  }),
  (elliot)<-[:SUBJECT]-(c1)-[:PARTNER]->(foo),
  (foo)<-[:SUBJECT]-(c2)-[:PARTNER]->(elliot),
  (elliot)<-[:SUBJECT]-(c3)-[:PARTNER]->(foo),
  (foo)<-[:SUBJECT]-(c4)-[:PARTNER]->(elliot);

// Elliot receives one message from bar;
MATCH (elliot:User {username: 'elliotalderson@protonmail.ch'}), (bar:User {username: 'bar@foo.de'})
CREATE
  (c1:ChatMessage {
    id:      '29bab65b-d8d8-4e11-9cb9-c4b61b50489a',
    message: 'Hello!',
    created: datetime({datetime: localdatetime('2020-12-15T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
    send:    false,
    read:    false
  }),
  (c2:ChatMessage {
    id:      '8179299b-010e-4bde-b93e-c2f9cc24ca12',
    message: 'Hello!',
    created: datetime({datetime: localdatetime('2020-12-15T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
    send:    true,
    read:    true
  }),
  (elliot)<-[:SUBJECT]-(c1)-[:PARTNER]->(bar),
  (bar)<-[:SUBJECT]-(c2)-[:PARTNER]->(elliot);
