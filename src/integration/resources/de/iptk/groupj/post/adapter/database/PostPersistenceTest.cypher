CREATE  (:User {
  id:                '00000000-0000-0000-0000-000000000069', username: 'elliotalderson1@protonmail.ch',
  given_name:        'Elliot', location: 'Darmstadt', biography: 'Hey!', picture_length: 1337,
  picture_mime_type: 'image/jpeg', family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

CREATE  (:User {
  id:                '00000000-0000-0000-0000-000000000420', username: 'ellenalderson2@protonmail.ch',
  given_name:        'Ellen', location: 'Darmstadt', biography: 'Hey!', picture_length: 1337,
  picture_mime_type: 'image/jpeg', family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

CREATE  (:User {
  id:                'f39105eb-a43e-453f-a7de-97d5700bfa49', username: 'elliotalderson3@protonmail.ch',
  given_name:        'Elliot', location: 'Darmstadt', biography: 'Hey!', picture_length: 1337,
  picture_mime_type: 'image/jpeg', family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

CREATE (:User {
  id:                '00000000-0000-0000-0000-000000001337', username: 'elliotalderson4@protonmail.ch',
  given_name:        'Elliot', location: 'Darmstadt', biography: 'Hey!', picture_length: 1337,
  picture_mime_type: 'image/jpeg', family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

CREATE (:Post:TextPost {
  id:       '00000000-0000-0000-0000-000000000001',
  location: point({srid: 4326, x: 49.87763091, y: 8.65493541}),
  text:     'TigerKing?',
  type:     'TEXT',
  created:  datetime({datetime: localdatetime('2020-12-14T17:57:00'), timezone: 'Europe/Berlin'}),
  updated:  datetime({datetime: localdatetime('2020-12-14T17:57:00'), timezone: 'Europe/Berlin'})
});

CREATE (:Post:ImagePost {
  id:       '00000000-0000-0000-0000-000000000002',
  created:  datetime({datetime: localdatetime('2020-12-14T17:58:00'), timezone: 'Europe/Berlin'}),
  location: point({srid: 4326, x: 49.86112964, y: 8.68051002}),
  text:     'TigerKing ist einfach beste',
  type:     'IMAGE',
  updated:  datetime({datetime: localdatetime('2020-12-14T17:58:00'), timezone: 'Europe/Berlin'})
})<-[:CONTENT]-(:Content:ImageContent {
  id:   '00000000-0000-0000-0000-000000000004',
  size: 69,
  type: 'image/jpeg'
});

CREATE (:Post:VideoPost {
  id:       '00000000-0000-0000-0000-000000000003',
  created:  datetime({datetime: localdatetime('2020-12-14T17:59:00'), timezone: 'Europe/Berlin'}),
  location: point({srid: 4326, x: 49.87457339, y: 8.67393361}),
  type:     'VIDEO',
  updated:  datetime({datetime: localdatetime('2020-12-14T17:59:00'), timezone: 'Europe/Berlin'})
})<-[:CONTENT]-(:Content:VideoContent {
  id:   '00000000-0000-0000-0000-000000000005',
  size: 1337,
  type: 'video/mp4'
});

MATCH (p:Post), (u:User {username: 'elliotalderson1@protonmail.ch'})
  WHERE p.id IN ['00000000-0000-0000-0000-000000000001', '00000000-0000-0000-0000-000000000002',
    '00000000-0000-0000-0000-000000000003']
CREATE (u)-[:PUBLISHED]->(p);

MATCH (p:Post), (t:Tag {name: 'sport'})
  WHERE p.id IN ['00000000-0000-0000-0000-000000000001', '00000000-0000-0000-0000-000000000002',
    '00000000-0000-0000-0000-000000000003']
CREATE (p)-[:TAGGED_WITH]->(t);

MATCH (p:Post {id: '00000000-0000-0000-0000-000000000002'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)<-[:LIKED]-(u);

MATCH (p:Post {id: '00000000-0000-0000-0000-000000000003'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)<-[:DISLIKED]-(u);

MATCH (p:Post {id: '00000000-0000-0000-0000-000000000002'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)-[recommended:RECOMMENDED]->(u)
SET recommended.timestamp = datetime();

MATCH (p:Post {id: '00000000-0000-0000-0000-000000000003'}), (u:User {username: 'elliotalderson3@protonmail.ch'})
CREATE (p)-[recommended:RECOMMENDED]->(u)
SET recommended.timestamp = datetime() - duration({hours: 48});

MATCH (first:User {username: 'elliotalderson1@protonmail.ch'}),
      (second:User {username: 'elliotalderson4@protonmail.ch'})
CREATE (first)-[:MATCHED]->(second);
