CREATE  (:User {
  id:                '00000000-0000-0000-0000-000000001337', username: 'elliotalderson2@protonmail.ch',
  given_name:        'Elliot', location: 'Darmstadt', biography: 'Hey!', picture_length: 1337,
  picture_mime_type: 'image/jpeg', family_name: 'Alderson', password: 'secrete', provider: 'LOCAL', version: 0
});

CREATE (:Post:ImagePost {
  id:       '00000000-0000-0000-0000-000000000007',
  created:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
  location: point({srid: 4326, x: 17, y: 15}),
  text:     'TigerKing ist einfach beste',
  type:     'IMAGE',
  updated:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'})
})<-[:CONTENT]-(:Content:ImageContent {
  id:   '00000000-0000-0000-0000-000000000009',
  size: 69,
  type: 'image/jpeg'
});

CREATE (:Post:VideoPost {
  id:       '00000000-0000-0000-0000-000000000008',
  created:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'}),
  location: point({srid: 4326, x: 17, y: 15}),
  text:     'TigerKing!',
  type:     'VIDEO',
  updated:  datetime({datetime: localdatetime('2020-12-14T17:59:43.843836000'), timezone: 'Europe/Berlin'})
})<-[:CONTENT]-(:Content:VideoContent {
  id:   '00000000-0000-0000-0000-000000000010',
  size: 1337,
  type: 'video/mp4'
});

MATCH (p:Post), (u:User {username: 'elliotalderson2@protonmail.ch'})
  WHERE p.id IN ['00000000-0000-0000-0000-000000000007', '00000000-0000-0000-0000-000000000008']
CREATE (u)-[:PUBLISHED]->(p);

MATCH (p:Post), (t:Tag {name: 'sport'})
  WHERE p.id IN ['00000000-0000-0000-0000-000000000007', '00000000-0000-0000-0000-000000000008']
CREATE (p)-[:TAGGED_WITH]->(t);