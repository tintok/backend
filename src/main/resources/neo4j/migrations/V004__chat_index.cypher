CREATE INDEX chat_created IF NOT EXISTS FOR (c:ChatMessage) ON (c.created);
CREATE INDEX chat_read IF NOT EXISTS FOR (c:ChatMessage) ON (c.read);