CREATE CONSTRAINT post_id_unique IF NOT exists ON (p:Post) ASSERT p.id IS UNIQUE;
CREATE CONSTRAINT user_id_unique IF NOT exists ON (u:User) ASSERT u.id IS UNIQUE;
CREATE CONSTRAINT content_id_unique IF NOT exists ON (c:Content) ASSERT c.id IS UNIQUE;
CREATE CONSTRAINT chat_id_unique IF NOT exists ON (c:ChatMessage) ASSERT c.id IS UNIQUE;