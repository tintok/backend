package de.iptk.groupj.security.filter

import de.iptk.groupj.security.model.JwtAuthenticationToken
import org.apache.commons.lang3.StringUtils
import org.springframework.http.HttpHeaders
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

private const val TOKEN_PREFIX = "Bearer "

/**
 * Filter which injects JWT into Springs Security Context.
 */
internal class JwtAuthenticationProviderFilter(vararg skippedPaths: String) :
    AbstractAuthenticationProviderFilter(*skippedPaths) {

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse)
            : Optional<Authentication> =
        getJwtFromRequest(request).map { token ->
            JwtAuthenticationToken(token, WebAuthenticationDetailsSource().buildDetails(request))
        }

    /**
     * Extract JWT from Request header.
     */
    private fun getJwtFromRequest(request: HttpServletRequest): Optional<String> {
        val token = request.getHeader(HttpHeaders.AUTHORIZATION)

        return when {
            StringUtils.startsWith(token, TOKEN_PREFIX) -> Optional.of(token.substring(TOKEN_PREFIX.length))
            else -> Optional.empty()
        }
    }

}