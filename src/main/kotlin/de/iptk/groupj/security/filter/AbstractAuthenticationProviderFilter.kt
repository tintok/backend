package de.iptk.groupj.security.filter

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.OrRequestMatcher
import org.springframework.security.web.util.matcher.RequestMatcher
import org.springframework.web.filter.OncePerRequestFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Abstract Auth Filter which is only executed on paths not matching nonSecuredAntPaths.
 * If an Auth is generated by the sub filter this is going to be set in Springs Security Context.
 * So long story short, if the implementation provides an Authentication set it in Springs Security Context, if not
 * just continue execution flow.
 */
abstract class AbstractAuthenticationProviderFilter(vararg skippedPaths: String) : OncePerRequestFilter() {

    private val requestMatcher: RequestMatcher = OrRequestMatcher(listOf(*skippedPaths)
        .map { pattern -> AntPathRequestMatcher(pattern, null) })

    /**
     * Generates authentication out of a Request.
     */
    abstract fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse)
            : Optional<Authentication>

    final override fun shouldNotFilter(request: HttpServletRequest) = requestMatcher.matches(request)

    /**
     * Set authentication in Springs Security Context to be validated.
     */
    private fun processAuthentication(authentication: Authentication) =
        SecurityContextHolder.createEmptyContext()
            .apply { this.authentication = authentication }
            .run(SecurityContextHolder::setContext)

    final override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        attemptAuthentication(request, response).ifPresent { auth ->
            processAuthentication(auth)
        }

        chain.doFilter(request, response)
    }

}