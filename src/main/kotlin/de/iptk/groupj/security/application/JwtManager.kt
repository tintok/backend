package de.iptk.groupj.security.application

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.Claim
import com.auth0.jwt.interfaces.DecodedJWT
import de.iptk.groupj.security.config.JwtConfig
import de.iptk.groupj.security.exception.InvalidTokenException
import de.iptk.groupj.shared.VALIDITY_NUMBER_OF_WEEKS
import org.apache.commons.lang3.time.DateUtils
import org.springframework.stereotype.Component
import java.time.ZonedDateTime
import java.util.*


private const val ISSUER = "TinTok"
private const val REFRESH_EXPIRED = VALIDITY_NUMBER_OF_WEEKS * 7L * 24L * 60L * 60L // In seconds

/**
 * Tries to return a UUID from a String claim.
 */
private fun Claim.asUUID(): UUID {
    val data = asString()
    return UUID.fromString(data)
}

/**
 * This class creates and validates JWT token.
 */
@Component
class JwtManager(private val jwtConfig: JwtConfig) {

    private val signAlgo: Algorithm = Algorithm.HMAC256(jwtConfig.secret)

    private val verifier: JWTVerifier = JWT.require(signAlgo)
        .withIssuer(ISSUER)
        .build()

    private val refreshValidator = JWT.require(signAlgo)
        .withIssuer(ISSUER)
        .acceptExpiresAt(REFRESH_EXPIRED)
        .build()

    /**
     * Create Token for a User.
     */
    fun createToken(username: String, userId: UUID): String {
        val now = Date.from(ZonedDateTime.now().toInstant())
        val expiresAt = DateUtils.addSeconds(now, jwtConfig.validity.toSeconds().toInt())

        return JWT.create()
            .withIssuer(ISSUER)
            .withSubject(username)
            .withIssuedAt(now)
            .withExpiresAt(expiresAt)
            .withNotBefore(now)
            .withClaim("id", userId.toString())
            .sign(signAlgo)
    }

    /**
     * Extracts Email (username) from a token.
     */
    fun extractUsername(token: String): String {
        return try {
            val jwt: DecodedJWT = verifier.verify(token)
            jwt.subject
        } catch (e: JWTVerificationException) {
            throw InvalidTokenException(e.message!!)
        }
    }

    /**
     * Create a new token out of a token even if the token is expired. The token should not be expired for
     * more then two weeks.
     */
    fun refresh(token: String): String {
        return try {
            val jwt: DecodedJWT = refreshValidator.verify(token)
            val username = jwt.subject
            val userId = jwt.claims["id"]!!.asUUID()
            createToken(username, userId)
        } catch (e: JWTVerificationException) {
            throw InvalidTokenException(e.message!!)
        }
    }

}
