package de.iptk.groupj.security.application

import com.fasterxml.jackson.databind.ObjectMapper
import de.iptk.groupj.errorhandling.ErrorResponse
import de.iptk.groupj.shared.logger
import org.apache.commons.io.IOUtils
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.web.firewall.RequestRejectedException
import org.springframework.security.web.firewall.RequestRejectedHandler
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST

/**
 * Component to handle potential malicious client requests.
 */
@Component
internal class TinTokRequestRejectedHandler(private val mapper: ObjectMapper, private val messageSource: MessageSource) :
    RequestRejectedHandler {

    private val log = logger()

    override fun handle(request: HttpServletRequest, response: HttpServletResponse, e: RequestRejectedException) {
        log.info("One tried to access path {} but was rejected", request.servletPath)

        response.apply {
            status = SC_BAD_REQUEST
            contentType = APPLICATION_JSON_VALUE
            characterEncoding = StandardCharsets.UTF_8.name()
        }

        val locale = LocaleContextHolder.getLocale()
        val message = messageSource.getMessage("spring.rejected-request", emptyArray(), e.localizedMessage, locale)
        val restErrorMessage = ErrorResponse(HttpStatus.BAD_REQUEST, message, request.servletPath)
        IOUtils.write(mapper.writeValueAsString(restErrorMessage), response.outputStream, StandardCharsets.UTF_8.name())
    }

}