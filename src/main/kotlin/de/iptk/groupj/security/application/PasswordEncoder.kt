package de.iptk.groupj.security.application

import org.mapstruct.Named
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.DelegatingPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

/**
 * Class for encoding and hashing passwords.
 */
class PasswordEncoder : PasswordEncoder {

    private val encoder: PasswordEncoder

    init {
        val encodingId = "bcrypt"
        val encoders: Map<String, PasswordEncoder> = java.util.Map.of(encodingId, BCryptPasswordEncoder(12))
        encoder = DelegatingPasswordEncoder(encodingId, encoders)
    }


    @Named("encode")
    override fun encode(raw: CharSequence): String = encoder.encode(raw)

    override fun matches(raw: CharSequence, encoded: String) = encoder.matches(raw, encoded)

}