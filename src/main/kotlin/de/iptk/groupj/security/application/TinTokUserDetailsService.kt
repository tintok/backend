package de.iptk.groupj.security.application

import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.security.config.BeanIds
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


/**
 * Class for proving [UserDetails] to Spring Security.
 */
@Service(BeanIds.USER_DETAILS_SERVICE)
class TinTokUserDetailsService(private val repository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserEntity = repository.findByUsername(username).orElseThrow {
        UsernameNotFoundException("Encountered non existing User")
    }

}