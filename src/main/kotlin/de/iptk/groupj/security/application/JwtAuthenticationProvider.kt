package de.iptk.groupj.security.application

import de.iptk.groupj.security.model.JwtAuthenticationToken
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService

/**
 * This Provider validates and provides authorization for JWT tokens.
 */
internal class JwtAuthenticationProvider(

    private val service: UserDetailsService,

    private val jwtManager: JwtManager

) : AuthenticationProvider {

    override fun authenticate(authentication: Authentication): Authentication {
        // Throws AuthenticationException if token is invalid
        val username: String = jwtManager.extractUsername(authentication.credentials.toString())
        val principal: UserDetails = service.loadUserByUsername(username)

        return JwtAuthenticationToken(authentication.credentials.toString(), authentication.details, principal)
    }

    override fun supports(authentication: Class<*>?) = (authentication == JwtAuthenticationToken::class.java)

}