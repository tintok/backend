package de.iptk.groupj.security.application

import de.iptk.groupj.shared.logger
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * Entry Point for failed authentication.
 */
internal class TinTokAuthenticationEntryPoint : AuthenticationEntryPoint {

    private val log = logger()

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, e: AuthenticationException) {
        log.info("Responding with unauthorized error. Message: {}", e.message)

        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.localizedMessage)
    }

}