package de.iptk.groupj.security.application

import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.access.AccessDeniedHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * AccessDenied handler for the platform if a user is logged in but does not have the proper permissions to access a
 * certain resource.
 */
internal class TinTokAccessDeniedHandler : AccessDeniedHandler {

    private val log = logger()

    override fun handle(request: HttpServletRequest, response: HttpServletResponse, e: AccessDeniedException) {
        val user = SecurityContextHolder.getContext().authentication.principal as UserEntity
        log.info("User(id={}) tried to access path {} but was denied", user.id, request.servletPath)

        response.sendError(HttpServletResponse.SC_FORBIDDEN, e.localizedMessage)
    }

}