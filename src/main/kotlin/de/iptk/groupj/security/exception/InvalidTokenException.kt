package de.iptk.groupj.security.exception

import org.springframework.security.core.AuthenticationException

/**
 * If the supplied JWT is invalid.
 */
class InvalidTokenException(message: String) : AuthenticationException(message)