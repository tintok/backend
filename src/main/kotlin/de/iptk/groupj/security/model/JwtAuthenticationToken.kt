package de.iptk.groupj.security.model

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * Authentication created by a JwtToken.
 */
internal class JwtAuthenticationToken(private val token: String, private val details: Any) : Authentication {

    private var authorities: Collection<GrantedAuthority> = emptySet()
    private var principal: Any = token
    private var authenticated: Boolean = false

    constructor(token: String, details: Any, principal: UserDetails) : this(token, details) {
        this.authorities = principal.authorities.toMutableSet().toSet()
        this.principal = principal
        this.authenticated = true
    }

    override fun getName() = "Jwt Authentication"

    override fun getAuthorities() = authorities

    override fun getCredentials() = token

    override fun getDetails() = details

    override fun getPrincipal() = principal

    override fun isAuthenticated() = authenticated

    override fun setAuthenticated(isAuthenticated: Boolean) {
        if (isAuthenticated) {
            throw IllegalArgumentException("You can not set authenticated to true")
        }

        authenticated = false
    }

}