package de.iptk.groupj.security.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.time.Duration

/**
 * JWT related configuration.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "security.jwt")
data class JwtConfig(val secret: String, val validity: Duration)

/**
 * OAuth2 configuration container which holds all different registered providers
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "security.oauth2")
data class OAuth2Config(val provider: Map<String, Provider> = mapOf())

/**
 * OAuth2 provider configuration.
 */
@ConstructorBinding
data class Provider(val id: String, val secret: String)