package de.iptk.groupj.security.config

import de.iptk.groupj.security.application.*
import de.iptk.groupj.security.filter.JwtAuthenticationProviderFilter
import de.iptk.groupj.shared.AUTH
import de.iptk.groupj.shared.INFO
import de.iptk.groupj.shared.STATUS
import org.springframework.context.annotation.Bean
import org.springframework.http.CacheControl
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


private val NON_SECURED_PATHS: Array<String> = arrayOf("$AUTH/**", INFO, STATUS)

/**
 * Global Web Security configuration.
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
internal class WebConfiguration(private val service: UserDetailsService, private val manager: JwtManager) :
    WebSecurityConfigurerAdapter() {

    override fun configure(authenticationManagerBuilder: AuthenticationManagerBuilder) {
        authenticationManagerBuilder
            .authenticationProvider(JwtAuthenticationProvider(service, manager))
            .userDetailsService(service)
            .passwordEncoder(passwordEncoderBean())
    }

    @Bean
    fun passwordEncoderBean() = PasswordEncoder()

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    override fun configure(http: HttpSecurity) {
        // Disabled cors because we use REST
        http.cors().disable()

            // make sure we use stateless session; session won't be used to store user's state.
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

            // Disable default caching headers
            .and()
            .headers().cacheControl().disable() // Disable default caching headers

            // Set cache control if not already present
            .and()
            .headers().addHeaderWriter { _, response ->
                if (!response.containsHeader(HttpHeaders.CACHE_CONTROL)) {
                    response.addHeader(
                        HttpHeaders.CACHE_CONTROL,
                        CacheControl.empty().noTransform().cachePrivate().mustRevalidate().headerValue
                    )
                }
            }

            //No CSRF protection required because we are only providing REST access
            .and()
            .csrf().disable()

            // Register custom Exception handlers
            .exceptionHandling()
            .authenticationEntryPoint(TinTokAuthenticationEntryPoint())
            .accessDeniedHandler(TinTokAccessDeniedHandler())

            // Secure every route except the ones from above's NON_SECURED_PATHS
            .and()
            .authorizeRequests()
            .antMatchers(*NON_SECURED_PATHS).permitAll()
            .anyRequest().authenticated()

            // Add jwt token filter to Springs filter chain
            .and()
            .addFilterBefore(
                JwtAuthenticationProviderFilter(*NON_SECURED_PATHS),
                UsernamePasswordAuthenticationFilter::class.java
            )
    }

}