package de.iptk.groupj.user.adapter.storage

import de.iptk.groupj.user.domain.UserEntity
import org.springframework.content.commons.repository.ContentStore
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Filesystem storage abstraction for a Users profile picture.
 */
@Repository
interface ProfilePictureStore : ContentStore<UserEntity, UUID>