package de.iptk.groupj.user.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.shared.MATCHES
import de.iptk.groupj.shared.ResponseEntities
import de.iptk.groupj.user.application.MatchesService
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.mapper.UserMapper
import de.iptk.groupj.user.model.MatchProfileDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(MATCHES)
class MatchController(private val service: MatchesService) {

    /**
     * Get a Collection of all your Matches.
     */
    @GetMapping
    fun getAllMatches(@CurrentUser me: UserEntity): ResponseEntity<List<MatchProfileDto>> =
        service.findAllMatches(me)
            .map(UserMapper.INSTANCE::mapToMatchProfileDto)
            .let(ResponseEntities::ok)

}