package de.iptk.groupj.user.adapter.database

import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.neo4j.repository.Neo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import java.util.*

/**
 * User database abstraction.
 */
@Repository
interface UserRepository : Neo4jRepository<UserEntity, UUID> {

    // Find
    fun findByUsername(username: String): Optional<UserEntity>

    @Query(
        """
        MATCH (:User { username: ${'$'}username }) - [:MATCHED] - (match:User)
        RETURN match
        ORDER BY toLower(match.given_name) ASC, toLower(match.family_name) ASC
        """
    )
    fun findAllMatches(username: String): Set<UserEntity>

    // Delete
    @Query(
        """
        MATCH (user:User { username: ${'$'}username })
        OPTIONAL MATCH (user) - [:PUBLISHED] -> (post:Post)
        OPTIONAL MATCH (content:Content) - [:CONTENT] -> (post)
        OPTIONAL MATCH (message:ChatMessage) - [:SUBJECT | PARTNER] -> (user)
        DETACH DELETE content, post, message, user
        """,
        delete = true
    )
    fun deleteByUsername(username: String)


    // Predicates
    @Query(
        """
        MATCH (user:User)
        WHERE user.username =~ "(?i)" + ${'$'}username
        RETURN count(user) > 0
        """
    )
    fun existsByUsername(username: String): Boolean


    // Count
    @Query("MATCH (user:User) RETURN count(user)")
    fun countAllUsers(): Long


    // Matches
    @Query(
        """
        MATCH (first:User { username: ${'$'}firstUsername }), (second:User { username: ${'$'}secondUsername })
        MERGE (first) - [:MATCHED] - (second)
        """
    )
    fun saveMatched(firstUsername: String, secondUsername: String)

    @Query("RETURN EXISTS( (:User { id: ${'$'}firstId }) - [:MATCHED] - (:User { id: ${'$'}secondId }) )")
    fun isMatched(firstId: UUID, secondId: UUID): Boolean

    @Query("RETURN EXISTS( (:User { username: ${'$'}firstUsername }) - [:MATCHED] - (:User { username: ${'$'}secondUsername }) )")
    fun isMatched(firstUsername: String, secondUsername: String): Boolean

    // FCM
    @Query("MATCH (u:User) WHERE u.fcm_token IS NOT NULL RETURN u.id")
    fun findAllUserIdsWhereFcmTokenIsNotNull(): Set<UUID>

    @Query("MATCH (u:User) WHERE u.id IN \$ids REMOVE u.fcm_token")
    fun removeFcmTokenForUserIdIn(ids: Set<UUID>)

}