package de.iptk.groupj.user.adapter.rest

import de.iptk.groupj.auth.model.UpdatePasswordCommand
import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.shared.USER_ME
import de.iptk.groupj.shared.USER_ME_PASSWORD
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.mapper.UserMapper
import de.iptk.groupj.user.model.UpdateProfileCommand
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


/**
 * Controller concerned with operations impacting your self.
 */
@Validated
@RestController
@RequestMapping(USER_ME)
class UserMeController(private val service: UserService) {

    @GetMapping
    fun userInfo(@CurrentUser me: UserEntity) = UserMapper.INSTANCE.mapToUserMeDto(me)

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateMe(@CurrentUser me: UserEntity, @Valid @RequestBody command: UpdateProfileCommand) =
        service.update(me, command)

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteMe(@CurrentUser me: UserEntity) = service.delete(me)

    @PutMapping(path = [USER_ME_PASSWORD])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun changePassword(@CurrentUser me: UserEntity, @Valid @RequestBody command: UpdatePasswordCommand) =
        service.changePassword(me, command.oldPassword, command.newPassword)

}