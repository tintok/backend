package de.iptk.groupj.user.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.shared.*
import de.iptk.groupj.user.application.ProfilePictureService
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.exception.ProfilePictureIsNotAPictureException
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.util.*

@RestController
class ProfilePictureController(private val service: ProfilePictureService) {

    @GetMapping(path = ["$USER/{id}$PROFILE_PICTURE"])
    fun getProfilePicture(@PathVariable id: UUID): ResponseEntity<Resource> =
        defaultCacheableContentResponse(service.find(id))

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(path = [USER_ME_PROFILE_PICTURE], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun update(@CurrentUser me: UserEntity, @RequestPart("picture") picture: MultipartFile) {
        if (!validateMultipartContentType(picture.contentType, ALLOWED_IMAGE_TYPES)) {
            throw ProfilePictureIsNotAPictureException(picture.contentType)
        }

        service.update(me, picture.inputStream, picture.contentType!!)
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = [USER_ME_PROFILE_PICTURE])
    fun delete(@CurrentUser me: UserEntity) {
        service.delete(me)
    }

}