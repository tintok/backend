package de.iptk.groupj.user.mapper

import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.model.MatchProfileDto
import de.iptk.groupj.user.model.ProfileDto
import de.iptk.groupj.user.model.UpdateProfileCommand
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(uses = [PasswordEncoder::class])
interface UserMapper {

    companion object {
        @JvmField
        val INSTANCE: UserMapper = Mappers.getMapper(UserMapper::class.java)
    }

    fun mapToUserMeDto(me: UserEntity): ProfileDto

    @Mappings(Mapping(target = "name", expression = "java(match.getGivenName() + \" \" + match.getFamilyName())"))
    fun mapToMatchProfileDto(match: UserEntity): MatchProfileDto

    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "pictureId", ignore = true),
        Mapping(target = "pictureLength", ignore = true),
        Mapping(target = "pictureMimeType", ignore = true),
        Mapping(target = "password", ignore = true),
        Mapping(target = "fcmToken", ignore = true),
        Mapping(target = "authorities", ignore = true)
    )
    fun update(userUpdate: UpdateProfileCommand, @MappingTarget me: UserEntity): UserEntity

}