package de.iptk.groupj.user.authorization

import de.iptk.groupj.shared.AuthorizationEvaluator
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * This classes handles all Spring Security related approvals in the User domain.
 */
@AuthorizationEvaluator("userAuthorizationSecurityEvaluator")
class UserAuthorizationEvaluator(private val repository: UserRepository) {

    private val log = logger()

    /**
     * Returns true if the user id of the User and the target id match or if the two Users are matched.
     */
    @Transactional(readOnly = true)
    fun isSelfOrIsFromMatched(me: UserEntity, partnerId: UUID): Boolean = me.id == partnerId || areMatched(me, partnerId)

    /**
     * Returns true if the user id of the User and the target id are matched. If the target id does not exist true is
     * returned because then the application can fail with 404 instead of 403.
     */
    @Transactional(readOnly = true)
    fun areMatched(me: UserEntity, partnerId: UUID): Boolean {
        val exist = repository.existsById(partnerId)
        val isMatched = repository.isMatched(me.id!!, partnerId)
        val granted = !exist || isMatched

        when {
            !exist -> log.info("User(id={}) accessed non existing User(id={})", me.id, partnerId)
            !granted -> log.info("User(id={}) is not matched to User(id={})", me.id, partnerId)
        }

        return granted
    }

}