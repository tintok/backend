package de.iptk.groupj.user.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * Dto for retrieving you own User Profile.
 */
data class ProfileDto(

    val id: UUID,

    val username: String,

    @JsonProperty("given_name")
    val givenName: String,

    @JsonProperty("family_name")
    val familyName: String,

    val location: String? = null,

    val biography: String? = null

)