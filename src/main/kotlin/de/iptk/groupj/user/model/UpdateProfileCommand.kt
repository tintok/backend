package de.iptk.groupj.user.model

import com.fasterxml.jackson.annotation.JsonProperty
import de.iptk.groupj.shared.USER_MAX_BIOGRAPHY_LENGTH
import de.iptk.groupj.shared.USER_MAX_FAMILY_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_GIVEN_NAME_LENGTH
import de.iptk.groupj.shared.USER_MAX_LOCATION_LENGTH
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * Updating your own profile.
 */
data class UpdateProfileCommand(

    @field:NotBlank
    @JsonProperty("given_name")
    @field:Size(max = USER_MAX_GIVEN_NAME_LENGTH)
    val givenName: String,

    @field:NotBlank
    @JsonProperty("family_name")
    @field:Size(max = USER_MAX_FAMILY_NAME_LENGTH)
    val familyName: String,

    @field:Size(max = USER_MAX_LOCATION_LENGTH)
    val location: String? = null,

    @field:Size(max = USER_MAX_BIOGRAPHY_LENGTH)
    val biography: String? = null

)