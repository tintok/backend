package de.iptk.groupj.user.model

import java.util.*

data class MatchProfileDto(val id: UUID, val name: String, val biography: String?, val location: String?)