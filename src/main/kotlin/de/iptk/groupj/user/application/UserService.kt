package de.iptk.groupj.user.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.eventing.UserDeleteEvent
import de.iptk.groupj.user.exception.OldPasswordIncorrectException
import de.iptk.groupj.user.exception.UsernameAlreadyInUseException
import de.iptk.groupj.user.mapper.UserMapper
import de.iptk.groupj.user.model.UpdateProfileCommand
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

/**
 * Service for handling all User related operations.
 */
@Service
class UserService(

    private val repository: UserRepository,

    private val publisher: ApplicationEventPublisher,

    private val encoder: PasswordEncoder

) {

    @Transactional(readOnly = true)
    fun existsByUsername(username: String): Boolean = repository.existsByUsername(username)

    @Transactional(readOnly = true)
    fun findByUsername(username: String): UserEntity =
        repository.findByUsername(username).orElseThrow { ResourceNotFoundException("User", username) }

    @Transactional(readOnly = true)
    fun existsById(userId: UUID): Boolean = repository.existsById(userId)

    /**
     * Because the method is only invoked on different Services we have to make sure in case of the
     * ResourceNotFoundException no rollback is triggered, otherwise, because the transaction is already closed,
     * this would lead to a crash.
     */
    @Transactional(readOnly = true, noRollbackFor = [ResourceNotFoundException::class])
    fun findById(userId: UUID): UserEntity =
        repository.findById(userId).orElseThrow { ResourceNotFoundException("User", userId) }

    @Transactional
    fun create(user: UserEntity): UserEntity {
        if (existsByUsername(user.username)) {
            throw UsernameAlreadyInUseException(user.username)
        }

        return repository.save(user)
    }

    @Transactional
    fun update(me: UserEntity, userUpdate: UpdateProfileCommand) {
        UserMapper.INSTANCE.update(userUpdate, me)
            .run(repository::save)
    }


    @Transactional
    fun update(me: UserEntity) {
        repository.save(me)
    }

    @Transactional
    fun delete(me: UserEntity) {
        publisher.publishEvent(UserDeleteEvent(me))
        repository.deleteByUsername(me.username)
    }

    @Transactional(readOnly = true)
    fun countUsers(): Long = repository.countAllUsers()

    @Transactional
    fun changePassword(me: UserEntity, oldPassword: String, newPassword: String) {
        // Only check password if pw is not null. If it is null that implies user is from oauth provider without pw.
        me.password?.let { password ->
            if (!encoder.matches(oldPassword, password)) {
                throw OldPasswordIncorrectException()
            }
        }

        me.setPassword(encoder.encode(newPassword))
        update(me)
    }

}