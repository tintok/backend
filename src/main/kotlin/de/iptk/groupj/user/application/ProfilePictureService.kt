package de.iptk.groupj.user.application

import de.iptk.groupj.user.adapter.storage.ProfilePictureStore
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.core.io.Resource
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.InputStream
import java.util.*

@Service
class ProfilePictureService(private val userService: UserService, private val store: ProfilePictureStore) {

    @Transactional(readOnly = true)
    @PreAuthorize("@userAuthorizationSecurityEvaluator.isSelfOrIsFromMatched(principal, #userId)")
    fun find(userId: UUID): Pair<String?, Resource?> = userService.findById(userId).let { user ->
        Pair(user.pictureMimeType, store.getResource(user))
    }

    @Transactional
    fun update(me: UserEntity, picture: InputStream, mimeType: String) {
        store.setContent(me, picture)
        me.pictureMimeType = mimeType
        userService.update(me)
    }


    @Transactional
    fun delete(me: UserEntity) {
        store.unsetContent(me)
        userService.update(me)
    }

}