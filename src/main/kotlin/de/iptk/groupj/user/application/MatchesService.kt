package de.iptk.groupj.user.application

import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class MatchesService(private val repository: UserRepository) {

    @Transactional(readOnly = true)
    fun findAllMatches(me: UserEntity): Set<UserEntity> = repository.findAllMatches(me.username)

}