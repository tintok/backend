package de.iptk.groupj.user.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus


/**
 * If an Username is already in use by another user.
 */
class UsernameAlreadyInUseException(username: String) : LocalizedException("$username is already in use") {

    override val messageKey = "user.username-in-use"

    override val status = HttpStatus.CONFLICT

    override val parameter: Array<Any> = arrayOf(username)

}