package de.iptk.groupj.user.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus

/**
 * Occurs in a password change if the supplied old password does not match the saved user password.
 */
class OldPasswordIncorrectException : LocalizedException("Old password is incorrect") {

    override val messageKey: String = "user.old-password-incorrect"

    override val status: HttpStatus = HttpStatus.FORBIDDEN

}