package de.iptk.groupj.user.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus

/**
 * Exception which occurs if an ApiUser tries to upload a new ProfileImage to his / her account but it is not of type
 * image.
 */
class ProfilePictureIsNotAPictureException(contentType: String?) :
    LocalizedException("${contentType ?: "No content type provided"} is not an image") {

    override val messageKey: String = "profile-picture-is-not-a-picture"

    override val status: HttpStatus = HttpStatus.UNSUPPORTED_MEDIA_TYPE

    override val parameter: Array<Any> = arrayOf(contentType ?: "No content type provided")

}