package de.iptk.groupj.user.eventing

import de.iptk.groupj.user.domain.UserEntity

/**
 * If first and second users have a match.
 */
data class MatchEvent(val first: UserEntity, val second: UserEntity)