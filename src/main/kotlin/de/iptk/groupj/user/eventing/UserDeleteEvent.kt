package de.iptk.groupj.user.eventing

import de.iptk.groupj.post.domain.ContentEntity
import de.iptk.groupj.user.domain.UserEntity

/**
 * Signal that a delete request for the included user was triggered.
 */
data class UserDeleteEvent(val user: UserEntity)

/**
 * Contains a Collection of a Users Post Content. Is used to delete the Content of a Users Post from the disk after
 * removal of a User.
 */
data class UserDeletedEvent(val posts: Collection<ContentEntity>)