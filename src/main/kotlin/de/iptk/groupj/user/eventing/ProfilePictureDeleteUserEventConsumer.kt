package de.iptk.groupj.user.eventing

import de.iptk.groupj.shared.EventConsumer
import de.iptk.groupj.user.adapter.storage.ProfilePictureStore
import org.springframework.scheduling.annotation.Async
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener

/**
 * Delete User Event Listener.
 */
@EventConsumer
class ProfilePictureDeleteUserEventConsumer(private val store: ProfilePictureStore) {

    /**
     * Method to remove a Users Profile Picture from disk if the User gets deleted.
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    fun afterUserDeleteEvent(event: UserDeleteEvent) {
        store.unsetContent(event.user)
    }

}