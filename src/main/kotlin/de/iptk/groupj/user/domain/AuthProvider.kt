package de.iptk.groupj.user.domain

/**
 * Auth Provider to determine if the Users Identity Provider.
 */
enum class AuthProvider {
    LOCAL, GOOGLE
}