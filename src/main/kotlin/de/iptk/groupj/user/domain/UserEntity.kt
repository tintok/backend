package de.iptk.groupj.user.domain

import de.iptk.groupj.shared.*
import org.springframework.content.commons.annotations.ContentId
import org.springframework.content.commons.annotations.ContentLength
import org.springframework.data.neo4j.core.schema.GeneratedValue
import org.springframework.data.neo4j.core.schema.Id
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Property
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

/**
 * Class which represents an User Object as Neo4j persistence object.
 */
@Node("User")
data class UserEntity(

    @Id
    @GeneratedValue
    @Property(name = "id")
    override var id: UUID? = null,

    @ContentId
    @Property(name = "picture_id")
    var pictureId: UUID? = null,

    @ContentLength
    @Property(name = "picture_length")
    var pictureLength: Long? = null,

    @field:Pattern(regexp = ALLOWED_IMAGE_TYPES)
    @Property(name = "picture_mime_type")
    var pictureMimeType: String? = null,

    @field:Email
    @Property(name = "username")
    @field:Size(max = USER_MAX_USERNAME_LENGTH)
    private val username: String,

    @field:NotBlank
    @Property(name = "given_name")
    @field:Size(max = USER_MAX_GIVEN_NAME_LENGTH)
    var givenName: String,

    @field:NotBlank
    @Property(name = "family_name")
    @field:Size(max = USER_MAX_FAMILY_NAME_LENGTH)
    var familyName: String,

    @Property(name = "provider")
    val provider: AuthProvider,

    @Property(name = "password")
    private var password: String? = null,

    @Property("location")
    @field:Size(max = USER_MAX_LOCATION_LENGTH)
    var location: String? = null,

    @Property("biography")
    @field:Size(max = USER_MAX_BIOGRAPHY_LENGTH)
    var biography: String? = null,

    @Property("fcm_token")
    var fcmToken: String? = null

) : Entity<UUID>(), UserDetails {

    // Spring Security related User Details
    override fun getPassword() = password
    fun setPassword(password: String) {
        this.password = password
    }

    override fun getUsername() = username

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true

    override fun getAuthorities(): Collection<GrantedAuthority> = emptyList()

}