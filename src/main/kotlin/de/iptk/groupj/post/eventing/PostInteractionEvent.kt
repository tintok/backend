package de.iptk.groupj.post.eventing

import de.iptk.groupj.post.domain.PostEntity
import de.iptk.groupj.user.domain.UserEntity
import java.util.*

/**
 * Signal that a post was interacted with: published, liked or disliked
 */
data class PostInteractionEvent(val user: UserEntity, val postId: UUID, val type: PostInteractionEventType) {
    constructor(post: PostEntity, type: PostInteractionEventType) : this(post.author!!, post.id!!, type)
}

enum class PostInteractionEventType {
    PUBLISHED,
    UPDATED,
    LIKED,
    DISLIKED,
    CLEARED
}
