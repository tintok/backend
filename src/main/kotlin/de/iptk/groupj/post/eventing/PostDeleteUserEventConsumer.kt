package de.iptk.groupj.post.eventing

import de.iptk.groupj.post.application.PostService
import de.iptk.groupj.shared.EventConsumer
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.eventing.UserDeleteEvent
import de.iptk.groupj.user.eventing.UserDeletedEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener

/**
 * Delete User Event Listener.
 */
@EventConsumer
class PostDeleteUserEventConsumer(private val publisher: ApplicationEventPublisher, private val service: PostService) {

    private val log = logger()

    /**
     * Event is handled synchronously. Find all Content produced by a User and mark it to be removed.
     */
    @EventListener
    fun onUserDeleteEvent(event: UserDeleteEvent) {
        service.findAllContentFromUser(event.user)
            .let(::UserDeletedEvent)
            .run(publisher::publishEvent)
    }


    /**
     * Event is only handled if no Exception occurred beforehand and is executed asynchronously. It is also executed
     * lastly.
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    fun afterUserDeletedEvent(event: UserDeletedEvent) {
        service.deleteContent(event.posts)
        log.info("deleted {} media files from disk on user removal", event.posts.size)
    }

}