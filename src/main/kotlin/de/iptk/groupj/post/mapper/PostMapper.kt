package de.iptk.groupj.post.mapper

import de.iptk.groupj.post.domain.*
import de.iptk.groupj.post.model.*
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.tag.model.TagDto
import de.iptk.groupj.user.domain.UserEntity
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers
import org.neo4j.driver.types.Point

/**
 * Map Content to and from requests.
 */
@Mapper
interface PostMapper {

    companion object {
        @JvmField
        val INSTANCE: PostMapper = Mappers.getMapper(PostMapper::class.java)
    }

    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "created", ignore = true),
        Mapping(target = "updated", ignore = true),
        Mapping(target = "author", ignore = true),
        Mapping(target = "tags", source = "tags")
    )
    fun mapToTextPostEntity(command: CreateTextPostCommand, tags: Collection<TagEntity>): TextPostEntity

    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "created", ignore = true),
        Mapping(target = "updated", ignore = true),
        Mapping(target = "author", ignore = true),
        Mapping(target = "tags", source = "tags")
    )
    fun mapToImagePostEntity(command: CreatePostWithContentCommand, tags: Collection<TagEntity>): ImagePostEntity

    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "created", ignore = true),
        Mapping(target = "updated", ignore = true),
        Mapping(target = "author", ignore = true),
        Mapping(target = "tags", source = "tags")
    )
    fun mapToVideoPostEntity(command: CreatePostWithContentCommand, tags: Collection<TagEntity>): VideoPostEntity

    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "created", ignore = true),
        Mapping(target = "updated", ignore = true),
        Mapping(target = "author", ignore = true),
        Mapping(target = "tags", source = "tags")
    )
    fun updatePost(command: UpdatePostCommand, tags: Collection<TagEntity>, @MappingTarget post: PostEntity)

    @Mappings(Mapping(target = "type", source = "."))
    fun mapToPostDto(post: PostEntity): PostDto

    @Mappings(Mapping(target = "name", expression = "java(author.getGivenName() + \" \" + author.getFamilyName())"))
    fun mapToAuthorDto(author: UserEntity): AuthorDto

    // Custom mapping methods
    @JvmDefault
    fun mapPointToLocation(point: Point) = Location(point.y(), point.x())

    @JvmDefault
    fun mapLocationToPoint(location: Location) = location.asPoint()

    @JvmDefault
    fun mapTagEntitiesToDtos(tags: Collection<TagEntity>) = tags.map(TagEntity::name).map(::TagDto).toSet()

    @JvmDefault
    fun mapTagNamesToDtos(tags: Collection<String>) = tags.map(::TagDto).toSet()

    /**
     * Map Post Domain class to PostType.
     */
    @JvmDefault
    fun mapPostToType(post: PostEntity) = when (post) {
        is TextPostEntity -> PostType.TEXT
        is ImagePostEntity -> PostType.IMAGE
        is VideoPostEntity -> PostType.VIDEO
    }

}