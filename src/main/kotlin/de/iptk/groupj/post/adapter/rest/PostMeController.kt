package de.iptk.groupj.post.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.post.application.PostMeService
import de.iptk.groupj.post.domain.PostType
import de.iptk.groupj.post.exception.InvalidPostContentException
import de.iptk.groupj.post.mapper.PostMapper
import de.iptk.groupj.post.model.CreatePostWithContentCommand
import de.iptk.groupj.post.model.CreateTextPostCommand
import de.iptk.groupj.post.model.PostDto
import de.iptk.groupj.post.model.UpdatePostCommand
import de.iptk.groupj.shared.*
import de.iptk.groupj.tag.application.TagService
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.util.*
import javax.validation.Valid

/**
 * Create and retrieve content from the backend.
 */
@Validated
@RestController
@RequestMapping(POSTS_ME)
class PostMeController(private val service: PostMeService, private val tagService: TagService) {

    /**
     * Submit a new Text Post.
     */
    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], params = ["text_post", "!image_post", "!video_post"])
    fun createTextPost(@Valid @RequestPart("text_post") command: CreateTextPostCommand): ResponseEntity<PostDto> {
        val tags = tagService.findAllTagsByName(command.tags)
        val textPost = PostMapper.INSTANCE.mapToTextPostEntity(command, tags)
            .let(service::createTextPost)
            .let(PostMapper.INSTANCE::mapToPostDto)

        val location: URI = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("{postId}")
            .build(textPost.id)

        return ResponseEntity.created(location).body(textPost)
    }

    /**
     * Submit a new Image Post.
     */
    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], params = ["!text_post", "image_post", "!video_post"])
    fun createImagePost(
        @RequestPart("image") image: MultipartFile,
        @Valid @RequestPart("image_post") attributes: CreatePostWithContentCommand
    ): ResponseEntity<PostDto> {

        if (!validateMultipartContentType(image.contentType, ALLOWED_IMAGE_TYPES)) {
            throw InvalidPostContentException(image.contentType, PostType.IMAGE)
        }

        val tags = tagService.findAllTagsByName(attributes.tags)
        val imagePost = PostMapper.INSTANCE.mapToImagePostEntity(attributes, tags)
            .let { post -> service.createImagePost(post, image.contentType!!, image.inputStream) }
            .let(PostMapper.INSTANCE::mapToPostDto)

        val location: URI = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("{postId}")
            .build(imagePost.id)

        return ResponseEntity.created(location).body(imagePost)
    }

    /**
     * Submit a new Video Post.
     */
    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], params = ["!text_post", "!image_post", "video_post"])
    fun createVideoPost(
        @RequestPart("video") video: MultipartFile,
        @Valid @RequestPart("video_post") command: CreatePostWithContentCommand
    ): ResponseEntity<PostDto> {

        if (!validateMultipartContentType(video.contentType, ALLOWED_VIDEO_TYPES)) {
            throw InvalidPostContentException(video.contentType, PostType.IMAGE)
        }

        val tags = tagService.findAllTagsByName(command.tags)
        val videoPost = PostMapper.INSTANCE.mapToVideoPostEntity(command, tags)
            .let { post -> service.createVideoPost(post, video.contentType!!, video.inputStream) }
            .let(PostMapper.INSTANCE::mapToPostDto)

        val location: URI = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("{postId}")
            .build(videoPost.id)

        return ResponseEntity.created(location).body(videoPost)
    }

    /**
     * Find all your Posts.
     */
    @GetMapping
    fun getMyPosts(@CurrentUser me: UserEntity, pageable: Pageable): ResponseEntity<Page<PostDto>> =
        service.findAllMyPosts(me, pageable)
            .map(PostMapper.INSTANCE::mapToPostDto)
            .let(ResponseEntities::ok)

    /**
     * Update one of your Posts.
     */
    @PutMapping(path = ["{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(@CurrentUser me: UserEntity, @PathVariable id: UUID, @Valid @RequestBody command: UpdatePostCommand) {
        val tags = tagService.findAllTagsByName(command.tags)
        service.update(id, me, command, tags)
    }

    /**
     * Delete one of your Posts.
     */
    @DeleteMapping(path = ["{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@CurrentUser me: UserEntity, @PathVariable id: UUID) {
        service.delete(id, me)
    }

}
