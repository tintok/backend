package de.iptk.groupj.post.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.post.application.PostService
import de.iptk.groupj.post.mapper.PostMapper
import de.iptk.groupj.post.model.PostDto
import de.iptk.groupj.shared.*
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping(POSTS)
class PostController(private val service: PostService) {

    /**
     * Find one of your Posts.
     */
    @GetMapping(path = ["{id}"])
    fun getPost(@PathVariable id: UUID): PostDto = service.findPost(id).let(PostMapper.INSTANCE::mapToPostDto)

    /**
     * Retrieve Image data from one of your Image Posts.
     */
    @GetMapping(path = ["{id}$POST_IMAGE_DATA"])
    fun getPostImageData(@PathVariable id: UUID): ResponseEntity<Resource> =
        cacheableContentResponse(service.findImagePostData(id))

    /**
     * Retrieve Video data from one of your Video Posts.
     */
    @GetMapping(path = ["{id}$POST_VIDEO_DATA"])
    fun getPostVideoData(@PathVariable id: UUID): ResponseEntity<Resource> =
        cacheableContentResponse(service.findVideoPostData(id))

    /**
     * Like a Post.
     */
    @PostMapping(path = ["{id}$LIKE"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun like(@CurrentUser me: UserEntity, @PathVariable id: UUID) {
        service.like(id, me)
    }

    /**
     * Dislike a Post.
     */
    @PostMapping(path = ["{id}$DISLIKE"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun dislike(@CurrentUser me: UserEntity, @PathVariable id: UUID) {
        service.dislike(id, me)
    }

    /**
     * Clear a Post.
     */
    @PostMapping(path = ["{id}$CLEAR"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun clear(@CurrentUser me: UserEntity, @PathVariable id: UUID) {
        service.clear(id, me)
    }

}