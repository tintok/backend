package de.iptk.groupj.post.adapter.database

import de.iptk.groupj.post.model.PostFeedDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.time.ZonedDateTime

/**
 * Interface which extends [PostRepository] with the ability to query for FeedPosts.
 */
interface PostRepositoryExtension {

    /**
     * Find all posts not before and not after a certain point in time from your matches. This is used for the feed.
     */
    fun findPostsFromMatches(
        username: String, nbf: ZonedDateTime, naf: ZonedDateTime, pageable: Pageable
    ): Page<PostFeedDto>

}