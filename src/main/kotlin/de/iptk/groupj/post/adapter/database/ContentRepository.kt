package de.iptk.groupj.post.adapter.database

import de.iptk.groupj.post.domain.ContentEntity
import de.iptk.groupj.post.domain.ImageContentEntity
import de.iptk.groupj.post.domain.VideoContentEntity
import org.springframework.data.neo4j.repository.Neo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Filesystem storage abstraction for a Posts content like videos and pictures.
 */
@Repository
interface ContentRepository : Neo4jRepository<ContentEntity, UUID> {

    /**
     * Finds all content from a User. Fetches the content without the Posts author and tags to increase performance.
     */
    @Query(
        """
            MATCH (content:Content) - [:CONTENT] -> (post:Post) <- [:PUBLISHED] - (user:User)
            WHERE user.username = ${'$'}username AND NOT post:TextPost
            RETURN content{.id, .size, .type, __nodeLabels__: labels(content),
                           Content_CONTENT_Post: [(content) - [:CONTENT] -> (post) 
                               | post{.created, .id, .location, .text, .updated, __nodeLabels__: labels(post)}]
            }
         """
    )
    fun findContentFromUser(username: String): Set<ContentEntity>

    /**
     * Finds image content for a certainty Post.
     */
    @Query(
        """
            MATCH (image:ImageContent) - [content:CONTENT] -> (post:ImagePost)
            WHERE post.id = ${'$'}postId
            RETURN labels(image), image, collect(content), collect(post)
         """
    )
    fun findImageContent(postId: UUID): Optional<ImageContentEntity>

    /**
     * Finds video content for a certain Post.
     */
    @Query(
        """
            MATCH (video:VideoContent) - [content:CONTENT] -> (post:VideoPost)
            WHERE post.id = ${'$'}postId
            RETURN labels(video), video, collect(content), collect(post)
         """
    )
    fun findVideoContent(postId: UUID): Optional<VideoContentEntity>

}