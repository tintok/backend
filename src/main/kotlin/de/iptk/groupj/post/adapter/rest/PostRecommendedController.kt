package de.iptk.groupj.post.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.post.application.RecommendedPostsService
import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.post.mapper.PostMapper
import de.iptk.groupj.post.model.PostDto
import de.iptk.groupj.shared.POSTS_RECOMMENDED
import de.iptk.groupj.shared.ResponseEntities
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Positive
import javax.validation.constraints.Size

@Validated
@RestController
@RequestMapping(POSTS_RECOMMENDED)
class PostRecommendedController(private val service: RecommendedPostsService) {

    /**
     * Find all posts recommended to a User.
     */
    @GetMapping
    fun getRecommendedPosts(
        @Max(100) @Positive @RequestParam("size", defaultValue = "5") size: Int,
        @Valid location: Location,                                  // deserialize as empty collection
        @Size(max = 100) @RequestParam("except", defaultValue = "") excludedPostIds: Set<UUID>,
        @CurrentUser me: UserEntity
    ): ResponseEntity<List<PostDto>> =
        service.findRecommendedPosts(location, me, excludedPostIds, size)
            .map(PostMapper.INSTANCE::mapToPostDto)
            .let(ResponseEntities::ok)

}