package de.iptk.groupj.post.adapter.storage

import de.iptk.groupj.post.domain.ContentEntity
import org.springframework.content.commons.repository.ContentStore
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Filesystem abstraction for a Posts media files like image sand videos.
 */
@Repository
interface ContentStore : ContentStore<ContentEntity, UUID>