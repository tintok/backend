package de.iptk.groupj.post.adapter.database

import de.iptk.groupj.post.domain.PostEntity
import org.neo4j.driver.types.Point
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.neo4j.repository.Neo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Post database abstraction.
 */
@Repository
interface PostRepository : Neo4jRepository<PostEntity, UUID>, PostRepositoryExtension {

    // Find
    /**
     * Find all your Posts. Exclude author in query because it is not needed.
     */
    @Query(
        """
        MATCH (post:Post) <- [:PUBLISHED]-(:User { username: ${'$'}username }) 
        RETURN post{.created, .id, .location, .text, .updated, __nodeLabels__: labels(post),
                 Post_TAGGED_WITH_Tag: [ (post) - [:TAGGED_WITH] -> (post_tags:Tag)
                                         | post_tags{ .id, .name, __internalNeo4jId__: id(post_tags)} ]
                 }
        ORDER BY post.created DESC SKIP ${'$'}skip LIMIT ${'$'}limit
        """,
        countQuery = """
        MATCH (post:Post) <- [:PUBLISHED] - (:User { username: ${'$'}username })
        RETURN count(post)
        """
    )
    fun findAllYourPosts(username: String, pageable: Pageable): Page<PostEntity>

    /**
     * Find one of your posts. Exclude author in query because it is not needed.
     */
    @Query(
        """
        MATCH (post:Post {id: ${'$'}postId}) <- [:PUBLISHED]-(:User { username: ${'$'}username }) 
        RETURN post{.created, .id, .location, .text, .updated, __nodeLabels__: labels(post),
                 Post_TAGGED_WITH_Tag: [ (post) - [:TAGGED_WITH] -> (post_tags:Tag)
                                         | post_tags{ .id, .name, __internalNeo4jId__: id(post_tags)} ]
                 }
        """
    )
    fun findByIdAndAuthorUsername(postId: UUID, username: String): Optional<PostEntity>


    // Delete
    /**
     * Delete one of your posts.
     */
    @Query(
        """
        MATCH (post:Post { id: ${'$'}postId }) <- [:PUBLISHED] - (:User { username: ${'$'}username })
        OPTIONAL MATCH (post) <- [:CONTENT] - (content:Content)
        DETACH DELETE content, post
        """,
        delete = true
    )
    fun deleteOwnPost(postId: UUID, username: String)

    // Liking
    /**
     * Like a Post.
     */
    @Query(
        """
        MATCH (post:Post { id: ${'$'}postId }), (user:User { username: ${'$'}username })
        OPTIONAL MATCH (user) - [disliked:DISLIKED] -> (post)
        MERGE (user) - [:LIKED] -> (post)
        DELETE disliked
        """
    )
    fun like(postId: UUID, username: String)

    /**
     * Dislike a Post.
     */
    @Query(
        """
        MATCH (post:Post { id: ${'$'}postId }), (user:User { username: ${'$'}username })
        OPTIONAL MATCH (user) - [liked:LIKED] -> (post)
        MERGE (user) - [:DISLIKED] -> (post)
        DELETE liked
        """
    )
    fun dislike(postId: UUID, username: String)

    @Query(
        """
        MATCH (:Post { id: ${'$'}postId }) <- [r:LIKED|DISLIKED] - (:User { username: ${'$'}username })
        DELETE r
        RETURN count(r) > 0
        """
    )
    fun deleteLikeOrDislike(postId: UUID, username: String): Boolean


    // Recommendations
    /**
     * Finds posts recommended to a user. Posts are recommended if they are not published, liked or disliked by the same
     * user and if the user is not already matched with the publisher of the post. The postScore function is used to
     * sort the returned posts. If a similarity between the user and the publisher of the post exists, it's score is
     * multiplied with the score produced by the postScore function. If similarities between the user and the tags of
     * the post exist, their average is multiplied with the score.
     *
     * The function also ensures that a post is saved as recommended by creating a relationship valid for one day.
     *
     * @param username username of the user to find recommended posts for
     * @param point current location of the user
     * @param excludePosts posts to exclude from the result
     * @param limit amount of posts to return
     */
    @Query(
        """
        MATCH (me:User { username: ${'$'}username }), (publisher:User) - [:PUBLISHED] -> (post:Post)
        WHERE publisher <> me AND NOT (me) - [:MATCHED] - (publisher) AND NOT (me) - [:LIKED | DISLIKED] -> (post)
              AND NOT post.id IN ${'$'}excludePosts
        OPTIONAL MATCH (post) <- [likes:LIKED] - (:User)
        OPTIONAL MATCH (post) <- [dislikes:DISLIKED] - (:User)
        OPTIONAL MATCH (me) - [us:SIMILAR] - (publisher)
        OPTIONAL MATCH (me) - [ts:SIMILAR] - (:Tag) <- [:TAGGED_WITH] - (post)
        WITH post, me, count(likes) AS likes, count(dislikes) AS dislikes, coalesce(us.score, 1) AS userSimilarity,
             coalesce(avg(ts.score), 1) AS tagSimilarity, datetime() AS timestamp
        ORDER BY (
            de.iptk.groupj.postScore(post, likes, dislikes, distance(post.location, ${'$'}point)) * 
            userSimilarity * tagSimilarity
        ) DESC
        LIMIT ${'$'}limit
        MERGE (post) - [recommended:RECOMMENDED] -> (me)
        SET recommended.timestamp = timestamp
        RETURN DISTINCT post{.created, .id, .location, .text, .updated, __nodeLabels__: labels(post),
                 Post_TAGGED_WITH_Tag: [ (post) - [:TAGGED_WITH] -> (post_tags:Tag)
                                         | post_tags{ .id, .name, __internalNeo4jId__: id(post_tags)} ]
                 }
        """
    )
    fun findRecommendedPosts(username: String, point: Point, excludePosts: Set<UUID>, limit: Int): Set<PostEntity>

    /**
     * Finds posts recommend to a user. The posts are selected randomly from a pool of [poolSize] recent posts near the
     * given location where the amount of likes minus the amount of dislikes is not greater than [likeLimit] and the
     * post is not published, liked or disliked by the given user and the user is not already matched with the publisher
     * of the post. The pool is created by taking the age of the post in minutes times the distance of the post to the
     * given location in meters.
     *
     * The function also ensures that the post is saved as recommended by creating a relationship valid for one day.
     *
     * @param username username of the user to find recommended posts for
     * @param point current location of the user
     * @param excludePosts posts to exclude from the pool
     * @param poolSize amount posts to draw randomly from
     * @param likeLimit minimum amount of likes minus dislikes for a post to be added to the pool
     * @param limit amount of posts to return
     */
    @Query(
        """
        MATCH (me:User {username: ${'$'}username }), (publisher:User) - [:PUBLISHED] -> (post:Post)
        WHERE publisher <> me AND NOT (me) - [:MATCHED] - (publisher) AND NOT (me) - [:LIKED | DISLIKED] -> (post)
              AND NOT post.id IN ${'$'}excludePosts
        OPTIONAL MATCH (post) <- [likes:LIKED] - (:User)
        OPTIONAL MATCH (post) <- [dislikes:DISLIKED] - (:User)
        WITH post, me, publisher, count(likes) - count(dislikes) AS likes
        WHERE likes >= ${'$'}likeLimit
        WITH post, me, publisher, datetime() AS timestamp
        ORDER BY duration.inSeconds(post.created, timestamp).minutes * distance(post.location, ${'$'}point) ASC
        LIMIT ${'$'}poolSize
        WITH post, me, timestamp, rand() AS random
        ORDER BY random
        LIMIT ${'$'}limit
        MERGE (post) - [recommended:RECOMMENDED] -> (me)
        SET recommended.timestamp = timestamp
        RETURN post{.created, .id, .location, .text, .updated, __nodeLabels__: labels(post),
                 Post_TAGGED_WITH_Tag: [ (post) - [:TAGGED_WITH] -> (post_tags:Tag)
                                         | post_tags{ .id, .name, __internalNeo4jId__: id(post_tags)} ]
                 }
        """
    )
    fun findRandomRecommendedPosts(
        username: String, point: Point, excludePosts: Set<UUID>, poolSize: Int, likeLimit: Int, limit: Int
    ): Set<PostEntity>

    @Query(
        """
            MATCH (:Post) - [recommended:RECOMMENDED] -> (:User)
            WHERE recommended.timestamp + duration({hours: 24}) < datetime()
            DELETE recommended
            RETURN count(recommended)
        """,
        delete = true
    )
    fun deleteExpiredRecommendations(): Long


    // Predicates
    @Query(
        """
            MATCH (p:Post {id: ${'$'}postId}) - [r:RECOMMENDED] -> (u:User {username: ${'$'}username})
            WHERE r.timestamp + duration({hours: 24}) >= datetime()
            RETURN count(r) > 0
        """
    )
    fun isRecommendedPost(postId: UUID, username: String): Boolean

    /**
     * Check if a post belongs to a certain user with the supplied username.
     */
    @Query("RETURN EXISTS( (:Post { id: ${'$'}postId }) <- [:PUBLISHED] - (:User { username: ${'$'}username }) )")
    fun isPostFromUser(postId: UUID, username: String): Boolean

    /**
     * Returns true if a Post belongs to a User you are matched with. Id is the Post id and username is your username.
     */
    @Query(
        """
        MATCH (:User { username: ${'$'}username }) - [:MATCHED] - (:User) - [:PUBLISHED] -> (post:Post { id: ${'$'}postId })
        RETURN count(post) > 0
        """
    )
    fun isPostFromMatch(postId: UUID, username: String): Boolean

}