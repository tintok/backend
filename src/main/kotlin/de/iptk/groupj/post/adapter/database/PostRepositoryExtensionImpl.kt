package de.iptk.groupj.post.adapter.database

import de.iptk.groupj.post.domain.PostFeedStatus
import de.iptk.groupj.post.domain.PostType
import de.iptk.groupj.post.mapper.PostMapper
import de.iptk.groupj.post.model.AuthorDto
import de.iptk.groupj.post.model.PostFeedDto
import de.iptk.groupj.shared.asUUID
import org.neo4j.driver.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.neo4j.core.Neo4jTemplate
import org.springframework.data.neo4j.core.PreparedQuery
import org.springframework.stereotype.Repository
import java.time.ZonedDateTime
import java.util.*

private const val postsStatement = """
        MATCH (me:User { username: ${'$'}username }) - [:MATCHED] - (author:User) - [:PUBLISHED] -> (post:Post)
        WHERE post.created > ${'$'}nbf AND post.created < ${'$'}naf
        WITH post, author, me
        ORDER BY post.created DESC SKIP ${'$'}skip LIMIT ${'$'}limit
        MATCH (post) - [:TAGGED_WITH] -> (tag:Tag)
        OPTIONAL MATCH (post) <- [status:LIKED|DISLIKED] - (me)
        OPTIONAL MATCH (post) <- [likes:LIKED] - (:User)
        OPTIONAL MATCH (post) <- [dislikes:DISLIKED] - (:User)
        WITH post, collect(DISTINCT tag.name) AS tags, author, coalesce(type(status), "NEUTRAL") AS status, 
             count(DISTINCT likes) AS likes, count(DISTINCT dislikes) AS dislikes
        RETURN post{.id, .location, .text, type: labels(post), .created, .updated, likes: likes, dislikes: dislikes, 
                    status: status, tags: tags, author: author{.id, name: author.given_name + " " + author.family_name}}
        """

private const val countStatement = """
        MATCH (:User { username: ${'$'}username }) - [:MATCHED] - (:User) - [:PUBLISHED] -> (post:Post)
        WHERE post.created > ${'$'}nbf AND post.created < ${'$'}naf
        RETURN count(post)
        """

/**
 * Implementation of [PostRepositoryExtension] to query for FeedPosts which contain the number of likes / dislikes.
 */
@Repository
internal class PostRepositoryExtensionImpl(private val template: Neo4jTemplate) :
    PostRepositoryExtension {

    override fun findPostsFromMatches(
        username: String, nbf: ZonedDateTime, naf: ZonedDateTime, pageable: Pageable
    ): Page<PostFeedDto> {
        val total = count(username, nbf, naf)
        val posts = posts(username, nbf, naf, pageable)

        return PageImpl(posts, pageable, total)
    }

    /**
     * Fetch a Page of Post Feed Entities.
     */
    private fun posts(username: String, nbf: ZonedDateTime, naf: ZonedDateTime, pageable: Pageable)
            : List<PostFeedDto> {

        val parameter = mapOf(
            "username" to username,
            "nbf" to nbf,
            "naf" to naf,
            "skip" to pageable.offset,
            "limit" to pageable.pageSize
        )

        val query = PreparedQuery.queryFor(PostFeedDto::class.java)
            .withCypherQuery(postsStatement.trimIndent())
            .withParameters(parameter)
            .usingMappingFunction { _, record -> mapResultToPostFeedEntity(record["post"]) }
            .build()

        return template.toExecutableQuery(query).results
    }

    /**
     * Count a number of Posts from your matches with the given criteria.
     */
    private fun count(username: String, nbf: ZonedDateTime, naf: ZonedDateTime): Long {
        val parameter = mapOf("username" to username, "nbf" to nbf, "naf" to naf)
        return template.count(countStatement.trimIndent(), parameter)
    }

    /**
     * Map a Neo4J result set to a FeedPostEntity
     */
    private fun mapResultToPostFeedEntity(post: Value): PostFeedDto = PostFeedDto(
        UUID.fromString(post["id"].asString()),
        PostMapper.INSTANCE.mapPointToLocation(post["location"].asPoint()),
        post["text"].let { text -> if (text.isNull) null else text.asString() },
        PostMapper.INSTANCE.mapTagNamesToDtos(post["tags"].asList(Value::asString)),
        post["created"].asZonedDateTime(),
        post["updated"].asZonedDateTime(),
        mapNodeLabelsToType(post["type"].asList(Value::asString)),
        mapAuthor(post["author"]),
        post["likes"].asLong(),
        post["dislikes"].asLong(),
        PostFeedStatus.valueOf(post["status"].asString())
    )

    /**
     * Maps a List of Post Node Labels to a Node Type.
     */
    private fun mapNodeLabelsToType(labels: Collection<String>): PostType = when {
        labels.contains("TextPost") -> PostType.TEXT
        labels.contains("ImagePost") -> PostType.IMAGE
        labels.contains("VideoPost") -> PostType.VIDEO
        else -> throw NotImplementedError()
    }

    /**
     * Map an author result set to an [AuthorDto].
     */
    private fun mapAuthor(author: Value): AuthorDto = AuthorDto(
        author["id"].asUUID(),
        author["name"].asString()
    )

}