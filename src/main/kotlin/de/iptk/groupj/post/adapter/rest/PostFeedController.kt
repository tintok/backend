package de.iptk.groupj.post.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.post.application.PostFeedService
import de.iptk.groupj.post.model.PostFeedDto
import de.iptk.groupj.shared.POSTS_FEED
import de.iptk.groupj.shared.ResponseEntities
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.ZoneId
import java.time.ZonedDateTime

@RestController
@RequestMapping(POSTS_FEED)
class PostFeedController(private val service: PostFeedService) {

    @GetMapping
    fun getPostsForFeed(
        @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) nbf: ZonedDateTime?,
        @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) naf: ZonedDateTime?,
        pageable: Pageable,
        @CurrentUser me: UserEntity
    ): ResponseEntity<Page<PostFeedDto>> {
        val notBefore = nbf ?: ZonedDateTime.of(1900, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault())
        val notAfter = naf ?: ZonedDateTime.of(2100, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault())

        return service.findPostsFromMatches(me, notBefore, notAfter, pageable).let(ResponseEntities::ok)
    }

}