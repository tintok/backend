package de.iptk.groupj.post.authorization

import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.shared.AuthorizationEvaluator
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.StopWatch
import java.util.*

/**
 * Check if a User is allowed to query one specific Post.
 */
@AuthorizationEvaluator("postAuthorizationSecurityEvaluator")
class PostAuthorizationEvaluator(private val repository: PostRepository) {

    private val log = logger()

    /**
     * Checks if a given Post is allowed to be accessed by a given User. Is allowed if the Post is from a match,
     * is the users own post or if the post is recommended.
     */
    @Transactional(readOnly = true)
    fun isMyPostOrFromMatchOrRecommended(me: UserEntity, postId: UUID): Boolean {
        val exists = repository.existsById(postId)
        val granted = !exists || repository.isPostFromUser(postId, me.username)
                || repository.isPostFromMatch(postId, me.username)
                || repository.isRecommendedPost(postId, me.username)

        when {
            !exists -> log.info("User(id={}) tried to access non existing Post(id={}))", me.id, postId)
            !granted -> log.info("User(id={}) tried to access denied Post(id={})", me.id, postId)
        }

        return granted
    }

    /**
     * Removes all expired RECOMMENDED relationships from Neo4J. They are expired after 24 hours without renewal.
     * This method is run every hour.
     */
    @Transactional
    @Scheduled(cron = "0 0 * * * *")
    internal fun removeExpiredRecommendations() {
        val watch = StopWatch("Recommendation CleanUp")
        watch.start()
        val cleanedUpRecommendations = repository.deleteExpiredRecommendations()
        watch.stop()

        if (cleanedUpRecommendations > 0) {
            log.info(watch.shortSummary())
        }
    }

}