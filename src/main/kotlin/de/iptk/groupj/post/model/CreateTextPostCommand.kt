package de.iptk.groupj.post.model

import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.tag.model.TagDto


import javax.validation.constraints.NotBlank

class CreateTextPostCommand(location: Location, tags: Set<TagDto>, @field:NotBlank override val text: String) :
    CreatePostCommand(location, tags)
