package de.iptk.groupj.post.model

import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.post.domain.PostFeedStatus
import de.iptk.groupj.post.domain.PostType
import de.iptk.groupj.tag.model.TagDto
import org.springframework.format.annotation.DateTimeFormat
import java.time.ZonedDateTime
import java.util.*

data class PostDto(

    val id: UUID,

    val location: Location,

    val text: String?,

    val tags: Set<TagDto>,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val created: ZonedDateTime,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val updated: ZonedDateTime,

    val type: PostType

)

data class PostFeedDto(

    val id: UUID,

    val location: Location,

    val text: String?,

    val tags: Set<TagDto>,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val created: ZonedDateTime,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val updated: ZonedDateTime,

    val type: PostType,

    val author: AuthorDto,

    val likes: Long,

    val dislikes: Long,

    val status: PostFeedStatus

)

data class AuthorDto(val id: UUID, val name: String)