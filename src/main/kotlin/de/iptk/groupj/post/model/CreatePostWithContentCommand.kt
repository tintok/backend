package de.iptk.groupj.post.model

import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.tag.model.TagDto

class CreatePostWithContentCommand(location: Location, tags: Set<TagDto>, override val text: String?) :
    CreatePostCommand(location, tags)
