package de.iptk.groupj.post.model

import de.iptk.groupj.shared.POST_TAG_MAX_SIZE
import de.iptk.groupj.shared.POST_TAG_MIN_SIZE
import de.iptk.groupj.shared.POST_TEXT_MAX_SIZE
import de.iptk.groupj.tag.model.TagDto
import javax.validation.constraints.Size

data class UpdatePostCommand(

    @field:Size(max = POST_TEXT_MAX_SIZE)
    val text: String?,

    @field:Size(min = POST_TAG_MIN_SIZE, max = POST_TAG_MAX_SIZE)
    val tags: Set<TagDto>

)