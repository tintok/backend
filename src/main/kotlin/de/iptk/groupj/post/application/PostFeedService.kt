package de.iptk.groupj.post.application

import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.post.model.PostFeedDto
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime

@Service
class PostFeedService(private val repository: PostRepository) {

    @Transactional(readOnly = true)
    fun findPostsFromMatches(me: UserEntity, nbf: ZonedDateTime, naf: ZonedDateTime, pageable: Pageable)
            : Page<PostFeedDto> = repository.findPostsFromMatches(me.username, nbf, naf, pageable)

}