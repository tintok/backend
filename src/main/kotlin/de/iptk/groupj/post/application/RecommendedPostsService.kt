package de.iptk.groupj.post.application

import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.post.domain.Location
import de.iptk.groupj.post.domain.PostEntity
import de.iptk.groupj.shared.REC_RANDOM_CHANCE
import de.iptk.groupj.shared.REC_RANDOM_LIKE_LIMIT
import de.iptk.groupj.shared.REC_RANDOM_POOL
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.dao.TransientDataAccessResourceException
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.random.Random

/**
 * Service for retrieving all recommended Posts.
 */
@Service
class RecommendedPostsService(private val repository: PostRepository) {

    @Transactional
    @Retryable(include = [TransientDataAccessResourceException::class], maxAttempts = 5, backoff = Backoff(delay = 150))
    fun findRecommendedPosts(location: Location, me: UserEntity, excludedPostIds: Set<UUID>, size: Int): Set<PostEntity> {
        // determine amount of random posts
        val randomAmount = List(size) { Random.nextDouble() }.filter { it < REC_RANDOM_CHANCE }.count()

        // find "normal" recommended posts
        val posts = repository.findRecommendedPosts(
            me.username, location.asPoint(), excludedPostIds, size - randomAmount
        )

        // insert random posts at random locations
        return if (randomAmount > 0) {
            val randomPosts = repository.findRandomRecommendedPosts(
                me.username,
                location.asPoint(),
                excludedPostIds + posts.map { post -> post.id!! },
                REC_RANDOM_POOL * randomAmount,
                REC_RANDOM_LIKE_LIMIT,
                randomAmount
            )

            (randomPosts + posts).shuffled().toSet()
        } else {
            posts
        }
    }

}
