package de.iptk.groupj.post.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.post.adapter.database.ContentRepository
import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.post.adapter.storage.ContentStore
import de.iptk.groupj.post.domain.*
import de.iptk.groupj.post.eventing.PostInteractionEvent
import de.iptk.groupj.post.eventing.PostInteractionEventType
import de.iptk.groupj.post.exception.TextPostBlankTextException
import de.iptk.groupj.post.mapper.PostMapper
import de.iptk.groupj.post.model.UpdatePostCommand
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.user.domain.UserEntity
import org.apache.commons.lang3.StringUtils
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.InputStream
import java.util.*

/**
 * Service for handling all Post operations related to the own user
 */
@Service
class PostMeService(
    private val repository: PostRepository, private val contentRepository: ContentRepository,
    private val contentStore: ContentStore, private val publisher: ApplicationEventPublisher
) {

    @Transactional
    fun createTextPost(post: TextPostEntity): TextPostEntity = repository.save(post).also { textPostEntity ->
        publisher.publishEvent(PostInteractionEvent(textPostEntity, PostInteractionEventType.PUBLISHED))
    }

    @Transactional
    fun createImagePost(post: ImagePostEntity, mimeType: String, file: InputStream): ImagePostEntity {
        val imagePostEntity = repository.save(post)
        val imageContent = ImageContentEntity(type = mimeType, post = imagePostEntity)
        contentStore.setContent(imageContent, file)
        contentRepository.save(imageContent)

        publisher.publishEvent(PostInteractionEvent(imagePostEntity, PostInteractionEventType.PUBLISHED))

        return imagePostEntity
    }

    @Transactional
    fun createVideoPost(post: VideoPostEntity, mimeType: String, file: InputStream): VideoPostEntity {
        val videoPostEntity = repository.save(post)
        val videoContent = VideoContentEntity(type = mimeType, post = videoPostEntity)
        contentStore.setContent(videoContent, file)
        contentRepository.save(videoContent)

        publisher.publishEvent(PostInteractionEvent(videoPostEntity, PostInteractionEventType.PUBLISHED))

        return videoPostEntity
    }

    @Transactional(readOnly = true)
    fun findAllMyPosts(me: UserEntity, pageable: Pageable): Page<PostEntity> =
        repository.findAllYourPosts(me.username, pageable)

    @Transactional(readOnly = true)
    protected fun find(postId: UUID, me: UserEntity): PostEntity =
        repository.findByIdAndAuthorUsername(postId, me.username).orElseThrow { ResourceNotFoundException("Post", postId) }

    @Transactional
    fun update(postId: UUID, me: UserEntity, command: UpdatePostCommand, tags: Set<TagEntity>) {
        val post = find(postId, me)

        if (post is TextPostEntity && StringUtils.isBlank(command.text)) {
            throw TextPostBlankTextException()
        }

        PostMapper.INSTANCE.updatePost(command, tags, post)
        repository.save(post)

        publisher.publishEvent(PostInteractionEvent(me, postId, PostInteractionEventType.UPDATED))
    }

    @Transactional
    fun delete(postId: UUID, me: UserEntity) {
        // Load Content if exists, also ensures that post exists.
        val content: Optional<out ContentEntity> = when (find(postId, me)) {
            is ImagePostEntity -> contentRepository.findImageContent(postId)
            is VideoPostEntity -> contentRepository.findVideoContent(postId)
            is TextPostEntity -> Optional.empty<ContentEntity>()
        }

        // Delete post
        repository.deleteOwnPost(postId, me.username)

        // Delete Content from disk
        content.ifPresent(contentStore::unsetContent)
    }

}