package de.iptk.groupj.post.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.post.adapter.database.ContentRepository
import de.iptk.groupj.post.adapter.database.PostRepository
import de.iptk.groupj.post.adapter.storage.ContentStore
import de.iptk.groupj.post.domain.ContentEntity
import de.iptk.groupj.post.domain.PostEntity
import de.iptk.groupj.post.eventing.PostInteractionEvent
import de.iptk.groupj.post.eventing.PostInteractionEventType
import de.iptk.groupj.post.exception.PostToLikeIsOwnPost
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.context.ApplicationEventPublisher
import org.springframework.core.io.Resource
import org.springframework.scheduling.annotation.Async
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class PostService(

    private val repository: PostRepository,

    private val contentRepository: ContentRepository,

    private val contentStore: ContentStore,

    private val publisher: ApplicationEventPublisher

) {

    @Transactional(readOnly = true)
    @PreAuthorize("@postAuthorizationSecurityEvaluator.isMyPostOrFromMatchOrRecommended(principal, #postId)")
    fun findPost(postId: UUID): PostEntity =
        repository.findById(postId).orElseThrow { ResourceNotFoundException("Post", postId) }

    @Transactional(readOnly = true)
    @PreAuthorize("@postAuthorizationSecurityEvaluator.isMyPostOrFromMatchOrRecommended(principal, #postId)")
    fun findImagePostData(postId: UUID): Pair<String, Resource> {
        val imageContent = contentRepository.findImageContent(postId)
            .orElseThrow { ResourceNotFoundException("Image Content for Post", postId) }

        val image = contentStore.getResource(imageContent.id)
        return Pair(imageContent.type, image)
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@postAuthorizationSecurityEvaluator.isMyPostOrFromMatchOrRecommended(principal, #postId)")
    fun findVideoPostData(postId: UUID): Pair<String, Resource?> {
        val videoContent = contentRepository.findVideoContent(postId)
            .orElseThrow { ResourceNotFoundException("Video Content for Post", postId) }

        val video = contentStore.getResource(videoContent.id)
        return Pair(videoContent.type, video)
    }

    @Transactional
    @PreAuthorize("@postAuthorizationSecurityEvaluator.isMyPostOrFromMatchOrRecommended(principal, #postId)")
    fun like(postId: UUID, me: UserEntity) {
        if (!repository.existsById(postId)) {
            throw ResourceNotFoundException("Post", postId)
        }

        if (repository.isPostFromUser(postId, me.username)) {
            throw PostToLikeIsOwnPost()
        }

        repository.like(postId, me.username)
        publisher.publishEvent(PostInteractionEvent(me, postId, PostInteractionEventType.LIKED))
    }

    @Transactional
    @PreAuthorize("@postAuthorizationSecurityEvaluator.isMyPostOrFromMatchOrRecommended(principal, #postId)")
    fun dislike(postId: UUID, me: UserEntity) {
        if (!repository.existsById(postId)) {
            throw ResourceNotFoundException("Post", postId)
        }

        if (repository.isPostFromUser(postId, me.username)) {
            throw PostToLikeIsOwnPost()
        }

        repository.dislike(postId, me.username)
        publisher.publishEvent(PostInteractionEvent(me, postId, PostInteractionEventType.DISLIKED))
    }

    @Transactional
    @PreAuthorize("@postAuthorizationSecurityEvaluator.isMyPostOrFromMatchOrRecommended(principal, #postId)")
    fun clear(postId: UUID, me: UserEntity) {
        if (!repository.existsById(postId)) {
            throw ResourceNotFoundException("Post", postId)
        }

        if (repository.deleteLikeOrDislike(postId, me.username)) {
            publisher.publishEvent(PostInteractionEvent(me, postId, PostInteractionEventType.CLEARED))
        }
    }

    @Transactional(readOnly = true)
    fun findAllContentFromUser(user: UserEntity): Set<ContentEntity> =
        contentRepository.findContentFromUser(user.username)

    /**
     * This is used to delete all media files from a given List of Post. It is called asynchronously to further improve
     * performance.
     */
    @Async
    fun deleteContent(content: Collection<ContentEntity>) {
        content.forEach(contentStore::unsetContent)
    }

}