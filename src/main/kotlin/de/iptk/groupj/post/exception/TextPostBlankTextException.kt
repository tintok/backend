package de.iptk.groupj.post.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus

/**
 * If a user tries to update a TextPost with a blank text.
 */
class TextPostBlankTextException : LocalizedException("text can not be blank") {

    override val messageKey: String = "post.blank-text-post"

    override val status: HttpStatus = HttpStatus.UNPROCESSABLE_ENTITY

}