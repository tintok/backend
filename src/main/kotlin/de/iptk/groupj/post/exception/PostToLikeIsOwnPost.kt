package de.iptk.groupj.post.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus

/**
 * If a user tries to like one of his / her own posts.
 */
class PostToLikeIsOwnPost : LocalizedException("you can not like one of your own posts") {

    override val messageKey: String = "post.like-own-post"

    override val status: HttpStatus = HttpStatus.UNPROCESSABLE_ENTITY

}