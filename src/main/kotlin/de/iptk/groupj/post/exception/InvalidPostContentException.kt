package de.iptk.groupj.post.exception

import de.iptk.groupj.errorhandling.LocalizedException
import de.iptk.groupj.post.domain.PostType
import org.springframework.http.HttpStatus

private fun mimeType(mimeType: String?) = mimeType ?: "No content type provided"

private fun category(category: PostType) = if (category == PostType.IMAGE) "image" else "video"

/**
 * If a user tries to supply image/jpeg for an create-video operation.
 */
class InvalidPostContentException(mimeType: String?, category: PostType) :
    LocalizedException("${mimeType(mimeType)} is not an allowed ${category(category)} Content-Type") {

    override val messageKey: String = "post.invalid-post-content"

    override val status: HttpStatus = HttpStatus.UNSUPPORTED_MEDIA_TYPE

    override val parameter: Array<Any> = arrayOf(mimeType(mimeType), category(category))

}