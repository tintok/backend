package de.iptk.groupj.post.domain

import org.neo4j.driver.Values
import org.neo4j.driver.types.Point
import javax.validation.constraints.Max
import javax.validation.constraints.Min

/**
 * Location Model for Requests and Response. Longitude is also called x, latitude y.
 */
data class Location(

    @field:Max(90)
    @field:Min(-90)
    val latitude: Double,

    @field:Max(180)
    @field:Min(-180)
    val longitude: Double

) {

    /**
     * Create a EPSG:4326 compatible Point for the database from the given location. See also at https://epsg.io/4326
     */
    fun asPoint(): Point = Values.point(4326, longitude, latitude).asPoint()

}