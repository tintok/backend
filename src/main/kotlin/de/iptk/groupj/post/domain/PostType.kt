package de.iptk.groupj.post.domain

/**
 * Type of content to determine appropriate content subclass
 */
enum class PostType {
    TEXT, IMAGE, VIDEO
}