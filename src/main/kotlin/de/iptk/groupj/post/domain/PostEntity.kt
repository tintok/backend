package de.iptk.groupj.post.domain

import de.iptk.groupj.shared.AbstractAuditableBaseEntity
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.user.domain.UserEntity
import org.neo4j.driver.types.Point
import org.springframework.data.neo4j.core.schema.*
import org.springframework.data.neo4j.core.schema.Relationship.Direction.OUTGOING
import java.time.ZonedDateTime
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@Node("Post")
sealed class PostEntity(

    @Id
    @GeneratedValue
    @Property(name = "id")
    override var id: UUID?,

    @Property(name = "location")
    val location: Point,

    @field:NotEmpty
    @Relationship(type = "TAGGED_WITH", direction = OUTGOING)
    var tags: Set<TagEntity>,

    created: ZonedDateTime?,

    updated: ZonedDateTime?,

    author: UserEntity?

) : AbstractAuditableBaseEntity<UUID>(created, updated, author) {

    abstract var text: String?

}


@Node("TextPost")
class TextPostEntity(

    id: UUID?,

    location: Point,

    @field:NotBlank
    @Property(name = "text")
    override var text: String?,

    tags: Set<TagEntity> = emptySet(),

    created: ZonedDateTime?,

    updated: ZonedDateTime?,

    author: UserEntity?

) : PostEntity(id, location, tags, created, updated, author)


@Node("ImagePost")
class ImagePostEntity(

    id: UUID?,

    location: Point,

    @Property(name = "text")
    override var text: String?,

    tags: Set<TagEntity> = emptySet(),

    created: ZonedDateTime?,

    updated: ZonedDateTime?,

    author: UserEntity?

) : PostEntity(id, location, tags, created, updated, author)


@Node("VideoPost")
class VideoPostEntity(

    id: UUID?,

    location: Point,

    @Property(name = "text")
    override var text: String?,

    tags: Set<TagEntity> = emptySet(),

    created: ZonedDateTime?,

    updated: ZonedDateTime?,

    author: UserEntity?

) : PostEntity(id, location, tags, created, updated, author)