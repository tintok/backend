package de.iptk.groupj.post.domain

import de.iptk.groupj.shared.ALLOWED_IMAGE_TYPES
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Property
import java.util.*
import javax.validation.constraints.Pattern


@Node("ImageContent")
class ImageContentEntity(

    id: UUID? = null,

    size: Long? = null,

    @Property(name = "type")
    @field:Pattern(regexp = ALLOWED_IMAGE_TYPES)
    override val type: String,

    post: ImagePostEntity,

    version: Long? = null

) : ContentEntity(id, size, post, version)