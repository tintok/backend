package de.iptk.groupj.post.domain

import de.iptk.groupj.shared.ALLOWED_VIDEO_TYPES
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Property
import java.util.*
import javax.validation.constraints.Pattern


@Node("VideoContent")
class VideoContentEntity(

    id: UUID? = null,

    size: Long? = null,

    @Property(name = "type")
    @field:Pattern(regexp = ALLOWED_VIDEO_TYPES)
    override val type: String,

    post: VideoPostEntity,

    version: Long? = null

) : ContentEntity(id, size, post, version)