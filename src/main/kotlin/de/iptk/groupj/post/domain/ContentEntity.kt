package de.iptk.groupj.post.domain

import de.iptk.groupj.shared.Entity
import org.springframework.content.commons.annotations.ContentId
import org.springframework.content.commons.annotations.ContentLength
import org.springframework.data.annotation.Version
import org.springframework.data.neo4j.core.schema.Id
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Property
import org.springframework.data.neo4j.core.schema.Relationship
import org.springframework.data.neo4j.core.schema.Relationship.Direction.OUTGOING
import java.util.*
import javax.validation.constraints.NotNull

@Node("Content")
abstract class ContentEntity(

    @Id
    @ContentId
    @Property(name = "id")
    @field:NotNull
    override var id: UUID?,

    @ContentLength
    @Property(name = "size")
    @field:NotNull
    var size: Long? = null,

    @Relationship(type = "CONTENT", direction = OUTGOING)
    val post: PostEntity,

    @Version
    @Property(name = "version")
    var version: Long?

) : Entity<UUID>() {

    abstract val type: String

}