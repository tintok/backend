package de.iptk.groupj.post.domain

/**
 * Status of a post with regards to a users feed. The post can be LIKED or DISLIKED or neither of those (NEUTRAL).
 */
enum class PostFeedStatus {
    LIKED, NEUTRAL, DISLIKED
}