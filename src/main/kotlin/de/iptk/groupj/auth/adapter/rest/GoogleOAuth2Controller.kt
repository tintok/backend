package de.iptk.groupj.auth.adapter.rest

import de.iptk.groupj.auth.application.GoogleOAuth2TokenService
import de.iptk.groupj.auth.config.ConditionalOnGoogleOAuthLoginEnabled
import de.iptk.groupj.auth.mapper.AuthMapper
import de.iptk.groupj.auth.model.RefreshJwtTokenCommand
import de.iptk.groupj.security.application.JwtManager
import de.iptk.groupj.shared.*
import de.iptk.groupj.user.application.ProfilePictureService
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI

/**
 * Entry Point for GoogleOAuth2 flow.
 */
@RestController
@RequestMapping(OAUTH)
@ConditionalOnGoogleOAuthLoginEnabled
class GoogleOAuth2Controller(

    private val service: UserService,

    private val profilePictureService: ProfilePictureService,

    private val tokenService: GoogleOAuth2TokenService,

    private val jwtManager: JwtManager

) {

    init {
        logger().info("Hello from GoogleOAuth2Controller")
    }

    /**
     * Start OAuth flow for google login.
     */
    @GetMapping(OAUTH_GOOGLE_AUTH)
    fun googleOAuth2EntryPoint(): ServerResponse {
        val baseUrl = ServletUriComponentsBuilder.fromCurrentServletMapping().build().toString()

        return ServerResponse.status(HttpStatus.FOUND)
            .header(HttpHeaders.LOCATION, tokenService.authorizationUrl(baseUrl))
            .build()
    }

    /**
     * Return Token from Google and sing in.
     */
    @GetMapping(OAUTH_GOOGLE_CODE)
    fun login(@RequestParam code: String): ResponseEntity<RefreshJwtTokenCommand> {
        val baseUrl = ServletUriComponentsBuilder.fromCurrentServletMapping().build().toString()

        // Get user details from google
        val oAuthUserInfo = tokenService.userInfo(baseUrl, code)

        // Find user in database or create a new one
        return if (service.existsByUsername(oAuthUserInfo.username)) {
            val user: UserEntity = service.findByUsername(oAuthUserInfo.username)
            val token = jwtManager.createToken(user.username, user.id!!)

            ResponseEntity.ok(RefreshJwtTokenCommand(token))
        } else {
            // Create new User
            val location: URI = ServletUriComponentsBuilder
                .fromCurrentServletMapping()
                .path(USER_ME)
                .build()
                .toUri()
            val user = service.create(AuthMapper.INSTANCE.mapGoogleOAuthToUser(oAuthUserInfo))

            // Try to set ProfilePicture from OAuth Provider
            val profileImage = AuthMapper.INSTANCE.oAuthProfileGetPicture(oAuthUserInfo)
            profileImage?.let { picture ->
                profilePictureService.update(user, picture.first, picture.second)
            }

            // Create Token
            val token = jwtManager.createToken(user.username, user.id!!)
            ResponseEntity.created(location).body(RefreshJwtTokenCommand(token))
        }
    }

}