package de.iptk.groupj.auth.adapter.rest

import de.iptk.groupj.auth.mapper.AuthMapper
import de.iptk.groupj.auth.model.JwtTokenDto
import de.iptk.groupj.auth.model.RefreshJwtTokenCommand
import de.iptk.groupj.auth.model.SignInCommand
import de.iptk.groupj.auth.model.SignUpCommand
import de.iptk.groupj.security.application.JwtManager
import de.iptk.groupj.shared.*
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.mapper.UserMapper
import de.iptk.groupj.user.model.ProfileDto
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


/**
 * SignUp and authenticate against the backend with you username and password for general User authentication.
 */
@Validated
@RestController
@RequestMapping(AUTH)
class AuthController(

    private val service: UserService,

    private val authenticationManager: AuthenticationManager,

    private val jwtManager: JwtManager

) {

    /**
     * Register with your personal data.
     */
    @PostMapping(SIGN_UP)
    fun register(@Valid @RequestBody command: SignUpCommand, request: HttpServletRequest): ResponseEntity<ProfileDto> {
        val user = AuthMapper.INSTANCE.mapToUserEntity(command)
            .let(service::create)
            .let(UserMapper.INSTANCE::mapToUserMeDto)

        val location = ServletUriComponentsBuilder
            .fromServletMapping(request)
            .path(USER_ME)
            .build()
            .toUri()

        return ResponseEntity.created(location).body(user)
    }

    /**
     * Login endpoint for retrieving a token.
     */
    @PostMapping(SIGN_IN)
    fun login(@Valid @RequestBody command: SignInCommand): JwtTokenDto {
        val credentials = UsernamePasswordAuthenticationToken(command.username, command.password)

        // will throw AuthenticationException if invalid credentials
        val user = authenticationManager.authenticate(credentials).principal as UserEntity
        val token = jwtManager.createToken(command.username, user.id!!)

        return JwtTokenDto(token = token)
    }

    /**
     * Endpoint for refreshing your token if the token is not longer expired then two weeks.
     */
    @PostMapping(REFRESH)
    fun refresh(@RequestBody command: RefreshJwtTokenCommand): JwtTokenDto {
        val token = jwtManager.refresh(command.token)
        val username = jwtManager.extractUsername(token)

        if (!service.existsByUsername(username)) {
            throw UsernameNotFoundException("User with username $username does not exist anymore")
        }

        return JwtTokenDto(token = token)
    }

}