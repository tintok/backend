package de.iptk.groupj.auth.model

import javax.validation.constraints.Pattern

/**
 * Command to trade a JWT for a new JWT.
 */
data class RefreshJwtTokenCommand(

    @field:Pattern(regexp = "^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*\$")
    val token: String

)