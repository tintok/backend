package de.iptk.groupj.auth.model

import de.iptk.groupj.shared.MAX_PASSWORD_LENGTH
import de.iptk.groupj.shared.MIN_PASSWORD_LENGTH
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * Command for LogIn.
 */
data class SignInCommand(

    @field:Email
    val username: String,

    @field:NotBlank
    @field:Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    val password: String

)