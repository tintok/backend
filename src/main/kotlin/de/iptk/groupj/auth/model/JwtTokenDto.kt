package de.iptk.groupj.auth.model

/**
 * Token response.
 */
data class JwtTokenDto(val prefix: String = "Bearer", val token: String)