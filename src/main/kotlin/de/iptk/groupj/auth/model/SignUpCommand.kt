package de.iptk.groupj.auth.model

import com.fasterxml.jackson.annotation.JsonProperty
import de.iptk.groupj.shared.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * Command for SignUp.
 */
data class SignUpCommand(

    @field:Email
    @field:Size(max = USER_MAX_USERNAME_LENGTH)
    val username: String,

    @field:NotBlank
    @field:Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    val password: String,

    @field:NotBlank
    @JsonProperty("given_name")
    @field:Size(max = USER_MAX_GIVEN_NAME_LENGTH)
    val givenName: String,

    @field:NotBlank
    @JsonProperty("family_name")
    @field:Size(max = USER_MAX_FAMILY_NAME_LENGTH)
    val familyName: String,

    @field:Size(max = USER_MAX_LOCATION_LENGTH)
    val location: String? = null,

    @field:Size(max = USER_MAX_BIOGRAPHY_LENGTH)
    val biography: String? = null

)