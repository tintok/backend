package de.iptk.groupj.auth.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.URL

/**
 * Class which models the response of the Google OAuth2 user details.
 */
data class GoogleOAuthUserInfo(

    val id: String,

    @JsonProperty("email")
    val username: String,

    val verified_email: Boolean,

    val name: String,

    @JsonProperty("given_name")
    val givenName: String,

    @JsonProperty("family_name")
    val familyName: String,

    @field:URL
    val picture: String?,

    val locale: String

)