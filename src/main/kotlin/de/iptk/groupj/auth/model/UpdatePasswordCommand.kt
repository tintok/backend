package de.iptk.groupj.auth.model

import com.fasterxml.jackson.annotation.JsonProperty
import de.iptk.groupj.shared.MAX_PASSWORD_LENGTH
import de.iptk.groupj.shared.MIN_PASSWORD_LENGTH
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * Command for updating a users password.
 */
data class UpdatePasswordCommand(

    @field:NotBlank
    @field:Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    @JsonProperty("old_password")
    val oldPassword: String,

    @field:NotBlank
    @field:Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    @JsonProperty("new_password")
    val newPassword: String

)