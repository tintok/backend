package de.iptk.groupj.auth.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import java.lang.annotation.Inherited

/**
 * Annotation which checks if the User provided configuration for Google FCM. If not, this annotation
 * disables the annotated component.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@MustBeDocumented
@Inherited
@ConditionalOnProperty(prefix = "security.oauth2.provider.google", name = ["id", "secret"])
annotation class ConditionalOnGoogleOAuthLoginEnabled
