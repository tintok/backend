package de.iptk.groupj.auth.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus

/**
 * If the OAuth provider did not approve the provided code. Many reasons for the error are possible. For instance a user
 * could have supplied a wrong code. He or she could also supply a malformed code.
 */
class InvalidOAuthResponse(override val status: HttpStatus) : LocalizedException(
    "OAuthProvider refused code, most likely due to invalidity, please have a look at the " +
            "returned error code"
) {

    override val messageKey: String = "auth.oauth-error"

}