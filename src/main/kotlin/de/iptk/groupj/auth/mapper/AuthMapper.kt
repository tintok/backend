package de.iptk.groupj.auth.mapper

import de.iptk.groupj.auth.model.GoogleOAuthUserInfo
import de.iptk.groupj.auth.model.SignUpCommand
import de.iptk.groupj.security.application.PasswordEncoder
import de.iptk.groupj.user.domain.UserEntity
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.web.client.RestTemplate
import java.io.InputStream

/**
 * Mapper for mapping model classes in the auth domain.
 */
@Mapper(uses = [PasswordEncoder::class], disableSubMappingMethodsGeneration = true)
interface AuthMapper {

    companion object {
        @JvmField
        val INSTANCE: AuthMapper = Mappers.getMapper(AuthMapper::class.java)

        @JvmField
        val restTemplate = RestTemplate()
    }

    /**
     * Map [signUpCommand] to [UserEntity].
     */
    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "pictureId", ignore = true),
        Mapping(target = "pictureLength", ignore = true),
        Mapping(target = "pictureMimeType", ignore = true),
        Mapping(target = "authorities", ignore = true),
        Mapping(target = "password", qualifiedByName = ["encode"]),
        Mapping(target = "fcmToken", ignore = true),
        Mapping(target = "provider", constant = "LOCAL")
    )
    fun mapToUserEntity(signUpCommand: SignUpCommand): UserEntity

    /**
     * Map [googleOAuthUserInfo] to [UserEntity].
     */
    @Mappings(
        Mapping(target = "id", ignore = true),
        Mapping(target = "pictureId", ignore = true),
        Mapping(target = "pictureLength", ignore = true),
        Mapping(target = "pictureMimeType", ignore = true),
        Mapping(target = "password", ignore = true),
        Mapping(target = "location", ignore = true),
        Mapping(target = "biography", ignore = true),
        Mapping(target = "fcmToken", ignore = true),
        Mapping(target = "authorities", ignore = true),
        Mapping(target = "provider", constant = "GOOGLE")
    )
    fun mapGoogleOAuthToUser(googleOAuthUserInfo: GoogleOAuthUserInfo): UserEntity

    /**
     * Get profile picture from Google via [profile] OAuth2 details.
     */
    @JvmDefault
    fun oAuthProfileGetPicture(profile: GoogleOAuthUserInfo): Pair<InputStream, String>? {
        if (profile.picture != null) {
            val result = restTemplate.exchange(
                profile.picture,
                HttpMethod.GET,
                HttpEntity("parameters", HttpHeaders()),
                ByteArray::class.java
            )

            result.body?.let { body ->
                result.headers.contentType?.let { contentType ->
                    return Pair(body.inputStream(), contentType.toString())
                }
            }
        }

        return null
    }

}