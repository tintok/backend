package de.iptk.groupj.auth.mapper

import com.github.scribejava.core.model.OAuth2AccessTokenErrorResponse
import com.github.scribejava.core.oauth2.OAuth2Error
import de.iptk.groupj.auth.exception.InvalidOAuthResponse
import org.mapstruct.*
import org.mapstruct.factory.Mappers
import org.springframework.http.HttpStatus

/**
 * Mapper for mapping OAuth Error Responses from Google OAuth2 provider to adequate error response.
 */
@Mapper
interface OAuthErrorResponseMapper {

    companion object {
        @JvmField
        val INSTANCE: OAuthErrorResponseMapper = Mappers.getMapper(OAuthErrorResponseMapper::class.java)
    }

    /**
     * Map [tokenException] to a [InvalidOAuthResponse].
     */
    @Mappings(Mapping(target = "status", source = "error"))
    fun mapToInvalidOAuthCodeException(tokenException: OAuth2AccessTokenErrorResponse): InvalidOAuthResponse

    /**
     * Map [oAuthError] to the corresponding [HttpStatus].
     */
    @ValueMappings(
        ValueMapping(source = "INVALID_REQUEST", target = "BAD_REQUEST"),
        ValueMapping(source = "UNAUTHORIZED_CLIENT", target = "UNAUTHORIZED"),
        ValueMapping(source = "ACCESS_DENIED", target = "FORBIDDEN"),
        ValueMapping(source = "UNSUPPORTED_RESPONSE_TYPE", target = "UNSUPPORTED_MEDIA_TYPE"),
        ValueMapping(source = "INVALID_SCOPE", target = "BAD_REQUEST"),
        ValueMapping(source = "SERVER_ERROR", target = "INTERNAL_SERVER_ERROR"),
        ValueMapping(source = "TEMPORARILY_UNAVAILABLE", target = "SERVICE_UNAVAILABLE"),
        ValueMapping(source = "INVALID_CLIENT", target = "BAD_REQUEST"),
        ValueMapping(source = "INVALID_GRANT", target = "BAD_REQUEST"),
        ValueMapping(source = "UNSUPPORTED_GRANT_TYPE", target = "BAD_REQUEST"),
        ValueMapping(source = "INVALID_TOKEN", target = "BAD_REQUEST"),
        ValueMapping(source = "INSUFFICIENT_SCOPE", target = "FORBIDDEN"),
        ValueMapping(source = "UNSUPPORTED_TOKEN_TYPE", target = "UNPROCESSABLE_ENTITY"),
        ValueMapping(source = "AUTHORIZATION_PENDING", target = "FORBIDDEN"),
        ValueMapping(source = "SLOW_DOWN", target = "SERVICE_UNAVAILABLE"),
        ValueMapping(source = "EXPIRED_TOKEN", target = "UNAUTHORIZED")
    )
    fun mapErrorCode(oAuthError: OAuth2Error): HttpStatus

}