package de.iptk.groupj.auth.application

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.scribejava.apis.GoogleApi20
import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.model.OAuth2AccessTokenErrorResponse
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Verb
import de.iptk.groupj.auth.config.ConditionalOnGoogleOAuthLoginEnabled
import de.iptk.groupj.auth.mapper.OAuthErrorResponseMapper
import de.iptk.groupj.auth.model.GoogleOAuthUserInfo
import de.iptk.groupj.security.config.OAuth2Config
import de.iptk.groupj.security.config.Provider
import de.iptk.groupj.shared.OAUTH
import de.iptk.groupj.shared.OAUTH_GOOGLE_CODE
import de.iptk.groupj.shared.logger
import org.springframework.stereotype.Service

// OAuth2 scopes
private const val SCOPES = "profile email openid"

private const val GOOGLE_API_URL = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json"

/**
 * Service for managing GoogleOAuth2 flow and receiving user details.
 */
@Service
@ConditionalOnGoogleOAuthLoginEnabled
class GoogleOAuth2TokenService(oauthConfig: OAuth2Config, private val mapper: ObjectMapper) {

    init {
        logger().info("Hello from GoogleOAuth2TokenService")
    }

    private val googleProperties: Provider = oauthConfig.provider["google"] ?: error("google not configured")

    private val serviceBuilder = ServiceBuilder(googleProperties.id)
        .apiSecret(googleProperties.secret)
        .defaultScope(SCOPES)


    /**
     * Get authorization url for authenticating with OAuth2.
     */
    fun authorizationUrl(baseUrl: String): String =
        serviceBuilder.callback("$baseUrl$OAUTH$OAUTH_GOOGLE_CODE")
            .build(GoogleApi20.instance())
            .authorizationUrl

    /**
     * Retrieve the users Google user details
     */
    fun userInfo(baseUrl: String, code: String): GoogleOAuthUserInfo {
        val service = serviceBuilder.callback("$baseUrl$OAUTH$OAUTH_GOOGLE_CODE")
            .build(GoogleApi20.instance())

        return try {
            val accessToken = service.getAccessToken(code)
            val oAuthRequest = OAuthRequest(Verb.GET, GOOGLE_API_URL)

            service.signRequest(accessToken, oAuthRequest)
            service.execute(oAuthRequest).use { response ->
                mapper.readValue(response.body, GoogleOAuthUserInfo::class.java)
            }
        } catch (e: OAuth2AccessTokenErrorResponse) {
            throw OAuthErrorResponseMapper.INSTANCE.mapToInvalidOAuthCodeException(e)
        }
    }

}