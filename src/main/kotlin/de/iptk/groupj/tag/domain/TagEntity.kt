package de.iptk.groupj.tag.domain

import de.iptk.groupj.shared.Entity
import org.springframework.data.neo4j.core.schema.GeneratedValue
import org.springframework.data.neo4j.core.schema.Id
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Property
import javax.validation.constraints.NotBlank

/**
 * Tag database representation.
 */
@Node("Tag")
data class TagEntity(

    @Id
    @GeneratedValue
    override var id: Long? = null,

    @field:NotBlank
    @Property(name = "name")
    var name: String

) : Entity<Long>()