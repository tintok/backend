package de.iptk.groupj.tag.adapter.database

import de.iptk.groupj.tag.domain.TagEntity
import org.springframework.data.neo4j.repository.Neo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Tag database abstraction.
 */
@Repository
interface TagRepository : Neo4jRepository<TagEntity, Long> {

    fun findAllByNameIn(names: Set<String>): Set<TagEntity>

    fun findByName(names: String): Optional<TagEntity>

    /**
     * Finds all tags, sorted by the similarity to the given user descending if applicable.
     * If no similarity is found or multiple similarities are equal, tags are sorted by name ascending.
     */
    @Query(
        """
        MATCH (me:User {username: ${'$'}username}), (tag:Tag)
        OPTIONAL MATCH (me) - [similarity:SIMILAR] -> (tag)
        RETURN tag
        ORDER BY coalesce(similarity.score, 0.0) DESC, tag.name ASC
        """
    )
    fun findAllOrderBySimilarityAndName(username: String): List<TagEntity>

}