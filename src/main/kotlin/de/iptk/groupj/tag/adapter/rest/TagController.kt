package de.iptk.groupj.tag.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.shared.TAGS
import de.iptk.groupj.tag.application.TagService
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.tag.model.TagDto
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Entry point for Tag related things.
 */
@RestController
@RequestMapping(TAGS)
class TagController(private val service: TagService) {

    @GetMapping
    fun getTags(@CurrentUser me: UserEntity): List<TagDto> =
        service.findAllTagsSortedBySimilarity(me).map(TagEntity::name).map(::TagDto)

}