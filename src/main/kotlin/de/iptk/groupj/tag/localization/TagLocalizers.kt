package de.iptk.groupj.tag.localization

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import de.iptk.groupj.tag.model.TagDto
import org.springframework.context.i18n.LocaleContextHolder
import java.util.*

/**
 * Translate Tag names to the supplied local. If no proper translation is found the input is returned.
 */
private fun localizeTagName(tagName: String, locale: Locale): String = try {
    ResourceBundle.getBundle("i18n/tag-names", locale).getString(tagName)
} catch (e: MissingResourceException) {
    tagName
}

/**
 * Class for deserializing Tag names with proper localization support.
 */
class TagDelocalizer(private val mapper: ObjectMapper) : JsonDeserializer<TagDto>() {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): TagDto =
        when (LocaleContextHolder.getLocaleContext()?.locale ?: Locale.ENGLISH) {
            Locale.ENGLISH -> TagDto(mapper.readValue(p, String::class.java))
            else -> TagDto(localizeTagName(mapper.readValue(p, String::class.java), Locale.ENGLISH))
        }

}

/**
 * Class for serializing Tag names with proper localization support.
 */
class TagLocalizer(private val mapper: ObjectMapper) : JsonSerializer<TagDto>() {

    override fun serialize(value: TagDto, gen: JsonGenerator, serializers: SerializerProvider) =
        when (val locale = LocaleContextHolder.getLocaleContext()?.locale ?: Locale.ENGLISH) {
            Locale.ENGLISH -> mapper.writeValue(gen, value.name)
            else -> mapper.writeValue(gen, localizeTagName(value.name, locale))
        }

}