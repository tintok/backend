package de.iptk.groupj.tag.application

import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.tag.adapter.database.TagRepository
import de.iptk.groupj.tag.domain.TagEntity
import de.iptk.groupj.tag.model.TagDto
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service for handling all Tag related operations.
 */
@Service
class TagService(private val repository: TagRepository) {

    @Transactional(readOnly = true)
    fun findAllTagsByName(names: Set<TagDto>): Set<TagEntity> {
        val tags = repository.findAllByNameIn(names.map(TagDto::name).toSet())

        if (tags.size != names.size) {
            val known = tags.map(TagEntity::name)
            val missing = names.filter { !known.contains(it.name) }
            throw ResourceNotFoundException("Tag(s)", "[ ${missing.joinToString(", ")} ]")
        }

        return tags
    }

    @Transactional(readOnly = true)
    fun findAllTagsSortedBySimilarity(me: UserEntity): List<TagEntity> =
        repository.findAllOrderBySimilarityAndName(me.username)

}