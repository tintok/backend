package de.iptk.groupj.tag.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import de.iptk.groupj.tag.localization.TagDelocalizer
import de.iptk.groupj.tag.localization.TagLocalizer

/**
 * TagDto for representing a Tag to the outside. The TagDto gets translated to the Users locale on
 * deserialization / serialization.
 */
@JsonSerialize(using = TagLocalizer::class)
@JsonDeserialize(using = TagDelocalizer::class)
data class TagDto(val name: String)