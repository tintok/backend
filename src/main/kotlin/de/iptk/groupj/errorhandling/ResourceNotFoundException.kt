package de.iptk.groupj.errorhandling

import org.springframework.http.HttpStatus

/**
 * Generic NOT FOUND Exception class.
 */
class ResourceNotFoundException(resourceName: String, identifier: Any) :
    LocalizedException("$resourceName identified by $identifier not found") {

    override val messageKey: String = "various.not-found"

    override val status: HttpStatus = HttpStatus.NOT_FOUND

    override val parameter: Array<Any> = arrayOf(resourceName, identifier)

}