package de.iptk.groupj.errorhandling

import org.springframework.http.HttpStatus

/**
 * Exception which is used as a generic Exception template class to indicate localization support.
 */
abstract class LocalizedException(message: String) : RuntimeException(message) {

    abstract val messageKey: String

    abstract val status: HttpStatus

    override val message: String
        get() = super.message!!

    open val parameter: Array<Any> = emptyArray()

}