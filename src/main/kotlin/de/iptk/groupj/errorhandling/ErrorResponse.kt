package de.iptk.groupj.errorhandling

import org.springframework.http.HttpStatus
import java.time.ZonedDateTime

/**
 * Error representation for clients.
 */
class ErrorResponse(httpStatus: HttpStatus, val message: Any?, val path: String) {

    val timestamp: ZonedDateTime = ZonedDateTime.now()

    val status = httpStatus.value()

    val error = httpStatus.reasonPhrase

}