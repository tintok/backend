package de.iptk.groupj.errorhandling

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import de.iptk.groupj.shared.logger
import org.springframework.context.MessageSource
import org.springframework.core.env.Environment
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.multipart.MaxUploadSizeExceededException
import org.springframework.web.servlet.NoHandlerFoundException
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.ConstraintViolationException

/**
 * Extension function for loading a Exception Message with just one key from the [MessageSource].
 */
private fun MessageSource.getMessage(id: String, locale: Locale) =
    getMessage({ arrayOf(id) }, locale)

/**
 * Global Exception Handler for the most common Exceptions.
 */
@RestControllerAdvice
class GlobalExceptionHandler(private val messageSource: MessageSource, private val env: Environment) {

    private val log = logger()

    /**
     * Failing validation for inputs which are validated through @Validated.
     * See at https://stackoverflow.com/questions/16133923/400-vs-422-response-to-post-of-data.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    fun handleValidationError(e: ConstraintViolationException, request: HttpServletRequest): ErrorResponse {
        val violations: List<Violation> = e.constraintViolations
            .map { violation -> Violation(violation.propertyPath.toString(), violation.message) }
            .toList()

        return ErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, violations, request.servletPath)
    }

    /**
     * Failing validation for inputs which are directly validated through a constrain in the controller method like
     * @Min(15).
     * See at https://stackoverflow.com/questions/16133923/400-vs-422-response-to-post-of-data.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    fun handleValidationError(e: MethodArgumentNotValidException, request: HttpServletRequest): ErrorResponse {
        val violations: List<Violation> = e.bindingResult.fieldErrors
            .map { violation -> Violation(violation.field, violation.defaultMessage.toString()) }
            .toList()

        return ErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, violations, request.servletPath)
    }

    /**
     * If one tries to upload data larger then set in application.yml possible.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
    fun handleSizeLimitExceeded(e: MaxUploadSizeExceededException, req: HttpServletRequest, locale: Locale): ErrorResponse {
        log.info("upload exceeded: {}", e.message)

        return ErrorResponse(
            HttpStatus.PAYLOAD_TOO_LARGE,
            messageSource.getMessage(
                "spring.upload-exceeded",
                arrayOf(env.getProperty("spring.servlet.multipart.max-file-size")),
                locale
            ),
            req.servletPath
        )
    }


    /**
     * If a unknown json field is part of the request.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleUnrecognizedProperty(e: UnrecognizedPropertyException, request: HttpServletRequest, locale: Locale) =
        ErrorResponse(
            HttpStatus.BAD_REQUEST,
            messageSource.getMessage("spring.unrecognized-field", arrayOf(e.propertyName), locale),
            request.servletPath
        )

    /**
     * If for instance a String was provided in a JSON object but an object was expected.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleMismatchedInput(e: MismatchedInputException, request: HttpServletRequest, locale: Locale): ErrorResponse =
        ErrorResponse(
            HttpStatus.BAD_REQUEST,
            messageSource.getMessage("spring.mismatched-input", locale),
            request.servletPath
        )

    /**
     * If a required field is missing.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleMissingKotlinParameter(e: MissingKotlinParameterException, request: HttpServletRequest, locale: Locale) =
        ErrorResponse(
            HttpStatus.BAD_REQUEST,
            messageSource.getMessage("spring.required-field-missing", arrayOf(e.parameter.name), locale),
            request.servletPath
        )

    /**
     * Syntax error in request.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleJsonParseError(e: JsonParseException, request: HttpServletRequest, locale: Locale): ErrorResponse =
        ErrorResponse(
            HttpStatus.BAD_REQUEST,
            messageSource.getMessage("spring.json-error", arrayOf(e.location.lineNr, e.location.columnNr), locale),
            request.servletPath
        )

    /**
     * TypeId for an command annotated with @JsonTypeInfo is missing.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleJsonInvalidTypeId(e: InvalidTypeIdException, request: HttpServletRequest, locale: Locale): ErrorResponse =
        ErrorResponse(
            HttpStatus.BAD_REQUEST,
            messageSource.getMessage("spring.json-type-missing", locale),
            request.servletPath
        )

    /**
     * This could happen if a client sends 'image_post' and 'video_post' at the same time. Otherwise
     * fall back to fallback handler.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleAmbiguousPath(e: UnsatisfiedServletRequestParameterException, request: HttpServletRequest, locale: Locale) =
        ErrorResponse(
            HttpStatus.BAD_REQUEST,
            messageSource.getMessage("post.invalid-create-post-parameter", locale),
            request.servletPath
        )

    /**
     * If two concurrent requests modify the same database object. Spring notices this by an optimistic locking
     * strategy. Mind the @Version property in each entity.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    fun handleConcurrentModification(e: OptimisticLockingFailureException, req: HttpServletRequest, locale: Locale) =
        ErrorResponse(
            HttpStatus.CONFLICT,
            messageSource.getMessage("spring.concurrent-modification", locale),
            req.servletPath
        )

    /**
     * Handle all localization related Exceptions and translate values.
     */
    @ExceptionHandler
    fun handle(e: LocalizedException, request: HttpServletRequest, locale: Locale): ResponseEntity<ErrorResponse> {
        log.info("application exception encountered: {}", e.message)

        return ResponseEntity(
            ErrorResponse(
                e.status,
                messageSource.getMessage(e.messageKey, e.parameter, e.message, locale),
                request.servletPath
            ),
            e.status
        )
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFound(e: NoHandlerFoundException, request: HttpServletRequest, locale: Locale): ErrorResponse =
        ErrorResponse(
            HttpStatus.NOT_FOUND,
            messageSource.getMessage("spring.path-not-found", locale),
            request.servletPath
        )

    /**
     * Fallback Exception Handler for uncaught Exceptions.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun fallback(e: IllegalStateException, request: HttpServletRequest, locale: Locale): ErrorResponse {
        log.error("encountered internal error", e)

        return ErrorResponse(
            HttpStatus.INTERNAL_SERVER_ERROR,
            messageSource.getMessage("various.unexpected-error", locale),
            request.servletPath
        )
    }

}
