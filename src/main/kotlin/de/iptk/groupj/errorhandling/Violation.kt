package de.iptk.groupj.errorhandling


/**
 * Represents a single Constraint Violation.
 */
data class Violation(val field: String, val message: String)