package de.iptk.groupj

import de.iptk.groupj.shared.logger
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.time.ZonedDateTime

@SpringBootApplication(proxyBeanMethods = false)
class TinTok : ApplicationRunner {

    override fun run(args: ApplicationArguments) {
        logger().info("TinTok backend started successfully, running in timezone: " + ZonedDateTime.now().zone)
    }

}

/**
 * Application entry point.
 */
fun main(args: Array<String>) {
    runApplication<TinTok>(*args)
}
