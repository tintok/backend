package de.iptk.groupj.info.application

import de.iptk.groupj.user.application.UserService
import org.springframework.boot.actuate.info.Info
import org.springframework.boot.actuate.info.InfoContributor
import org.springframework.stereotype.Service

/**
 * This class adds additional information to the defined block in application.yml for application information.
 * It extends it by adding the number of registered users for the platform.
 */
@Service
class InfoService(private val service: UserService) : InfoContributor {

    override fun contribute(builder: Info.Builder) {
        builder.withDetail("registered_users", service.countUsers())
    }

}