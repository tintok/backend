package de.iptk.groupj.fcm.exception

import de.iptk.groupj.errorhandling.LocalizedException
import org.springframework.http.HttpStatus

/**
 * Exception which indicates an error response from FCM server.
 */
class InvalidFcmTokenException(body: String) :
    LocalizedException("FCM returned error, most likely due to client error, response body was $body") {

    override val messageKey = "fcm.invalid-token"

    override val status = HttpStatus.BAD_REQUEST

}