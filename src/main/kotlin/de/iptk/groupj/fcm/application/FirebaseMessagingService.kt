package de.iptk.groupj.fcm.application

import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.fcm.exception.InvalidFcmTokenException
import de.iptk.groupj.fcm.model.AddToGroupCommand
import de.iptk.groupj.fcm.model.CreateGroupCommand
import de.iptk.groupj.fcm.model.FcmNotificationCommand
import de.iptk.groupj.shared.logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import java.util.*

/**
 * Service which provides all FCM related operations against the Google FCM api.
 */
@Service
@ConditionalOnFcmEnabled
class FirebaseMessagingService(private val api: GoogleFcmGroupApi) {

    private val log = logger()

    init {
        log.info("FirebaseMessaging initialized")
    }

    /**
     * Create a Messaging Group with the supplied name and add the provided token.
     */
    fun createGroup(groupName: String, token: String): String {
        val response = runBlocking {
            api.create(CreateGroupCommand(groupName, token))
        }

        return if (response.isSuccessful) {
            response.body()!!.groupToken
        } else {
            val message = response.errorBody()?.string() ?: "No error response"
            log.warn("encountered error in create fcm group: {}", message)
            throw InvalidFcmTokenException(message)
        }
    }

    /**
     * Add a given token to a Notification Group.
     */
    fun addToGroup(groupName: String, groupToken: String, token: String) {
        val response = runBlocking {
            api.addMember(AddToGroupCommand(groupName, groupToken, token))
        }

        if (!response.isSuccessful) {
            val message = response.errorBody()?.string() ?: "No error response"
            log.warn("encountered error in add to fcm group: {}", message)
            throw InvalidFcmTokenException(message)
        }
    }

    /**
     * Send asynchronously message to a message group.
     */
    fun sendToGroup(command: FcmNotificationCommand, senderId: UUID, recipientId: UUID) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = api.sendMessage(command)

            // Google returns in this case 200 so we have to watch out for a certain field to see the difference ...
            if (response.body()?.containsKey("results") == true) {
                log.warn("Sending of message to group failed: {}, code {}", response.body(), response.code())
            } else {
                log.debug("User(id={}) send notification to User(id={})", senderId, recipientId)
            }
        }
    }

    /**
     * Checks if an FCM group does exist. If it does not exist it is signaled by an unsuccessful request.
     */
    fun doesGroupExists(groupName: String): Boolean {
        val response = runBlocking {
            api.exists(groupName)
        }

        return response.isSuccessful
    }

}