package de.iptk.groupj.fcm.application

import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.adapter.database.UserRepository
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Google removes an FCM group if the last app users uninstalls the app. If no more members are left in the group the
 * group is closed by google. We can not guarantee if a users logs out that he or she is the last member of an FCM group.
 * Therefore we periodically check if a FCM group still exists.
 */
@Service
@Transactional
@ConditionalOnFcmEnabled
internal class FcmCleanUpService(private val messaging: FirebaseMessagingService, private val repository: UserRepository) {

    private val log = logger()

    init {
        log.info("Hello from FcmCleanUpService")
    }

    /**
     * Check for non existing fcm groups and remove them from the database on midnight.
     */
    @Scheduled(cron = "0 0 0 * * *")
    fun unsetNonExistingFcmGroups() {
        val nonExistingFcmGroupUserIds = repository.findAllUserIdsWhereFcmTokenIsNotNull()
            .filter { id -> !messaging.doesGroupExists(id.toString()) }

        if (nonExistingFcmGroupUserIds.isNotEmpty()) {
            repository.removeFcmTokenForUserIdIn(nonExistingFcmGroupUserIds.toSet())
            log.info(
                "found and unset {} non existing fcm groups for users: {}",
                nonExistingFcmGroupUserIds.size,
                nonExistingFcmGroupUserIds
            )
        }
    }

}