package de.iptk.groupj.fcm.application

import de.iptk.groupj.fcm.model.AddToGroupCommand
import de.iptk.groupj.fcm.model.CreateGroupCommand
import de.iptk.groupj.fcm.model.FcmNotificationCommand
import de.iptk.groupj.fcm.model.GroupCreatedDto
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Google FCM Group API.
 */
interface GoogleFcmGroupApi {

    @GET("notification")
    suspend fun exists(@Query("notification_key_name") groupName: String): Response<Unit>

    @POST("notification")
    suspend fun create(@Body command: CreateGroupCommand): Response<GroupCreatedDto>

    @POST("notification")
    suspend fun addMember(@Body command: AddToGroupCommand): Response<Unit>

    @POST("send")
    suspend fun sendMessage(@Body command: FcmNotificationCommand): Response<Map<String, Any>>

}