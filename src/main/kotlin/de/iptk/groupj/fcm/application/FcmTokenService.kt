package de.iptk.groupj.fcm.application

import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service which is concerned with all FCM related operations.
 */
@Service
@Transactional
@ConditionalOnFcmEnabled
class FcmTokenService(private val messaging: FirebaseMessagingService, private val service: UserService) {

    private val log = logger()

    /**
     * Adds a Token to the Users notification group.
     */
    fun addToken(me: UserEntity, token: String) {
        me.fcmToken?.let { fcmToken ->
            if (messaging.doesGroupExists(me.id.toString())) {
                addTokenToGroup(me.id!!.toString(), fcmToken, token)
                return
            }

            log.info("encountered non-existing fcm group on new fcm token for user: {}", me.id)
        }

        createGroup(me, token)
    }

    /**
     *  Create a Messaging Group with the supplied name and add the provided token. Afterwards set the retrieved new Group
     *  token to the User and save it.
     */
    private fun createGroup(me: UserEntity, token: String) {
        me.fcmToken = messaging.createGroup(me.id!!.toString(), token)
        service.update(me)
    }


    /**
     * Add a given token to a Notification Group.
     */
    private fun addTokenToGroup(groupName: String, groupToken: String, token: String) {
        messaging.addToGroup(groupName, groupToken, token)
    }

}