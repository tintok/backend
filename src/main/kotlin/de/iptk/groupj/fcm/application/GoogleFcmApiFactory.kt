package de.iptk.groupj.fcm.application

import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.fcm.config.FcmConfig
import okhttp3.OkHttpClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

private const val FCM_BASE_URL = "https://fcm.googleapis.com/fcm/"

/**
 * Factory for a [GoogleFcmGroupApi].
 */
@ConditionalOnFcmEnabled
@Configuration(proxyBeanMethods = false)
class GoogleFcmApiFactory(config: FcmConfig) {

    private val client = OkHttpClient.Builder()
        .addInterceptor { chain ->
            val request = chain.request().newBuilder() // Always write google security headers
                .addHeader(HttpHeaders.AUTHORIZATION, "key=${config.apiKey}")
                .addHeader("project_id", "${config.projectId}")
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build()
            chain.proceed(request)
        }
        .build()

    /**
     * Create a [GoogleFcmGroupApi] bean.
     */
    @Bean
    fun fcmApi() = Retrofit.Builder()
        .baseUrl(FCM_BASE_URL)
        .addConverterFactory(JacksonConverterFactory.create())
        .client(client)
        .build()
        .create(GoogleFcmGroupApi::class.java)

}