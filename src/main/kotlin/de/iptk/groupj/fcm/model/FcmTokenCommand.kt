package de.iptk.groupj.fcm.model

import javax.validation.constraints.NotBlank

/**
 * Command to set a Users FCM token.
 */
data class FcmTokenCommand(@field:NotBlank val token: String)