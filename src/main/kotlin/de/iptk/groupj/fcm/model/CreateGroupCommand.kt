package de.iptk.groupj.fcm.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Command to create a new FCM Notification group on Googles API.
 */
class CreateGroupCommand(@field:JsonProperty("notification_key_name") val groupName: String, token: String) {

    val operation = "create"

    @JsonProperty("registration_ids")
    val ids = listOf(token)

}