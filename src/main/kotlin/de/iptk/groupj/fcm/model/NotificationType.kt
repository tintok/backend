package de.iptk.groupj.fcm.model

/**
 * Models which type a Notification has.
 */
enum class NotificationType {
    MATCH, CHAT
}