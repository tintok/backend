package de.iptk.groupj.fcm.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Command used to add a Notification key to an already existing FCM group.
 */
class AddToGroupCommand(

    @field:JsonProperty("notification_key_name")
    val groupName: String,

    @field:JsonProperty("notification_key")
    val groupToken: String,

    token: String

) {

    val operation = "add"

    @JsonProperty("registration_ids")
    val ids = listOf(token)

}