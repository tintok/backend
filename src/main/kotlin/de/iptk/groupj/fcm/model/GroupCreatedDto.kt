package de.iptk.groupj.fcm.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Googles Response if a new FCM group was created.
 */
data class GroupCreatedDto(@JsonProperty("notification_key") val groupToken: String)