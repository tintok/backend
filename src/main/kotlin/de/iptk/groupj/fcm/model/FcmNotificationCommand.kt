package de.iptk.groupj.fcm.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Command to send a new Notification to a client device via Googles FCM API.
 */
data class FcmNotificationCommand(@field:JsonProperty("to") val groupToken: String, val data: Map<String, Any?>)