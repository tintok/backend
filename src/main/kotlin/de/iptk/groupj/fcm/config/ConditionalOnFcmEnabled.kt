package de.iptk.groupj.fcm.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import java.lang.annotation.Inherited

/**
 * Annotation which checks if FCM is configured and can therefore be used. If not, the annotated component gets disabled.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@MustBeDocumented
@Inherited
@ConditionalOnProperty(prefix = "security.fcm", name = ["apiKey", "projectId"])
annotation class ConditionalOnFcmEnabled
