package de.iptk.groupj.fcm.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

/**
 * Configuration class for external FCM configuration.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "security.fcm")
data class FcmConfig(val projectId: Long = -1, val apiKey: String = "")