package de.iptk.groupj.fcm.adapter.rest

import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.fcm.application.FcmTokenService
import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.fcm.model.FcmTokenCommand
import de.iptk.groupj.shared.NOTIFICATIONS_FCM
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

/**
 * Entry point for adding a FCM  token to a Users account to enable notifications via FCM.
 */
@RestController
@ConditionalOnFcmEnabled
@RequestMapping(NOTIFICATIONS_FCM)
class FcmController(private val service: FcmTokenService) {

    init {
        logger().info("Hello from FcmController")
    }

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun addToken(@CurrentUser me: UserEntity, @RequestBody command: FcmTokenCommand) {
        service.addToken(me, command.token)
    }

}