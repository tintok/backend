package de.iptk.groupj.fcm.eventing

import de.iptk.groupj.chat.eventing.ChatMessageEvent
import de.iptk.groupj.fcm.application.FirebaseMessagingService
import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.fcm.model.FcmNotificationCommand
import de.iptk.groupj.fcm.model.NotificationType
import de.iptk.groupj.shared.EventConsumer
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.scheduling.annotation.Async
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import java.util.*

/**
 * Produces Chat notifications on new ChatEvents.
 */
@EventConsumer
@ConditionalOnFcmEnabled
class ChatFcmNotificationProducer(private val notifications: FirebaseMessagingService) {

    init {
        logger().info("Hello from ChatNotificationProducer")
    }

    /**
     * Send Chat message Notification to recipient if the recipient has a FCM token set.
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    fun afterChatMessageEvent(event: ChatMessageEvent) {
        event.recipient.fcmToken?.let { token ->
            val notification =
                notificationForChatMessage(event.sender, token, event.messageId, event.message)
            notifications.sendToGroup(notification, event.sender.id!!, event.recipient.id!!)
        }
    }

    /**
     * Create a Notification Message which is supplied to Googles API to produce a notification for the recipient.
     */
    private fun notificationForChatMessage(sender: UserEntity, token: String, messageId: UUID, message: String) =
        FcmNotificationCommand(
            token, mapOf(
                "id" to sender.id,
                "name" to "${sender.givenName} ${sender.familyName}",
                "messageId" to messageId,
                "message" to message,
                "type" to NotificationType.CHAT
            )
        )

}