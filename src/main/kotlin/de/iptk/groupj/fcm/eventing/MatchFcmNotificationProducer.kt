package de.iptk.groupj.fcm.eventing

import de.iptk.groupj.fcm.application.FirebaseMessagingService
import de.iptk.groupj.fcm.config.ConditionalOnFcmEnabled
import de.iptk.groupj.fcm.model.FcmNotificationCommand
import de.iptk.groupj.fcm.model.NotificationType
import de.iptk.groupj.shared.EventConsumer
import de.iptk.groupj.shared.logger
import de.iptk.groupj.user.domain.UserEntity
import de.iptk.groupj.user.eventing.MatchEvent
import org.springframework.scheduling.annotation.Async
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener

/**
 * Match Notification producer in case of a Match between to Users.
 */
@EventConsumer
@ConditionalOnFcmEnabled
class MatchFcmNotificationProducer(private val notifications: FirebaseMessagingService) {

    init {
        logger().info("Hello from MatchNotificationProducer")
    }

    /**
     * Send Match Notification to first and second involved User. Is only send if the corresponding User has a token set.
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    fun afterMatchEvent(event: MatchEvent) {
        event.second.fcmToken?.let { token ->
            val notification = notificationForMatch(event.first, token)
            notifications.sendToGroup(notification, event.first.id!!, event.second.id!!)
        }

        event.first.fcmToken?.let { token ->
            val notification = notificationForMatch(event.second, token)
            notifications.sendToGroup(notification, event.second.id!!, event.first.id!!)
        }
    }

    /**
     * Construct a Notification for a Match which is used to send it to Googles FCM API.
     */
    private fun notificationForMatch(matched: UserEntity, token: String): FcmNotificationCommand = FcmNotificationCommand(
        token, mapOf(
            "id" to matched.id,
            "name" to "${matched.givenName} ${matched.familyName}",
            "type" to NotificationType.MATCH
        )
    )

}