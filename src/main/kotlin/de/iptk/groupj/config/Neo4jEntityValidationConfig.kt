package de.iptk.groupj.config

import de.iptk.groupj.shared.logger
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.data.neo4j.repository.Neo4jRepository
import javax.validation.ConstraintViolation
import javax.validation.ConstraintViolationException
import javax.validation.Validation
import javax.validation.Validator

/**
 * This class intercepts all [Neo4jRepository.save] and [Neo4jRepository.saveAll] calls and validates each entity
 * beforehand for [ConstraintViolation]s.
 */
@Aspect
@EnableAspectJAutoProxy
@Configuration(proxyBeanMethods = false)
class Neo4jEntityValidationConfig {

    private final val logger = logger()
    private final var validator: Validator = Validation.buildDefaultValidatorFactory().validator


    @Pointcut("execution(* org.springframework.data.neo4j.repository.Neo4jRepository+.save(*))")
    fun whenSaveOrUpdate() {
    }

    @Pointcut("execution(* org.springframework.data.neo4j.repository.Neo4jRepository+.saveAll(*))")
    fun whenSaveAll() {
    }

    @Before("whenSaveOrUpdate() && args(entity)")
    fun validateEntity(entity: Any) {
        val violations: Set<ConstraintViolation<*>> = validator.validate(entity)
        if (violations.isNotEmpty()) {
            val error = ConstraintViolationException(violations)
            logger.error(
                "A constraint validation leaked in our domain layer and was caught by an Domain Layer " +
                        "Constraint Validation. The containing class is ${entity::class.simpleName}", error
            )
            throw error
        }
    }

    @Before("whenSaveAll() && args(entities)")
    fun validateEntities(entities: Iterable<Any>) {
        entities.forEach(this::validateEntity)
    }

}