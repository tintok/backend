package de.iptk.groupj.config

import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled

/**
 * Enable Springs scheduling support via [Scheduled].
 */
@EnableScheduling
@Configuration(proxyBeanMethods = false)
class SchedulingConfig