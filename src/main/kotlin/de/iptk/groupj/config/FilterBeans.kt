package de.iptk.groupj.config

import com.atlassian.oai.validator.springmvc.OpenApiValidationFilter
import com.github.ziplet.filter.compression.CompressingFilter
import org.springframework.boot.web.servlet.ServletComponentScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.filter.ShallowEtagHeaderFilter
import javax.servlet.Filter


/**
 * This class supplies all custom Filter Beans to the application.
 */
@ServletComponentScan
@Configuration(proxyBeanMethods = false)
class FilterBeans {

    /**
     * Include an unique ETag hash for each response.
     */
    @Bean
    fun etag() = ShallowEtagHeaderFilter()

    /**
     * Enable gzip compression capabilities.
     */
    @Bean
    fun gzip() = CompressingFilter()

    /**
     * Filter which validates requests and responses against OpenAPI spec.
     */
    @Bean
    fun validationFilter(): Filter = OpenApiValidationFilter(true, true)

}