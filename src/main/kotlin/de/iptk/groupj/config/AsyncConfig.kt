package de.iptk.groupj.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.task.SimpleAsyncTaskExecutor
import org.springframework.scheduling.annotation.EnableAsync
import java.util.concurrent.Executor


/**
 * Spring Async configuration for parallel and background execution.
 */
@EnableAsync
@Configuration(proxyBeanMethods = false)
class AsyncConfig {

    /**
     * [Executor] bean for Spring.
     */
    @Bean("taskExecutor")
    fun taskExecutor(): Executor = SimpleAsyncTaskExecutor()

}