package de.iptk.groupj.config

import de.iptk.groupj.shared.logger
import org.neo4j.driver.Driver
import org.neo4j.driver.exceptions.ServiceUnavailableException
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.ExitCodeGenerator
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.neo4j.ConfigBuilderCustomizer
import org.springframework.boot.autoconfigure.neo4j.Neo4jAutoConfiguration
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.EnableRetry
import org.springframework.retry.annotation.Recover
import org.springframework.retry.annotation.Retryable
import kotlin.system.exitProcess

/**
 * Auto Config for checking the drivers connectivity to the database on startup.
 */
@Primary
@EnableRetry
abstract class Neo4jValidatingAutoConfiguration(private val applicationContext: ConfigurableApplicationContext) :
    Neo4jAutoConfiguration() {

    /**
     * Connect to database and verify connection is established.
     */
    override fun neo4jDriver(p: Neo4jProperties, e: Environment, c: ObjectProvider<ConfigBuilderCustomizer>): Driver {
        val driver = super.neo4jDriver(p, e, c)
        driver.verifyConnectivity()

        return driver
    }

    /**
     * This method is called in case of an unsuccessful connection attempt to the database and exits the application.
     */
    @Recover
    internal fun exit(
        ex: ServiceUnavailableException, p: Neo4jProperties, e: Environment, c: ObjectProvider<ConfigBuilderCustomizer>
    ): Driver {
        val exitCode = -1
        logger().error(ex.message)
        SpringApplication.exit(applicationContext, ExitCodeGenerator { exitCode })
        exitProcess(exitCode)
    }

}

/**
 * Neo4J autoconfig for reconnecting to the database if on startup a connection could not be established.
 * This is only used in a production setup to connect to the docker container after the neo4j container started.
 */
@Profile("prod")
@Configuration(proxyBeanMethods = false)
class Neo4JRetryableAutoConfig(context: ConfigurableApplicationContext) : Neo4jValidatingAutoConfiguration(context) {

    /**
     * Retry database connection with auto config for Docker setup.
     */
    @Retryable(include = [ServiceUnavailableException::class], maxAttempts = 10, backoff = Backoff(delay = 30000))
    override fun neo4jDriver(p: Neo4jProperties, e: Environment, c: ObjectProvider<ConfigBuilderCustomizer>): Driver =
        super.neo4jDriver(p, e, c)

}

/**
 * Neo4J autoconfig for development to kill the Spring boot app instantly if no Connection to the database could be
 * established.
 */
@Profile("dev")
@Configuration(proxyBeanMethods = false)
class Neo4JFailFastAutoConfig(context: ConfigurableApplicationContext) : Neo4jValidatingAutoConfiguration(context) {

    /**
     * Only do one connection attempt in a development environment.
     */
    @Retryable(include = [ServiceUnavailableException::class], maxAttempts = 1)
    override fun neo4jDriver(p: Neo4jProperties, e: Environment, c: ObjectProvider<ConfigBuilderCustomizer>): Driver =
        super.neo4jDriver(p, e, c)

}