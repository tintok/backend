package de.iptk.groupj.config

import de.iptk.groupj.tag.adapter.rest.TagController
import org.springframework.security.core.annotation.AuthenticationPrincipal


/**
 * This Meta Annotation loads the current user as a method argument if used in a Controller.
 * For instance see at [TagController] to see how to use it.
 */
@Target(AnnotationTarget.TYPE, AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@AuthenticationPrincipal
annotation class CurrentUser
