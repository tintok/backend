package de.iptk.groupj.config

import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.nio.charset.StandardCharsets


/**
 * Spring content negotiation configuration.
 */
@Configuration(proxyBeanMethods = false)
internal class WebConfig : WebMvcConfigurer {

    /**
     * Set default Content-Type to application/json and disabled whitelabel error page by ignoring a clients accept
     * header.
     */
    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        val parameterMap: Map<String, String> = mapOf("charset" to StandardCharsets.UTF_8.name())

        configurer.apply {
            ignoreAcceptHeader(true)
            defaultContentType(MediaType(MediaType.APPLICATION_JSON, parameterMap))
        }
    }

}