package de.iptk.groupj.config

import com.atlassian.oai.validator.OpenApiInteractionValidator
import com.atlassian.oai.validator.report.LevelResolver
import com.atlassian.oai.validator.report.ValidationReport
import com.atlassian.oai.validator.springmvc.OpenApiValidationInterceptor
import de.iptk.groupj.shared.logger
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Validate requests and responses against open api schema. Violations are logged.
 */
@Profile("prod")
@Configuration(proxyBeanMethods = false)
class RequestResponseValidation : WebMvcConfigurer {

    private val validationInterceptor: HandlerInterceptor

    init {
        logger().info("Hello from RequestResponseValidation")

        val levelResolver = LevelResolver.create()
            .withDefaultLevel(ValidationReport.Level.WARN)
            .withLevel("validation.request.path.missing", ValidationReport.Level.INFO)
            .build()

        val validator = OpenApiInteractionValidator
            .createForSpecificationUrl("openapi/api-bundle.yaml")
            .withLevelResolver(levelResolver)
            .build()

        validationInterceptor = OpenApiValidationInterceptor(validator)
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(validationInterceptor)
    }

}