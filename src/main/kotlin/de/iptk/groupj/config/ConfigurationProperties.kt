package de.iptk.groupj.config

import de.iptk.groupj.fcm.config.FcmConfig
import de.iptk.groupj.security.config.JwtConfig
import de.iptk.groupj.security.config.OAuth2Config
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
 * Configuration class for external configuration from application.yaml.
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(OAuth2Config::class, JwtConfig::class, FcmConfig::class)
class ConfigurationProperties