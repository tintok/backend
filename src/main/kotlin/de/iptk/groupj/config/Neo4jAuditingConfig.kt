package de.iptk.groupj.config

import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.auditing.DateTimeProvider
import org.springframework.data.domain.AuditorAware
import org.springframework.data.neo4j.config.EnableNeo4jAuditing
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.time.ZonedDateTime
import java.util.*
import kotlin.reflect.cast


private const val AUDITOR_PROVIDER = "AuditorProvider"
private const val TEMPORAL_PROVIDER = "TemporalProvider"

/**
 * Configure auditing support for [CreatedBy], [CreatedDate], [LastModifiedBy] and [LastModifiedBy] annotations.
 */
@EnableTransactionManagement
@Configuration(proxyBeanMethods = false)
@EnableNeo4jAuditing(auditorAwareRef = AUDITOR_PROVIDER, dateTimeProviderRef = TEMPORAL_PROVIDER)
class Neo4jAuditingConfig {

    @Bean(AUDITOR_PROVIDER)
    fun auditorProvider(service: UserService): AuditorAware<UserEntity> = AuditorAware {
        Optional.of(SecurityContextHolder.getContext())
            .map(SecurityContext::getAuthentication)
            .filter(Authentication::isAuthenticated)
            .map(Authentication::getPrincipal)
            .map(UserEntity::class::cast)
    }

    @Bean(TEMPORAL_PROVIDER)
    fun temporalProvider(): DateTimeProvider = DateTimeProvider { Optional.of(ZonedDateTime.now()) }

}