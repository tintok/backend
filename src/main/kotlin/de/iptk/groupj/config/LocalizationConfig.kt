package de.iptk.groupj.config

import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.AbstractApplicationContext
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import java.nio.charset.StandardCharsets


/**
 * Configure Exception messages a User gets shown. Localization is supported as well.
 */
@Configuration(proxyBeanMethods = false)
class LocalizationConfig {

    /**
     * Provide a [MessageSource] bean to tell Spring how and where Exception messages are located.
     */
    @Bean(AbstractApplicationContext.MESSAGE_SOURCE_BEAN_NAME)
    fun messageSource(): MessageSource = ReloadableResourceBundleMessageSource().apply {
        setDefaultEncoding(StandardCharsets.UTF_8.name())
        setFallbackToSystemLocale(false)
        addBasenames(
            "classpath:org/springframework/security/messages",
            "classpath:i18n/exceptions"
        )
    }

}