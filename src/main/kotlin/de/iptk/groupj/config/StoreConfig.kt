package de.iptk.groupj.config

import org.springframework.content.commons.repository.ContentStore
import org.springframework.content.fs.config.EnableFilesystemStores
import org.springframework.context.annotation.Configuration

/**
 * Enables filesystem stores for [ContentStore] interfaces.
 */
@EnableFilesystemStores
@Configuration(proxyBeanMethods = false)
class StoreConfig