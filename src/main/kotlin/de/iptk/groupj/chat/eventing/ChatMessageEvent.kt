package de.iptk.groupj.chat.eventing

import de.iptk.groupj.user.domain.UserEntity
import java.util.*

/**
 * Event which is used to signal that a new Chat message was sent.
 */
data class ChatMessageEvent(val sender: UserEntity, val recipient: UserEntity, val messageId: UUID, val message: String)