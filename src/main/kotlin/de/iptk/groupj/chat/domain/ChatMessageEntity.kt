package de.iptk.groupj.chat.domain

import de.iptk.groupj.shared.CHAT_MESSAGE_MAX_SIZE
import de.iptk.groupj.shared.Entity
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.neo4j.core.schema.*
import java.time.ZonedDateTime
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * Domain object for a single Chat message.
 */
@Node("ChatMessage")
class ChatMessageEntity(

    @Id
    @GeneratedValue
    override var id: UUID? = null,

    @field:NotBlank
    @Property("message")
    @field:Size(max = CHAT_MESSAGE_MAX_SIZE)
    val message: String,

    @Property("created")
    val created: ZonedDateTime,

    @Relationship(type = "SUBJECT", direction = Relationship.Direction.OUTGOING)
    val subject: UserEntity,

    @Property("send")
    val send: Boolean,

    @Property("read")
    val read: Boolean,

    @Relationship(type = "PARTNER", direction = Relationship.Direction.OUTGOING)
    val partner: UserEntity

) : Entity<UUID>()