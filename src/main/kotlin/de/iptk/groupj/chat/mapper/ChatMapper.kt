package de.iptk.groupj.chat.mapper

import de.iptk.groupj.chat.domain.ChatMessageEntity
import de.iptk.groupj.chat.model.ChatMessageDto
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

/**
 * Mapper for mapping model classes in the chat domain.
 */
@Mapper
interface ChatMapper {

    companion object {
        @JvmField
        val INSTANCE: ChatMapper = Mappers.getMapper(ChatMapper::class.java)
    }

    fun mapToChatMessageDto(entity: ChatMessageEntity): ChatMessageDto

}