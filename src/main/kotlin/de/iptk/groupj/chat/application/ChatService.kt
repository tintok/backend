package de.iptk.groupj.chat.application

import de.iptk.groupj.chat.adapter.database.ChatRepository
import de.iptk.groupj.chat.domain.ChatMessageEntity
import de.iptk.groupj.chat.eventing.ChatMessageEvent
import de.iptk.groupj.chat.model.ConversationOverviewEntry
import de.iptk.groupj.errorhandling.ResourceNotFoundException
import de.iptk.groupj.user.application.UserService
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.*

/**
 * Service class which is concerned with all Chat related operations.
 */
@Service
class ChatService(

    private val repository: ChatRepository,

    private val userService: UserService,

    private val publisher: ApplicationEventPublisher

) {

    // Conversation
    @Transactional(readOnly = true)
    fun findAllConversations(me: UserEntity, pageable: Pageable): Page<ConversationOverviewEntry> =
        repository.findConversations(me.username, pageable)

    /**
     * Retrieve a Conversation with another User and set the retrieved messages to read.
     */
    @Transactional
    @PreAuthorize("@userAuthorizationSecurityEvaluator.areMatched(#me, #partnerId)")
    fun findConversation(me: UserEntity, partnerId: UUID, pageable: Pageable): Page<ChatMessageEntity> {
        if (!userService.existsById(partnerId)) {
            throw ResourceNotFoundException("Chat Partner", partnerId)
        }

        return repository.findConversation(me.username, partnerId, pageable)
    }

    @Transactional
    @PreAuthorize("@userAuthorizationSecurityEvaluator.areMatched(#me, #partnerId)")
    fun deleteConversation(me: UserEntity, partnerId: UUID) {
        if (!userService.existsById(partnerId)) {
            throw ResourceNotFoundException("Chat Partner", partnerId)
        }

        if (!repository.deleteConversation(me.username, partnerId)) {
            throw ResourceNotFoundException("Conversation", partnerId)
        }
    }


    // Message
    @Transactional
    @PreAuthorize("@userAuthorizationSecurityEvaluator.areMatched(#me, #partnerId)")
    fun sendChatMessage(me: UserEntity, partnerId: UUID, message: String): ChatMessageEntity {
        // Throws if User does not exist
        val recipient = userService.findById(partnerId)

        // Save messages inverted and normal
        val created = ZonedDateTime.now()
        val chatMessage = ChatMessageEntity(
            message = message, created = created, subject = me, partner = recipient, send = true, read = true
        ).let(repository::save)

        val invertedChatMessage = ChatMessageEntity(
            message = message, created = created, subject = recipient, partner = me, send = false, read = false
        ).let(repository::save)

        publisher.publishEvent(ChatMessageEvent(me, recipient, invertedChatMessage.id!!, invertedChatMessage.message))
        return chatMessage
    }

    @Transactional
    @PreAuthorize("@userAuthorizationSecurityEvaluator.areMatched(#me, #partnerId)")
    fun markChatMessageAsRead(me: UserEntity, partnerId: UUID, messageId: UUID) {
        if (!userService.existsById(partnerId)) {
            throw ResourceNotFoundException("Chat Partner", partnerId)
        }

        if (!repository.markMessageAsRead(me.username, partnerId, messageId)) {
            throw ResourceNotFoundException("Message in Conversation with partner $partnerId", messageId)
        }
    }

    @Transactional
    @PreAuthorize("@userAuthorizationSecurityEvaluator.areMatched(#me, #partnerId)")
    fun deleteChatMessage(me: UserEntity, partnerId: UUID, messageId: UUID) {
        if (!userService.existsById(partnerId)) {
            throw ResourceNotFoundException("Chat Partner", partnerId)
        }

        if (!repository.deleteMessage(me.username, partnerId, messageId)) {
            throw ResourceNotFoundException("Message in Conversation with partner $partnerId", messageId)
        }
    }

}