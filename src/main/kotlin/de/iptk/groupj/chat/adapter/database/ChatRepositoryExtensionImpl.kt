package de.iptk.groupj.chat.adapter.database

import com.fasterxml.jackson.databind.ObjectMapper
import de.iptk.groupj.chat.domain.ChatMessageEntity
import de.iptk.groupj.chat.model.ConversationOverviewEntry
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.neo4j.core.Neo4jTemplate
import org.springframework.data.neo4j.core.PreparedQuery
import org.springframework.stereotype.Repository

private const val conversationsStatement = """
            MATCH (me:User {username: ${'$'}username}) <- [:SUBJECT] - (chat:ChatMessage) - [:PARTNER] -> (partner:User)
            WITH me, partner, chat
            ORDER BY chat.created DESC
            WITH me, partner, collect(chat) as chats, max(chat.created) AS last
            RETURN { id: partner.id, name: partner.given_name + " " + partner.family_name } as partner,
                     last,
                     chats[0].message as message,
                     chats[0].send as send,
                     size([c in chats WHERE NOT c.read]) as unread
            SKIP ${'$'}skip LIMIT ${'$'}limit
        """

private const val countStatement = """
            MATCH (:User {username: ${'$'}username}) <- [:SUBJECT] - (:ChatMessage) - [:PARTNER] -> (partner:User)
            RETURN count(DISTINCT partner)
        """

/**
 * Implementation of [ChatRepositoryExtension] which extends [ChatRepository] to collect a Users Chat overview.
 */
@Repository
internal class ChatRepositoryExtensionImpl(private val template: Neo4jTemplate, private val mapper: ObjectMapper) :
    ChatRepositoryExtension<ChatMessageEntity> {

    override fun findConversations(username: String, pageable: Pageable): Page<ConversationOverviewEntry> {
        val total = count(username)
        val conversations = conversations(username, pageable)

        return PageImpl(conversations, pageable, total)
    }

    /**
     * Find all Conversations.
     */
    private fun conversations(username: String, pageable: Pageable): List<ConversationOverviewEntry> {
        val parameter = mapOf("username" to username, "skip" to pageable.offset, "limit" to pageable.pageSize)
        val query = PreparedQuery.queryFor(ConversationOverviewEntry::class.java)
            .withCypherQuery(conversationsStatement.trimIndent())
            .withParameters(parameter)
            .usingMappingFunction { _, record -> mapper.convertValue(record.asMap(), ConversationOverviewEntry::class.java) }
            .build()

        return template.toExecutableQuery(query).results
    }

    /**
     * Count all Conversation overview entries.
     */
    private fun count(username: String): Long {
        val parameter = mapOf("username" to username)
        return template.count(countStatement.trimIndent(), parameter)
    }

}


