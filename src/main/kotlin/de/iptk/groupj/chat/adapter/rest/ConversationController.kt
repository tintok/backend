package de.iptk.groupj.chat.adapter.rest

import de.iptk.groupj.chat.application.ChatService
import de.iptk.groupj.chat.mapper.ChatMapper
import de.iptk.groupj.chat.model.ChatMessageDto
import de.iptk.groupj.chat.model.ConversationOverviewEntry
import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.shared.CHAT_CONVERSATIONS
import de.iptk.groupj.shared.ResponseEntities
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

/**
 * Controller for interacting with Chat Conversations.
 */
@RestController
@RequestMapping(CHAT_CONVERSATIONS)
class ConversationController(private val service: ChatService) {

    @GetMapping
    fun getChatOverview(@CurrentUser me: UserEntity, pageable: Pageable): ResponseEntity<Page<ConversationOverviewEntry>> =
        service.findAllConversations(me, pageable).let(ResponseEntities::ok)

    @GetMapping(path = ["{partnerId}"])
    fun getConversation(@CurrentUser me: UserEntity, @PathVariable partnerId: UUID, pageable: Pageable)
            : ResponseEntity<Page<ChatMessageDto>> =
        service.findConversation(me, partnerId, pageable)
            .map(ChatMapper.INSTANCE::mapToChatMessageDto)
            .let(ResponseEntities::ok)

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = ["{partnerId}"])
    fun deleteConversation(@CurrentUser me: UserEntity, @PathVariable partnerId: UUID) {
        service.deleteConversation(me, partnerId)
    }


}