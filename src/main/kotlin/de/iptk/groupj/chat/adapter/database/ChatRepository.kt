package de.iptk.groupj.chat.adapter.database

import de.iptk.groupj.chat.domain.ChatMessageEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.neo4j.repository.Neo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Data abstraction class for [ChatMessageEntity].
 */
@Repository
interface ChatRepository : Neo4jRepository<ChatMessageEntity, UUID>, ChatRepositoryExtension<ChatMessageEntity> {

    // Conversation
    /**
     * Find messages from a certain Conversation.
     */
    @Query(
        value = """
            MATCH (chat:ChatMessage) - [s:SUBJECT] -> (subject:User {username: ${'$'}username}),
                  (chat) - [p:PARTNER] -> (participant:User {id: ${'$'}partnerId})
            SET chat.read = true
            RETURN chat, collect(s), collect(subject), collect(p), collect(participant)
            ORDER BY chat.created DESC SKIP ${'$'}skip LIMIT ${'$'}limit 
        """,
        countQuery = """
            MATCH (message:ChatMessage) - [:SUBJECT] -> (:User {username: ${'$'}username}),
                  (message) - [:PARTNER] -> (:User {id: ${'$'}partnerId})
            RETURN count(message)
        """
    )
    fun findConversation(username: String, partnerId: UUID, pageable: Pageable): Page<ChatMessageEntity>

    /**
     * Delete a Conversation. If the Conversation does not exist returns false, else true.
     */
    @Query(
        value = """
            MATCH (chat:ChatMessage) - [:SUBJECT] -> (:User {username: ${'$'}username}),
                  (chat) - [:PARTNER] -> (:User {id: ${'$'}partnerId})
            DETACH DELETE chat
            RETURN count(chat) > 0
        """,
        delete = true
    )
    fun deleteConversation(username: String, partnerId: UUID): Boolean


    // Messages
    /**
     * Mark a Message as read. If the message does not exist returns false, else true.
     */
    @Query(
        """
        MATCH (message:ChatMessage {id : ${'$'}messageId}) - [:SUBJECT] -> (:User {username: ${'$'}username}),
              (message) - [:PARTNER] -> (:User {id: ${'$'}partnerId})
        SET message.read = true
        RETURN count(message) > 0
        """
    )
    fun markMessageAsRead(username: String, partnerId: UUID, messageId: UUID): Boolean

    /**
     * Deletes a Message. If the message does not exist returns false, else true.
     */
    @Query(
        value = """
           MATCH (chat:ChatMessage {id : ${'$'}messageId}) - [:SUBJECT] -> (:User {username: ${'$'}username}),
                 (chat) - [:PARTNER] -> (:User {id: ${'$'}partnerId})
           DETACH DELETE chat
           RETURN count(chat) > 0
        """,
        delete = true
    )
    fun deleteMessage(username: String, partnerId: UUID, messageId: UUID): Boolean

}