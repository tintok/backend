package de.iptk.groupj.chat.adapter.rest

import de.iptk.groupj.chat.application.ChatService
import de.iptk.groupj.chat.mapper.ChatMapper
import de.iptk.groupj.chat.model.ChatCommand
import de.iptk.groupj.chat.model.ChatMessageDto
import de.iptk.groupj.config.CurrentUser
import de.iptk.groupj.shared.CHAT_MESSAGES
import de.iptk.groupj.user.domain.UserEntity
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.*
import javax.validation.Valid

/**
 * Controller for interacting with Chat messages.
 */
@Validated
@RestController
@RequestMapping(CHAT_MESSAGES)
class ChatMessageController(private val service: ChatService) {

    @PostMapping
    fun send(@CurrentUser me: UserEntity, @PathVariable partnerId: UUID, @Valid @RequestBody command: ChatCommand)
            : ResponseEntity<ChatMessageDto> {

        val message = service.sendChatMessage(me, partnerId, command.message)
            .let(ChatMapper.INSTANCE::mapToChatMessageDto)

        val location = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("{messageId}")
            .build(message.id)

        return ResponseEntity.created(location).body(message)
    }

    @PutMapping(path = ["{messageId}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun markAsRead(@CurrentUser me: UserEntity, @PathVariable partnerId: UUID, @PathVariable messageId: UUID) {
        service.markChatMessageAsRead(me, partnerId, messageId)
    }


    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = ["{messageId}"])
    fun delete(@CurrentUser me: UserEntity, @PathVariable partnerId: UUID, @PathVariable messageId: UUID) {
        service.deleteChatMessage(me, partnerId, messageId)
    }

}