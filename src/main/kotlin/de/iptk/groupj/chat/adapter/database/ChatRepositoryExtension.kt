package de.iptk.groupj.chat.adapter.database

import de.iptk.groupj.chat.model.ConversationOverviewEntry
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * Interface which extends ChatRepository with custom operations for retrieving a Users Chat overview.
 */
interface ChatRepositoryExtension<T> {

    /**
     * Find a Users Chat overview.
     */
    fun findConversations(username: String, pageable: Pageable): Page<ConversationOverviewEntry>

}