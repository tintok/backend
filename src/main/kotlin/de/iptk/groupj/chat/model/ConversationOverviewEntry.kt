package de.iptk.groupj.chat.model

import java.time.ZonedDateTime
import java.util.*

/**
 * Models a single entry in a Chat overview.
 */
data class ConversationOverviewEntry(

    val partner: ConversationPartner,

    val last: ZonedDateTime,

    val unread: Int,

    val message: String,

    val send: Boolean

)

/**
 * Models the Chat partner in a Conversation.
 */
data class ConversationPartner(val id: UUID, val name: String)