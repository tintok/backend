package de.iptk.groupj.chat.model

import de.iptk.groupj.shared.CHAT_MESSAGE_MAX_SIZE
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * Command used to model the process of sending a message.
 */
data class ChatCommand(

    @field:NotBlank @field:Size(max = CHAT_MESSAGE_MAX_SIZE)
    val message: String

)