package de.iptk.groupj.chat.model

import java.time.ZonedDateTime
import java.util.*

/**
 * Model which is used to represent a Chat message to a User.
 */
data class ChatMessageDto(val id: UUID, val created: ZonedDateTime, val message: String, val send: Boolean)