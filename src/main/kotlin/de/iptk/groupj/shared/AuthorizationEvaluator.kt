package de.iptk.groupj.shared

import org.springframework.core.annotation.AliasFor
import org.springframework.stereotype.Component

/**
 * Component annotation for indicating a Component is used to evaluate security permissions.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Component
annotation class AuthorizationEvaluator(@get:AliasFor(annotation = Component::class) val value: String = "")