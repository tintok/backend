package de.iptk.groupj.shared

/**
 * Check if the mime type of a supplied Post corresponds to the Posts Type.
 */
fun validateMultipartContentType(mimeType: String?, allowedTypes: String): Boolean = when {
    mimeType == null -> false
    !mimeType.matches(Regex(allowedTypes)) -> false
    else -> true
}