package de.iptk.groupj.shared

import org.springframework.core.io.Resource
import org.springframework.http.CacheControl
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import java.util.concurrent.TimeUnit

/**
 * Build a response which is cacheable.
 */
fun cacheableContentResponse(content: Pair<String?, Resource?>): ResponseEntity<Resource> = buildResponse(
    content.second,
    CacheControl.maxAge(365, TimeUnit.DAYS).cachePrivate().noTransform(),
    content.first
)

/**
 * Build a response which is non-cacheable.
 */
fun defaultCacheableContentResponse(content: Pair<String?, Resource?>): ResponseEntity<Resource> = buildResponse(
    content.second,
    CacheControl.empty(),
    content.first
)

/**
 * Build a response with the provided caching strategy. If the provided [Resource] does not exist return NO_CONTENT.
 */
private fun buildResponse(resource: Resource?, cache: CacheControl, contentType: String?): ResponseEntity<Resource> {
    return if (resource?.exists() == true) {
        val headers = contentType?.let { type ->
            HttpHeaders().apply {
                set(HttpHeaders.CONTENT_TYPE, type)
            }
        }

        ResponseEntity.ok()
            .cacheControl(cache)
            .headers(headers)
            .body(resource)
    } else {
        ResponseEntity.noContent().build()
    }
}