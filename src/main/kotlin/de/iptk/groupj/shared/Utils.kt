package de.iptk.groupj.shared

import org.neo4j.driver.Value
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import java.util.*

/**
 * Get logger for certain class.
 */
inline fun <reified T> T.logger(): Logger = LoggerFactory.getLogger(T::class.java)

/**
 * Simpler if else operator much more like the java ?.
 */
infix fun <T> Boolean.then(value: T) = TernaryExpression(this, value)

/**
 * Get a possibility to access Neo4Js values as UUID.
 */
fun Value.asUUID(): UUID = UUID.fromString(asString())

class TernaryExpression<T>(private val flag: Boolean, private val truly: T) {
    infix fun or(falsy: T): T = if (flag) truly else falsy
}

/**
 * Give Iterable the ability to check if is not empty.
 */
val <T> Iterable<T?>.isNotEmpty: Boolean
    get() = iterator().hasNext()

/**
 * Class which returns NO_CONTENT 204 if supplied iterable is empty, else it returns OK 200.
 */
class ResponseEntities {
    companion object {
        fun <T> ok(payload: T): ResponseEntity<T> where T : Iterable<Any?> {
            return payload.isNotEmpty then ResponseEntity.ok(payload) or ResponseEntity.noContent().build()
        }
    }
}