package de.iptk.groupj.shared

import de.iptk.groupj.user.domain.UserEntity
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.neo4j.core.schema.Property
import org.springframework.data.neo4j.core.schema.Relationship
import java.io.Serializable
import java.time.ZonedDateTime


/**
 * Provide last edited and last updated Spring functionality as a template class.
 */
abstract class AbstractAuditableBaseEntity<T : Serializable>(

    @CreatedDate
    @Property(name = "created")
    var created: ZonedDateTime?,

    @LastModifiedDate
    @Property(name = "updated")
    var updated: ZonedDateTime?,

    @CreatedBy
    @Relationship(type = "PUBLISHED", direction = Relationship.Direction.INCOMING)
    var author: UserEntity?

) : Entity<T>()