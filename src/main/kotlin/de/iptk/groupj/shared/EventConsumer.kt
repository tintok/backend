package de.iptk.groupj.shared

import org.springframework.core.annotation.AliasFor
import org.springframework.stereotype.Component

/**
 * Annotation for indicating a component consumes events.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Component
annotation class EventConsumer(@get:AliasFor(annotation = Component::class) val value: String = "")
