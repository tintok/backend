package de.iptk.groupj.shared

// Auth
const val AUTH = "/auth"
const val SIGN_IN = "/sign-in"
const val SIGN_UP = "/sign-up"
const val REFRESH = "/refresh"

// FCM
const val NOTIFICATIONS_FCM = "/notifications/fcm"

// OAuth
const val OAUTH = "$AUTH/oauth2"
const val OAUTH_GOOGLE_AUTH = "/authorization/google"
const val OAUTH_GOOGLE_CODE = "/code/google"

// User
const val USER = "/users"

// User me
const val USER_ME = "$USER/me"
const val USER_ME_PASSWORD = "/password"
const val MATCHES = "$USER_ME/matches"

// Profile-Picture
const val PROFILE_PICTURE = "/profile-picture"
const val USER_ME_PROFILE_PICTURE = "$USER_ME$PROFILE_PICTURE"

// Info
const val STATUS = "/status"
const val INFO = "/info"

// Tags
const val TAGS = "/tags"

// Posts
const val POSTS = "/posts"
const val POST_IMAGE_DATA = "/image"
const val POST_VIDEO_DATA = "/video"
const val LIKE = "/like"
const val DISLIKE = "/dislike"
const val CLEAR = "/clear"

// Posts Me
const val POSTS_ME = "$POSTS/me"

// Posts Home
const val POSTS_FEED = "$POSTS/feed"

// Posts Recommended
const val POSTS_RECOMMENDED = "$POSTS/recommended"

// Chats
const val CHAT_CONVERSATIONS = "/chats"
const val CHAT_MESSAGES = "$CHAT_CONVERSATIONS/{partnerId}/messages"
