package de.iptk.groupj.shared

// PASSWORD
const val MIN_PASSWORD_LENGTH = 6
const val MAX_PASSWORD_LENGTH = 64

// JWT
const val VALIDITY_NUMBER_OF_WEEKS = 2

// POST
const val POST_TEXT_MAX_SIZE = 512
const val POST_TAG_MIN_SIZE = 1
const val POST_TAG_MAX_SIZE = 8

// RECOMMENDATION
const val REC_RANDOM_CHANCE = 0.1       // chance for a recommended post to be drawn randomly
const val REC_RANDOM_POOL = 10          // amount of posts do draw from
const val REC_RANDOM_LIKE_LIMIT = -3    // draw from posts with at least this many likes minus dislikes

// MEDIA
const val ALLOWED_IMAGE_TYPES = "^image/(gif|jpeg|jpg|png|webp)$"
const val ALLOWED_VIDEO_TYPES = "^video/(mp4|webm)$"

// MATCHING
const val MATCH_INTERACTION_THRESHOLD = 5

// CHAT
const val CHAT_MESSAGE_MAX_SIZE = 512

// User
const val USER_MAX_GIVEN_NAME_LENGTH = 64
const val USER_MAX_FAMILY_NAME_LENGTH = 64
const val USER_MAX_USERNAME_LENGTH = 64
const val USER_MAX_LOCATION_LENGTH = 64
const val USER_MAX_BIOGRAPHY_LENGTH = 512