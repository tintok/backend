package de.iptk.groupj.shared

import java.io.Serializable

/**
 * Abstract base entity class which provides equals and hashcode methods based on the entities id.
 */
abstract class Entity<T : Serializable> : Serializable {

    abstract val id: T?

    /**
     * Id is our primary index and merge key, therefore equals contract is based on it.
     */
    final override fun equals(other: Any?) = when (other) {
        null -> false
        is Entity<*> -> this === other || (id != null && id == other.id)
        else -> super.equals(other)
    }

    /**
     * Id is our primary index and merge key, therefore hashCode contract is based on it.
     */
    final override fun hashCode() = id?.hashCode() ?: System.identityHashCode(this)

}