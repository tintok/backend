package de.iptk.groupj.similarity.eventing

import de.iptk.groupj.post.eventing.PostInteractionEvent
import de.iptk.groupj.shared.EventConsumer
import de.iptk.groupj.similarity.application.SimilarityTaskExecutor
import org.springframework.scheduling.annotation.Async
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener

/**
 * This class consumes all PostInteractionEvent to eventually issue post recommendation updates via similarity
 * algorithm.
 */
@EventConsumer
internal class PostInteractionEventConsumer(private val coordinator: SimilarityTaskExecutor) {

    /**
     * Prepares a Job for a similarity computation. This is executed synchronously and invokes the coordinator
     * to process the generated job.
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    fun afterPostInteraction(event: PostInteractionEvent) {
        coordinator.addJob(event.user.id!!, event.user.username, event.postId, event.type)
    }

}