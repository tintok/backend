package de.iptk.groupj.similarity.adapter.database

import com.fasterxml.jackson.databind.ObjectMapper
import de.iptk.groupj.similarity.model.PostInteractions
import de.iptk.groupj.similarity.model.Similarity
import de.iptk.groupj.similarity.model.TagInteractions
import org.springframework.data.neo4j.core.Neo4jTemplate
import org.springframework.data.neo4j.core.PreparedQuery
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class SimilarityRepositoryImpl(private val template: Neo4jTemplate, private val mapper: ObjectMapper) {

    /**
     * Creates or updates the given similarities as 'SIMILAR' relationships between the users.
     */
    fun saveUserToUserSimilarities(similarities: Collection<Similarity<String>>) = executeSaveQuery(
        """
        MATCH (from:User { username: ${'$'}from }), (to:User { username: ${'$'}to })
        MERGE (from) - [s:SIMILAR] - (to)
        SET s.score = ${'$'}score
        """,
        similarities
    )

    /**
     * Creates or updates the given similarities as 'SIMILAR' relationships between users and tags.
     */
    fun saveUserToTagSimilarities(similarities: Collection<Similarity<String>>) = executeSaveQuery(
        """
        MATCH (from:User { username: ${'$'}from }), (to:Tag { name: ${'$'}to })
        MERGE (from) - [s:SIMILAR] - (to)
        SET s.score = ${'$'}score
        """,
        similarities
    )

    /**
     * Executes the given query. The query must contain exactly three variables: `$from`, `$to` and `$score`.
     *
     * @param statement cypher query to be executed
     * @param similarities similarities to save
     */
    private fun executeSaveQuery(statement: String, similarities: Collection<Similarity<String>>) {
        similarities.forEach { similarity ->
            val parameters = mapOf(
                "from" to similarity.from,
                "to" to similarity.to,
                "score" to similarity.score
            )

            val query = PreparedQuery.queryFor(Unit::class.java)
                .withCypherQuery(statement.trimIndent())
                .withParameters(parameters)
                .build()

            template.toExecutableQuery(query).results
        }
    }

    /**
     * For all users who liked or published a given post, counts all posts that were liked or published by those users
     * and the given user.
     *
     * The query first finds the given user and post. It then finds all other users who liked or published this post.
     * It then counts the amount of posts the other users _and_ the given user both liked.
     *
     * @param username username of user who is compared to the other users
     * @param postId id of the post from which the search is started.
     * @return the amount of common likes for the given user and every user who liked the given post
     */
    fun countCommonLikes(username: String, postId: UUID): List<PostInteractions> = executeAggregateQuery(
        """
        MATCH (me:User { username: ${'$'}username })
        MATCH (post:Post { id: ${'$'}postId })
        MATCH (other:User) - [:LIKED|PUBLISHED] -> (post)
        MATCH (other) - [:LIKED|PUBLISHED] -> (common:Post)
        WHERE me <> other AND (me) - [:LIKED|PUBLISHED] -> (common)
        RETURN other.username AS username, count(DISTINCT common) AS interactions
        """,
        username,
        postId
    )

    /**
     * For all users who disliked a given post, counts all posts that were disliked by those users and the given user.
     *
     * The query first finds the given user and post. It then finds all other users who disliked this post.
     * It then counts the amount of posts the other users _and_ the given user both disliked.
     *
     * @param username username of user who is compared to the other users
     * @param postId id of the post from which the search is started.
     * @return the amount of common dislikes for the given user and every user who disliked the given post
     */
    fun countCommonDislikes(username: String, postId: UUID): List<PostInteractions> = executeAggregateQuery(
        """
        MATCH (me:User { username: ${'$'}username })
        MATCH (post:Post { id: ${'$'}postId })
        MATCH (other:User) - [:DISLIKED] -> (post)
        MATCH (other) - [:DISLIKED] -> (common:Post)
        WHERE me <> other AND (me) - [:DISLIKED] -> (common)
        RETURN other.username AS username, count(DISTINCT common) AS interactions
        """,
        username,
        postId
    )

    /**
     * For all users who liked or published a given post, counts all posts that were liked or published by those users
     * and disliked by the given user.
     *
     * The query first finds the given user and post. It then finds all other users who liked or published this post.
     * It then counts the amount of posts the other users liked or published and the given user disliked.
     *
     * @param username username of user who is compared to the other users
     * @param postId id of the post from which the search is started.
     * @return the amount of opposed likes for the given user and every user who liked the given post
     */
    fun countOpposedLikes(username: String, postId: UUID): List<PostInteractions> = executeAggregateQuery(
        """
        MATCH (me:User { username: ${'$'}username })
        MATCH (post:Post { id: ${'$'}postId })
        MATCH (other:User) - [:LIKED|PUBLISHED] -> (post)
        MATCH (other) - [:LIKED|PUBLISHED] -> (opposed:Post)
        WHERE me <> other AND (me) - [:DISLIKED] -> (opposed)
        RETURN other.username AS username, count(DISTINCT opposed) AS interactions
        """,
        username,
        postId
    )

    /**
     * For all users who disliked a given post, counts all posts that were disliked by those users
     * and liked or published by the given user.
     *
     * The query first finds the given user and post. It then finds all other users who disliked this post.
     * It then counts the amount of posts the other users disliked and the given user liked or published.
     *
     * @param username username of user who is compared to the other users
     * @param postId id of the post from which the search is started.
     * @return the amount of opposed dislikes for the given user and every user who liked the given post
     */
    fun countOpposedDislikes(username: String, postId: UUID): List<PostInteractions> = executeAggregateQuery(
        """
        MATCH (me:User { username: ${'$'}username })
        MATCH (post:Post { id: ${'$'}postId })
        MATCH (other:User) - [:DISLIKED] -> (post)
        MATCH (other) - [:DISLIKED] -> (opposed:Post)
        WHERE me <> other AND (me) - [:LIKED|PUBLISHED] -> (opposed)
        RETURN other.username AS username, count(DISTINCT opposed) AS interactions
        """,
        username,
        postId
    )

    /**
     * For all tags the given post is tagged with, counts the amount of liked and published interactions between the
     * given user and all posts tagged with those tags.
     *
     * @param username username of user whose interactions are counted
     * @param postId post used to find all tags going to be counted
     * @return the amount of interactions for the given user and all tags connected to the given post
     */
    fun countLikedTags(username: String, postId: UUID): List<TagInteractions> = executeAggregateQuery(
        """
        MATCH (:Post { id: ${'$'}postId }) - [:TAGGED_WITH] -> (tag:Tag)
        MATCH (:User { username: ${'$'}username }) - [liked:LIKED|PUBLISHED] -> (:Post) - [:TAGGED_WITH] -> (tag)
        RETURN tag.name AS tag, count(DISTINCT liked) AS interactions
        """,
        username,
        postId
    )

    /**
     * For all tags the given post is tagged with, counts the amount of disliked interactions between the given user
     * and all posts tagged with those tags.
     *
     * @param username username of user whose interactions are counted
     * @param postId post used to find all tags going to be counted
     * @return the amount of interactions for the given user and all tags connected to the given post
     */
    fun countDislikedTags(username: String, postId: UUID): List<TagInteractions> = executeAggregateQuery(
        """
        MATCH (:Post { id: ${'$'}postId }) - [:TAGGED_WITH] -> (tag:Tag)
        MATCH (:User { username: ${'$'}username }) - [disliked:DISLIKED] -> (:Post) - [:TAGGED_WITH] -> (tag)
        RETURN tag.name AS tag, count(DISTINCT disliked) AS interactions
        """,
        username,
        postId
    )

    /**
     * Executes the given query and maps the result to a list of interactions. The query must contain exactly
     * two variables: `$username` and `$postId`.
     *
     * @param statement cypher query to be executed
     * @param username variable to bind to `$username`
     * @param postId variable to bind to `$postId`
     * @return result of query
     */
    private inline fun <reified T> executeAggregateQuery(statement: String, username: String, postId: UUID): List<T> {
        val parameters = mapOf("username" to username, "postId" to postId.toString())

        val query = PreparedQuery.queryFor(T::class.java)
            .withCypherQuery(statement.trimIndent())
            .withParameters(parameters)
            .usingMappingFunction { _, interaction ->
                mapper.convertValue(interaction.asMap(), T::class.java)
            }
            .build()

        return template.toExecutableQuery(query).results
    }

}
