package de.iptk.groupj.similarity.model

/**
 * Represents the similarity of two nodes in the neo4j database.
 *
 * @param from source node identifier
 * @param to target node identifier
 * @param similarity similarity between the two nodes
 * @param T type of the identifying property of the node
 */
data class Similarity<T>(val from: T, val to: T, var similarity: Int, var score: Double) {

    constructor(from: T, to: T, similarity: Int) : this(from, to, similarity, similarity.toDouble())

}