package de.iptk.groupj.similarity.model

/**
 * Represents a count of interactions with a user
 */
data class PostInteractions(val username: String, val interactions: Int)