package de.iptk.groupj.similarity.model

/**
 * Represents a count of interactions with a tag
 */
data class TagInteractions(val tag: String, val interactions: Int)
