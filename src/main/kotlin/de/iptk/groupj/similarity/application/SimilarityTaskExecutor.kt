package de.iptk.groupj.similarity.application

import de.iptk.groupj.post.eventing.PostInteractionEventType
import de.iptk.groupj.shared.logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.HashSet
import kotlin.collections.LinkedHashMap

private val WORKER_COUNT = Runtime.getRuntime().availableProcessors()

/**
 * Class for coordinating Job execution for similarity tasks.
 */
@Component
internal class SimilarityTaskExecutor(

    private val userSimilarityService: UserPostSimilarityService,

    private val tagSimilarityService: UserTagSimilarityService

) {

    private val log = logger()
    private val activeWorkerCount = AtomicInteger(0)
    private val runningJobsForUsers: MutableSet<UUID> = Collections.synchronizedSet(HashSet(100))
    private val jobQueue: MutableMap<UUID, Job> = Collections.synchronizedMap(LinkedHashMap(100))

    init {
        log.info("Registered $WORKER_COUNT workers")
    }

    /**
     * Add a new Job or extends an already existing Job with the new Job details.
     */
    fun addJob(userId: UUID, username: String, postId: UUID, type: PostInteractionEventType) {
        synchronized(jobQueue) {
            if (jobQueue.containsKey(userId)) {
                val jobDef = jobQueue[userId]!!
                addJobInteractions(jobDef, type, postId)
                log.debug("adding additional details for Post(id={}) for User(id={})", postId, userId)
            } else {
                val jobDef = Job(username).also { job -> addJobInteractions(job, type, postId) }
                jobQueue[userId] = jobDef
                log.debug("adding new Job for Post(id={}) for User(id={})", postId, userId)

                spawnWorker()
            }
        }
    }

    /**
     * Determines based on the type if a computation is done for user similarity or tag similarity or both and adds it
     * to the Job.
     */
    private fun addJobInteractions(job: Job, type: PostInteractionEventType, postId: UUID) = when (type) {
        // update user and tag similarities on like and dislike
        PostInteractionEventType.LIKED, PostInteractionEventType.DISLIKED, PostInteractionEventType.CLEARED -> {
            job.tagSimilarityPostIds.add(postId)
            job.userSimilarityPostIds.add(postId)
        }

        // update tag similarities on publish and update
        PostInteractionEventType.PUBLISHED, PostInteractionEventType.UPDATED -> {
            job.tagSimilarityPostIds.add(postId)
        }
    }

    /**
     * Retrieve a new Job if a Job is available. If no Job is available Option.empty() is returned.
     * If a Job for User X is already executing and another Job for User X is in the queue the method returns Option.empty().
     * We ensure that no similarity computation for the same User is run in parallel.
     */
    private fun retrieveJob(): Optional<Pair<UUID, Job>> = synchronized(jobQueue) {
        if (jobAvailable()) {
            val userId = jobQueue.keys.firstOrNull { key -> !runningJobsForUsers.contains(key) }!!
            val jobDef = jobQueue.remove(userId)!!
            runningJobsForUsers.add(userId)

            log.debug("similarity computation started for user(id={})", userId)
            log.debug("active workers: {}", activeWorkerCount.incrementAndGet())
            log.debug("queue contains {} unprocessed Jobs", jobQueue.size)

            Optional.of(Pair(userId, jobDef))
        } else {
            Optional.empty()
        }
    }

    /**
     * Method which is called by a worker after the Worker finished the Job. This is used to determine how many workers
     * are active and when a Job is finished.
     */
    private fun markJobAsCompleted(userId: UUID) {
        synchronized(jobQueue) {
            runningJobsForUsers.remove(userId)

            log.debug("similarity computation finished for user(id={})", userId)
            log.debug("active workers: {}", activeWorkerCount.decrementAndGet())

            spawnWorker()
        }
    }

    /**
     * Determines if a new Job is available and can be executed.
     */
    private fun jobAvailable(): Boolean = synchronized(runningJobsForUsers) {
        synchronized(jobQueue) {
            activeWorkerCount.get() < WORKER_COUNT && jobQueue.keys.firstOrNull { key -> !runningJobsForUsers.contains(key) } != null
        }
    }

    /**
     * Method for spawning a new async Worker if a new Job is available.
     */
    private fun spawnWorker() {
        CoroutineScope(Dispatchers.Default).launch {
            val workerName = "Recommendation Worker: ${Thread.currentThread().name}"
            retrieveJob().ifPresentOrElse(
                { job ->
                    val watch = StopWatch(workerName)
                    watch.start()
                    job.second.tagSimilarityPostIds.forEach { postId ->
                        tagSimilarityService.updateSimilarities(job.second.username, postId)
                    }

                    job.second.userSimilarityPostIds.forEach { postId ->
                        userSimilarityService.updateSimilarities(job.second.username, postId)
                    }

                    markJobAsCompleted(job.first)
                    watch.stop()
                    log.debug(watch.shortSummary())
                },
                { log.debug("$workerName did not receive a Job, terminating ...") }
            )
        }
    }

    /**
     * Job container with the Users username and for which Posts a Tag and User similarity calculation has to be performed.
     */
    private inner class Job(

        val username: String,

        val tagSimilarityPostIds: MutableSet<UUID> = mutableSetOf(),

        val userSimilarityPostIds: MutableSet<UUID> = mutableSetOf()

    )

}