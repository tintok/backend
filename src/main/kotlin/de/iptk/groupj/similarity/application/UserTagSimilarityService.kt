package de.iptk.groupj.similarity.application

import de.iptk.groupj.similarity.adapter.database.SimilarityRepositoryImpl
import de.iptk.groupj.similarity.model.Similarity
import de.iptk.groupj.similarity.model.TagInteractions
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.math.exp
import kotlin.math.log10

@Service
class UserTagSimilarityService(private val repository: SimilarityRepositoryImpl) {

    /**
     * Updates the 'SIMILAR' relationship for this user and all tags connected to the given post. The relationship
     * contains a score computed by the following formula:
     *
     * `x := (sum of all positive interactions) - (sum of all negative interactions)`
     *
     * `score := x >= 1 ? log10(x + 1) / 2 + 1 : -e^(10 / x) / 2 + 1`
     *
     * A user has a positive interaction with a tag if they liked/published a post tagged with the tag. They have a
     * negative interaction if they disliked a post tagged with the tag.
     *
     * @param username name of the user to update similarities for
     * @param postId id of the post which is used to find all tags being updated
     */
    @Transactional
    fun updateSimilarities(username: String, postId: UUID) {
        // count likes and dislikes with the tags of the post
        val likes = repository.countLikedTags(username, postId)
        val dislikes = repository.countDislikedTags(username, postId)

        // combine and compute similarity
        val similarities = computeSimilarityScores(username, likes, dislikes) { x ->
            // if net interactions are positive, compute similarity by using log10, but half the score
            // to dampen the impact compared to user similarities
            // if net interactions are negative or zero, compute similarity by using -e^(10/x) to gradually punish
            // posts tagged with the disliked tags
            if (x >= 1) log10(x + 1.0) / 2 + 1 else -exp(10.0 / x) / 2 + 1
        }

        // update
        repository.saveUserToTagSimilarities(similarities)
    }

    /**
     * Computes similarities for the given user with all tags in the given interactions. Half of the dislikes are
     * subtracted from the likes. After that the modifier function is applied.
     *
     * @param username name of the user who is similar to all tags
     * @param likes positive interactions of the given user for posts tagged with the tags
     * @param dislikes negative interactions of the given user for posts tagged with the tags
     * @param modifier function mapping the net amount of interactions to a similarity score. Default is the identity
     * function
     * @return a collection of similarities between the given user and the tags in the given interactions.
     */
    internal fun computeSimilarityScores(
        username: String,
        likes: List<TagInteractions>,
        dislikes: List<TagInteractions>,
        modifier: (similarity: Int) -> Double
    ): Collection<Similarity<String>> {

        val netInteractions = mutableMapOf<String, Int>()

        // insert all likes
        likes.forEach { l ->
            netInteractions[l.tag] = l.interactions
        }
        // subtract all dislikes
        dislikes.forEach { d ->
            netInteractions.compute(d.tag) { _, v ->
                (v ?: 0) - d.interactions / 2
            }
        }

        return netInteractions.entries.map { interactions ->
            Similarity(username, interactions.key, interactions.value, modifier(interactions.value))
        }
    }

}