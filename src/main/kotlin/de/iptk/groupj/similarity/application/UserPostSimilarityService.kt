package de.iptk.groupj.similarity.application

import de.iptk.groupj.shared.MATCH_INTERACTION_THRESHOLD
import de.iptk.groupj.similarity.adapter.database.SimilarityRepositoryImpl
import de.iptk.groupj.similarity.model.PostInteractions
import de.iptk.groupj.similarity.model.Similarity
import de.iptk.groupj.user.adapter.database.UserRepository
import de.iptk.groupj.user.eventing.MatchEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.math.log10

@Service
class UserPostSimilarityService(

    private val repository: SimilarityRepositoryImpl,

    private val userRepository: UserRepository,

    private val publisher: ApplicationEventPublisher

) {

    /**
     * Updates the 'SIMILAR' relationship for this user and all users connected to the given post. The relationship
     * contains a score computed by the following formula:
     *
     * `x := (sum of all common interactions) - (sum of all opposed interactions)`
     *
     * `score := x >= 1 ? log10(x + 1) + 1 : 1`
     *
     * Two users have a common interaction with a post if they both liked/published the post. They have opposed
     * interactions if one user liked/published a post the other disliked.
     *
     * @param username name of the user to compare with the other users
     * @param postId id of the post from which the search for other users is started
     */
    @Transactional
    fun updateSimilarities(username: String, postId: UUID) {
        // count common and opposed interactions
        val commonLikes = repository.countCommonLikes(username, postId)
        val commonDislikes = repository.countCommonDislikes(username, postId)
        val opposedLikes = repository.countOpposedLikes(username, postId)
        val opposedDislikes = repository.countOpposedDislikes(username, postId)

        // combine and compute similarity
        val similarities =
            computeSimilarityScores(username, commonLikes, commonDislikes, opposedLikes, opposedDislikes) { x ->
                // if net interactions are positive, compute similarity by using log10
                // if net interactions are negative or zero, do nothing
                if (x >= 1) log10(x + 1.0) + 1 else 1.0
            }

        // generate matches if net similarity is high enough
        similarities
            .filter { similarity -> similarity.similarity >= MATCH_INTERACTION_THRESHOLD }
            .filter { similarity -> !userRepository.isMatched(similarity.from, similarity.to) }
            .forEach(this::generateMatch)

        // update
        repository.saveUserToUserSimilarities(similarities)
    }

    /**
     * Saves a 'MATCHED' relation between the two users of the given similarity and publishes a [MatchEvent].
     *
     * @param similarity dto containing the usernames of the two users
     */
    internal fun generateMatch(similarity: Similarity<String>) {
        val first = userRepository.findByUsername(similarity.from).get()
        val second = userRepository.findByUsername(similarity.to).get()

        userRepository.saveMatched(first.username, second.username)
        publisher.publishEvent(MatchEvent(first, second))
    }

    /**
     * Computes similarities for the given user with all users in the given interactions. The common likes and dislikes
     * are summed, all opposed likes and dislikes are subtracted. After that the given modifier function is applied.
     *
     * @param username name of the user who is similar to all other users
     * @param commonLikes common likes of the given user and other users
     * @param commonDislikes common dislikes of the given user and other users
     * @param opposedLikes opposed likes of the given user and other users
     * @param opposedDislikes opposed dislikes of the given user and other users
     * @param modifier function mapping the net amount of interactions to a similarity score. Default is the identity
     * function.
     * @return a collection of similarities between the given user and the users in the given interactions.
     */
    internal fun computeSimilarityScores(
        username: String,
        commonLikes: List<PostInteractions>,
        commonDislikes: List<PostInteractions>,
        opposedLikes: List<PostInteractions>,
        opposedDislikes: List<PostInteractions>,
        modifier: (similarity: Int) -> Double = { v -> v.toDouble() }
    ): Collection<Similarity<String>> {

        val netInteractions = mutableMapOf<String, Int>()

        // insert all common likes
        commonLikes.forEach { likes ->
            netInteractions[likes.username] = likes.interactions
        }
        // add all common dislikes
        commonDislikes.forEach { dislikes ->
            netInteractions.compute(dislikes.username) { _, v ->
                (v ?: 0) + dislikes.interactions
            }
        }
        // subtract all opposed likes
        opposedLikes.forEach { likes ->
            netInteractions.compute(likes.username) { _, v ->
                (v ?: 0) - likes.interactions
            }
        }
        // subtract all opposed dislikes
        opposedDislikes.forEach { dislikes ->
            netInteractions.compute(dislikes.username) { _, v ->
                (v ?: 0) - dislikes.interactions
            }
        }

        // map to dto and apply modifier
        return netInteractions.entries.map { interactions ->
            Similarity(username, interactions.key, interactions.value, modifier(interactions.value))
        }
    }

}
